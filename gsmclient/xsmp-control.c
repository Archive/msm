
#include "gsmclient.h"
#include <libintl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define _(x) gettext (x)

static GMainLoop *main_loop = NULL;

static void
usage (void)
{
  g_print ("xsmp-control [--logout] [--save]\n");
  exit (1);
}

static void
finished_callback (GsmClient *client)
{
  if (g_main_is_running (main_loop))
    g_main_quit (main_loop);
}

int
main (int argc, char **argv)
{
  int i;
  const char *prev_arg;
  gboolean logout = FALSE;
  gboolean save = FALSE;
  GsmClient *client;
  
  prev_arg = NULL;
  i = 1;
  while (i < argc)
    {
      const char *arg = argv[i];
      
      if (strcmp (arg, "--help") == 0 ||
          strcmp (arg, "-h") == 0 ||
          strcmp (arg, "-?") == 0)
        usage ();
      else if (strcmp (arg, "--logout") == 0)
        logout = TRUE;
      else if (strcmp (arg, "--save") == 0)
        save = TRUE;
      else
        usage ();
      
      prev_arg = arg;
      
      ++i;
    }

  if (logout && save)
    {
      fprintf (stderr, _("Can't specify both --logout and --save\n"));
      exit (1);
    }
  else if (!(logout || save))
    {
      fprintf (stderr, _("Must specify either --logout or --save\n"));
      exit (1);
    }

  g_set_prgname ("xsmp-control");
  
  client = gsm_client_new ();

  gsm_client_set_restart_style (client, GSM_RESTART_NEVER);
  
  gsm_client_connect (client, NULL);

  if (!gsm_client_get_connected (client))
    {
      fprintf (stderr, _("Failed to connect to session manager\n"));
      exit (1);
    }

  if (save)
    gsm_client_request_global_session_save (client);
  else if (logout)
    gsm_client_request_global_session_logout (client);

  g_signal_connect (G_OBJECT (client), "die", G_CALLBACK (finished_callback),
                    NULL);
  g_signal_connect (G_OBJECT (client), "save_complete", G_CALLBACK (finished_callback),
                    NULL);
  
  main_loop = g_main_loop_new (NULL, FALSE);

  g_main_run (main_loop);
  
  return 0;
}
