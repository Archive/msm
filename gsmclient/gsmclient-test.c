#include "gsmclient.h"
#include <X11/SM/SMlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <popt.h>

#include <libintl.h>
#define _(x) gettext(x)
#define N_(x) x

static GMainLoop *main_loop = NULL;
static guint count_level = 0;

static void
quit_callback (GsmClient *client)
{
  g_print (_("Quitting...\n"));

  if (g_main_is_running (main_loop))
    g_main_quit (main_loop);
}

static void
save_callback (GsmClient *client, gboolean is_phase2)
{
  g_print (_("Saving state...\n"));
  g_print (_("Count level is '%d'\n"), count_level);
}

static void
get_properties_callback (SmcConn smc_conn, void *data, 
                         int num_props, SmProp **props)
{
  GsmClient *client;
  SmProp *prop;
  int i;

  client = (GsmClient *) data;

  printf ("--\n");
  for (i = 0; i < num_props; i++)
    {
      prop = props[i];

      printf ("%s: ", prop->name);

      if (!strcmp (prop->type, SmCARD8))
        printf ("%x\n", *((int *) prop->vals[0].value));
      else if (!strcmp (prop->type, SmARRAY8))
        printf ("%s\n", (char *) prop->vals[0].value);
      else if (!strcmp (prop->type, SmLISTofARRAY8))
        {
          int j;
          printf ("[ ");
          for (j = 0; j < prop->num_vals; j++)
            {
              printf ("\"%s\"", (char *) prop->vals[j].value);
              printf (" ");
            }
          printf ("]\n");
        }
    }
}

static gboolean
count_callback (GsmClient *client)
{
  count_level++;

  SmcGetProperties (gsm_client_get_smc_connection (client),
                    get_properties_callback,
                    client);

  return TRUE;
}

void
set_restart_style (GsmClient *client, const char *restart_style_string)
{
  GsmRestartStyle restart_style;

  if (!restart_style_string)
    restart_style = GSM_RESTART_IF_RUNNING;
  else if (!strcmp (restart_style_string, "if-running"))
    restart_style = GSM_RESTART_IF_RUNNING;
  else if (!strcmp (restart_style_string, "anyway"))
    restart_style = GSM_RESTART_ANYWAY;
  else if (!strcmp (restart_style_string, "immediately"))
    restart_style = GSM_RESTART_IMMEDIATELY;
  else if (!strcmp (restart_style_string, "never"))
    restart_style = GSM_RESTART_NEVER;
  else
    {
      g_printerr (_("Unknown restart style '%s'."
                    "Using default restart style (if-running).\n"), 
                    restart_style_string);
      restart_style = GSM_RESTART_NEVER;
    }

  gsm_client_set_restart_style (client, restart_style);
}

void
set_role (GsmClient *client, const char *role_string)
{
  GsmRoleFlags role;

  if (!role_string)
    role = GSM_ROLE_NORMAL;
  else if (!strcmp (role_string, "window-manager"))
    role = GSM_ROLE_WINDOW_MANAGER;
  else if (!strcmp (role_string, "desktop-handler"))
    role = GSM_ROLE_DESKTOP_HANDLER;
  else if (!strcmp (role_string, "panel"))
    role = GSM_ROLE_PANEL;
  else if (!strcmp (role_string, "desktop-component"))
    role = GSM_ROLE_DESKTOP_COMPONENT;
  else if (!strcmp (role_string, "setup"))
    role = GSM_ROLE_SETUP;
  else
    {
      g_printerr (_("Unknown client role '%s'."
                    "Using default role (normal).\n"), 
                    role_string);
      role = GSM_ROLE_NORMAL;
    }

  gsm_client_set_supported_roles (client, role);
}

int
main (int argc, char **argv)
{
  GsmClient *client;
  char *restart_style = NULL, *session_id = NULL, *role = NULL;
  poptContext popt_context;

  struct poptOption options[] = 
    {
      { "sm-client-id", 0, POPT_ARG_STRING, &session_id, 0,
        N_("Specify session management ID"), N_("ID") }, 

      { "sm-client-role", 0, POPT_ARG_STRING, &role, 0,
        N_("Specify client role.  Can be one of: "
           "normal, window-manager, setup, desktop-component"), N_("ROLE") }, 

      { "restart-style", 0, POPT_ARG_STRING, &restart_style, 0,
        N_("Specify restart style.  Can be one of: "
        "if-running, anyway, immediately, never"), N_("STYLE") },

      { "count-level", 'c', POPT_ARG_INT, &count_level, 0,
        N_("Specify where to count from"), N_("NUMBER") },

      POPT_AUTOHELP 

      { NULL, 0, 0, NULL, 0 }
    };

  popt_context = poptGetContext (NULL, argc, (const char **) argv, 
                                 options, 0);
  while (poptGetNextOpt (popt_context) > 0);
  poptFreeContext (popt_context);
  
  client = gsm_client_new ();

  gsm_client_connect (client, session_id);

  if (!gsm_client_get_connected (client))
    {
      g_printerr ("Failed to connect to session manager\n\n");
      return 1;
    }

  g_signal_connect (G_OBJECT (client), "die", G_CALLBACK (quit_callback),
                    NULL);

  g_signal_connect (G_OBJECT (client), "save", G_CALLBACK (save_callback),
                    NULL);

  set_role (client, role);

  set_restart_style (client, restart_style);

  {
      char *restart_argv[128] = { NULL }, *count_level_string;
      int restart_argc = 0;

      count_level_string = g_strdup_printf ("%d", count_level);

      restart_argv[restart_argc++] = argv[0];
      restart_argv[restart_argc++] = "--sm-client-id";
      restart_argv[restart_argc++] = (char *) gsm_client_get_id (client);
      restart_argv[restart_argc++] = "--count-level";
      restart_argv[restart_argc++] = count_level_string;

      gsm_client_set_restart_command (client, restart_argc, 
                                              restart_argv);

      g_free (count_level_string);
  }

  gsm_client_set_icon (client, "gnome-gegl");

  g_timeout_add (5 * 1000, (GSourceFunc) count_callback, client);

  main_loop = g_main_loop_new (NULL, FALSE);

  g_main_loop_run (main_loop);

  return 0;
}
