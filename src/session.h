/* msm session */

/* 
 * Copyright (C) 2001 Havoc Pennington
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef MSM_SESSION_H
#define MSM_SESSION_H

#include "client.h"

typedef struct _MsmSession MsmSession;

MsmSession* msm_session_get               (const char *name);
MsmSession* msm_session_get_failsafe      (void);
void        msm_session_save              (MsmSession *session,
                                           MsmServer  *server);
void        msm_session_update_client     (MsmSession *session,
                                           MsmClient  *client);
void        msm_session_remove_client     (MsmSession *session,
                                           MsmClient  *client);
void        msm_session_launch            (MsmSession *session);
void        msm_session_client_registered (MsmSession *session,
                                           const char *previous_id);
void        msm_session_restart_client    (MsmSession *session,
                                           MsmClient  *client);
gboolean    msm_session_client_id_known   (MsmSession *session,
                                           const char *previous_id);
void        msm_session_purge_clients     (MsmSession *session,
                                           MsmServer  *server);


gboolean    msm_run_command           (const char   *command,
                                       GList        *proplist,
                                       GError      **error);
void        msm_run_discard           (const char   *client_name,
                                       GList        *proplist);

#endif

