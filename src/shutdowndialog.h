/* shutdowndialog.h - dialog for shutting down

   Copyright (C) 2004 Ray Strode

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.  
*/
#ifndef MSM_SHUTDOWN_DIALOG_H
#define MSM_SHUTDOWN_DIALOG_H

#include <gtk/gtkdialog.h>

#define MSM_TYPE_SHUTDOWN_DIALOG                  (msm_shutdown_dialog_get_type ())
#define MSM_SHUTDOWN_DIALOG(obj)                  (GTK_CHECK_CAST ((obj), MSM_TYPE_SHUTDOWN_DIALOG, MsmShutdownDialog))
#define MSM_SHUTDOWN_DIALOG_CLASS(klass)          (GTK_CHECK_CLASS_CAST ((klass), MSM_TYPE_SHUTDOWN_DIALOG, MsmShutdownDialogClass))
#define MSM_IS_SHUTDOWN_DIALOG(obj)               (GTK_CHECK_TYPE ((obj), MSM_TYPE_SHUTDOWN_DIALOG))
#define MSM_IS_SHUTDOWN_DIALOG_CLASS(klass)       (GTK_CHECK_CLASS_TYPE ((klass), MSM_TYPE_SHUTDOWN_DIALOG))
#define MSM_SHUTDOWN_DIALOG_GET_CLASS(obj)        (GTK_CHECK_GET_CLASS ((obj), MSM_TYPE_SHUTDOWN_DIALOG, MsmShutdownDialogClass))

#define MSM_TYPE_SHUTDOWN_TYPE_FLAGS  (msm_shutdown_type_flags_get_type ())

typedef struct _MsmShutdownDialogPrivate MsmShutdownDialogPrivate;

typedef enum
{
  MSM_SHUTDOWN_TYPE_CANCEL = ~0,   /* Used for shutdown dialog cancel button */
  MSM_SHUTDOWN_TYPE_DEFAULT = 0,
  MSM_SHUTDOWN_TYPE_LOGOUT = 1 << 0,
  MSM_SHUTDOWN_TYPE_REBOOT = 1 << 1,
  MSM_SHUTDOWN_TYPE_HALT = 1 << 2
} MsmShutdownTypeFlags;

typedef struct
{
  GtkDialog dialog;
  MsmShutdownDialogPrivate *priv;
} MsmShutdownDialog;

typedef struct
{
  GtkDialogClass parent_class;
} MsmShutdownDialogClass;

GType msm_shutdown_dialog_get_type (void) G_GNUC_CONST;
GtkWidget *msm_shutdown_dialog_new (MsmShutdownTypeFlags shutdown_types);
MsmShutdownTypeFlags
msm_shutdown_dialog_get_shutdown_types (MsmShutdownDialog * dialog);
gboolean msm_shutdown_dialog_get_save_session (MsmShutdownDialog * dialog);

GType msm_shutdown_type_flags_get_type (void) G_GNUC_CONST;

#endif /* MSM_SHUTDOWN_DIALOG_H */
