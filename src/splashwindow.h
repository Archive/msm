/* splashwindow.h - splash screen rendering

   Copyright (C) 1999-2002 Jacob Berkman, Ximian Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.  
*/
#ifndef MSM_SPLASH_WINDOW_H
#define MSM_SPLASH_WINDOW_H

#include <gtk/gtkwindow.h>

#define MSM_TYPE_SPLASH_WINDOW                  (msm_splash_window_get_type ())
#define MSM_SPLASH_WINDOW(obj)                  (GTK_CHECK_CAST ((obj), MSM_TYPE_SPLASH_WINDOW, MsmSplashWindow))
#define MSM_SPLASH_WINDOW_CLASS(klass)          (GTK_CHECK_CLASS_CAST ((klass), MSM_TYPE_SPLASH_WINDOW, MsmSplashWindowClass))
#define MSM_IS_SPLASH_WINDOW(obj)               (GTK_CHECK_TYPE ((obj), MSM_TYPE_SPLASH_WINDOW))
#define MSM_IS_SPLASH_WINDOW_CLASS(klass)       (GTK_CHECK_CLASS_TYPE ((klass), MSM_TYPE_SPLASH_WINDOW))
#define MSM_SPLASH_WINDOW_GET_CLASS(obj)        (GTK_CHECK_GET_CLASS ((obj), MSM_TYPE_SPLASH_WINDOW, MsmSplashWindowClass))

typedef struct _MsmSplashWindowPrivate MsmSplashWindowPrivate;

typedef struct
{
  GtkWindow               window;
  MsmSplashWindowPrivate *priv;
} MsmSplashWindow;

typedef struct
{
  GtkWindowClass parent_class;
} MsmSplashWindowClass;

GType      msm_splash_window_get_type (void) G_GNUC_CONST;
GtkWidget *msm_splash_window_new (GdkPixbuf *background);

int        msm_splash_window_add_icon (MsmSplashWindow *window,
                                       GdkPixbuf *icon);
void       msm_splash_window_set_status (MsmSplashWindow *window,
                                         const char *status);
int        msm_splash_window_get_icon_position (MsmSplashWindow *window,
                                                GdkPixbuf *icon);
void       msm_splash_window_replace_icon (MsmSplashWindow *window,
                                           GdkPixbuf *icon, gint position);

GdkPixbuf *msm_splash_window_get_background (MsmSplashWindow * window);



#endif /* MSM_SPLASH_WINDOW_H */
