/* msm server object */

/* 
 * Copyright (C) 2001 Havoc Pennington
 * Copyright (C) 2003 Red Hat, Inc.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Some code in here from xsm:
 *
 * Copyright 1993, 1998  The Open Group
 * 
 * All Rights Reserved.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE OPEN GROUP BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of The Open Group
 * shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written
 * authorization from The Open Group.
 */

#include <config.h>

#include <X11/ICE/ICEutil.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <time.h>

#include <errno.h>

#include "server.h"
#include "session.h"
#include "util.h"
#include "dialogs.h"
#include "shutdowndialog.h"
#include "logout.h"
#include "props.h"
#include "gdmremote.h"

#if 0
#define PRINT_TIME(x) access (x, R_OK)
#else
#define PRINT_TIME(x)
#endif

#define MAGIC_COOKIE_LEN 16
#define GDM_SOCKET "/tmp/.gdm_socket"

/* FIXME we need to time out anytime we're waiting for a client
 * response, such as InteractDone, SaveYourselfDone, ConnectionClosed
 * (after sending Die)
 */

struct _MsmServer
{
  MsmSession *session;
  GList *clients;
  IceAuthDataEntry *auth_entries;
  int n_auth_entries;
  MsmClient *currently_interacting;
  GList     *interact_pending;

  guint client_timeout_handler;

  MsmClient *expired_client;
  GtkWidget *client_expired_dialog;
  
  guint in_global_save : 1; /* True if we are saving, but not done saving */
  guint in_shutdown : 1;    /* True if we started to shutdown, and haven't
                             * cancelled shutdown
                             */
  guint save_allows_interaction : 1;
  
  guint user_disabled_save : 1; /* True if user didn't ask to save in logout dialog */
  
  MsmShutdownTypeFlags post_logout;
  GdmRemote *gdm_remote;

  MsmShutdownTypeFlags shutdown_types;
};

static Status register_client_callback              (SmsConn         cnxn,
                                                     SmPointer       manager_data,
                                                     char           *previous_id);
static void   interact_request_callback             (SmsConn         cnxn,
                                                     SmPointer       manager_data,
                                                     int             dialog_type);
static void   interact_done_callback                (SmsConn         cnxn,
                                                     SmPointer       manager_data,
                                                     Bool            cancel_shutdown);
static void   save_yourself_request_callback        (SmsConn         cnxn,
                                                     SmPointer       manager_data,
                                                     int             save_type,
                                                     Bool            shutdown,
                                                     int             interact_style,
                                                     Bool            fast,
                                                     Bool            global);
static void   save_yourself_phase2_request_callback (SmsConn         cnxn,
                                                     SmPointer       manager_data);
static void   save_yourself_done_callback           (SmsConn         cnxn,
                                                     SmPointer       manager_data,
                                                     Bool            success);
static void   close_connection_callback             (SmsConn         cnxn,
                                                     SmPointer       manager_data,
                                                     int             count,
                                                     char          **reasonMsgs);
static void set_properties_callback    (SmsConn     cnxn,
                                        SmPointer   manager_data,
                                        int         num_props,
                                        SmProp    **props);
static void delete_properties_callback (SmsConn     cnxn,
                                        SmPointer   manager_data,
                                        int         num_props,
                                        char      **prop_names);
static void get_properties_callback    (SmsConn     cnxn,
                                        SmPointer   manager_data);


static Status new_client_callback                   (SmsConn         cnxn,
                                                     SmPointer       manager_data,
                                                     unsigned long  *mask_ret,
                                                     SmsCallbacks   *callbacks_ret,
                                                     char          **failure_reason_ret);
static Bool   host_auth_callback                    (char           *hostname);


static void ice_init (MsmServer *server);

static gboolean create_auth_entries (MsmServer       *server,
                                     IceListenObj    *listen_objs,
                                     int              n_listen_objs);
static void free_auth_entries       (IceAuthDataEntry *entries,
                                     int               n_entries);

static void handle_timeouts         (MsmServer        *server);

static void mark_ice_connection_closed (MsmServer     *server,
                                        IceConn        connection);

static void ask_user_about_logout      (MsmServer     *server);


static void sms_error_handler (SmsConn       smsConn,
                               Bool          swap,
                               int           offendingMinorOpcode,
                               unsigned long offendingSequence,
                               int           errorClass,
                               int           severity,
                               SmPointer     values);

static MsmShutdownTypeFlags msm_server_get_available_shutdown_types (MsmServer *server);

static SmsErrorHandler old_sms_error_handler = NULL;

static MsmServer*
msm_server_new_with_session (MsmSession *session)
{
  char errbuf[256];
  MsmServer *server;

  server = g_new (MsmServer, 1);

  server->session = session;
  server->clients = NULL;
  server->auth_entries = NULL;
  server->n_auth_entries = 0;
  server->client_timeout_handler = 0;
  server->expired_client = NULL;
  server->client_expired_dialog = NULL;
  server->currently_interacting = NULL;
  server->interact_pending = NULL;
  server->in_global_save = FALSE;
  server->in_shutdown = FALSE;
  server->save_allows_interaction = FALSE;
  server->user_disabled_save = FALSE;
  server->post_logout = MSM_SHUTDOWN_TYPE_CANCEL;
  server->gdm_remote = NULL;

  if (old_sms_error_handler == NULL)
    old_sms_error_handler = SmsSetErrorHandler (sms_error_handler);
  
  if (!SmsInitialize (PACKAGE, VERSION,
                      new_client_callback,
                      server,
                      host_auth_callback,
                      sizeof (errbuf), errbuf))
    msm_fatal (_("Could not initialize SMS: %s\n"), errbuf);
  
  ice_init (server);

  server->shutdown_types = msm_server_get_available_shutdown_types (server);

  return server;
}

static MsmShutdownTypeFlags
msm_server_get_available_shutdown_types (MsmServer *server)
{
  MsmShutdownTypeFlags shutdown_types = MSM_SHUTDOWN_TYPE_LOGOUT;
  GError *error = NULL;

  if (!server->gdm_remote)
    {
      server->gdm_remote = gdm_remote_new (GDM_SOCKET, &error);

      if (!server->gdm_remote)
        msm_warning ("Unable to connect to GDM socket: %s\n", 
                     error? error->message: "unknown reason");
      else if (!gdm_remote_is_authenticated (server->gdm_remote))
        msm_verbose ("Unable to authenticate with GDM socket: %s\n",
                     error? error->message: "unknown reason");

      if (error)
        {
          g_error_free (error);
          error = NULL;
        }
    }

  if (server->gdm_remote)
    {
      GdmRemoteLogoutFlags logout_actions;
      logout_actions = gdm_remote_query_logout_actions (server->gdm_remote, 
                                                        &error);

      if (error)
        {
          msm_verbose ("%s\n", error->message);
          g_error_free (error);
          error = NULL;
        }

      if (logout_actions != GDM_REMOTE_LOGOUT_NONE)
        {
          if (logout_actions & GDM_REMOTE_LOGOUT_HALT)
            shutdown_types |= MSM_SHUTDOWN_TYPE_HALT;

          if (logout_actions & GDM_REMOTE_LOGOUT_REBOOT)
            shutdown_types |= MSM_SHUTDOWN_TYPE_REBOOT;
        }
    }

  /* FIXME: consolehelper checks go here
   */
  return shutdown_types;
}

static void
sms_error_handler (SmsConn       smsConn,
                   Bool          swap,
                   int           offendingMinorOpcode,
                   unsigned long offendingSequence,
                   int           errorClass,
                   int           severity,
                   SmPointer     values)
{
  MsmClient *client;

  client = msm_client_get_by_connection (smsConn);

  msm_warning (_("session management error on client '%s' in state %s:\n"),
               client ? msm_client_get_description (client) : _("(unknown)"),
               client ? msm_client_state_as_string (client) : _("n/a"));

  if (old_sms_error_handler)
    (* old_sms_error_handler) (smsConn, swap,
                               offendingMinorOpcode,
                               offendingSequence,
                               errorClass,
                               severity,
                               values);
}

MsmServer*
msm_server_new (const char *session_name)
{
  MsmSession *session;

  session = msm_session_get (session_name);

  return msm_server_new_with_session (session);
}

MsmServer*
msm_server_new_failsafe (void)
{
  return msm_server_new_with_session (msm_session_get_failsafe ());
}

void
msm_server_free (MsmServer *server)
{
  if (server->client_timeout_handler != 0)
    g_source_remove (server->client_timeout_handler);
  
  g_list_free (server->clients);
  g_list_free (server->interact_pending);  
  
  free_auth_entries (server->auth_entries, server->n_auth_entries);

  if (server->gdm_remote)
    gdm_remote_free (server->gdm_remote);
  
  g_free (server);
}

void
msm_server_shut_down (MsmServer *server)
{
  GError *error = NULL;

  if (server->gdm_remote && gdm_remote_is_authenticated (server->gdm_remote))
    {
      switch (server->post_logout)
        {
          case MSM_SHUTDOWN_TYPE_REBOOT:
            if (gdm_remote_set_logout_action (server->gdm_remote, 
                                              GDM_REMOTE_LOGOUT_REBOOT,
                                              &error))
            break;

          case MSM_SHUTDOWN_TYPE_HALT:
            if (gdm_remote_set_logout_action (server->gdm_remote, 
                                              GDM_REMOTE_LOGOUT_HALT,
                                              &error))
            break;
          default:
            break;
        }

      if (error)
        {
          msm_warning ("Could not shutdown with GDM: %s\n",
                       error->message);
          g_error_free (error);
          error = NULL;
        }
    }

#if 0
  const char **post_logout_command;
  /* FIXME "/usr/bin/poweroff" */
  static const char *shutdown_command[] = { "/bin/ls", NULL };
  /* FIXME "/usr/bin/reboot" */
  static const char *reboot_command[] = { "/bin/date", NULL };
  

  post_logout_command = NULL;
  
  if (server->post_logout == MSM_LOGOUT_REBOOT)
    post_logout_command = reboot_command;
  else if (server->post_logout == MSM_LOGOUT_SHUT_DOWN)
    post_logout_command = shutdown_command;

  if (post_logout_command)
    {
      GError *err = NULL;

      msm_verbose ("Running command '%s' on logout\n",
                   *post_logout_command);
      
      g_spawn_async ("/",
                     (char **) post_logout_command,
                     NULL, 0, NULL, NULL,
                     NULL,
                     &err);

      if (err)
        {
          GtkWidget *dialog;

          msm_verbose ("Logout command failed: %s\n", err->message);
          
          dialog = gtk_message_dialog_new (NULL,
                                           0,
                                           GTK_MESSAGE_ERROR,
                                           GTK_BUTTONS_NONE,
                                           _("Reboot or shutdown was unsuccessful."));

          gtk_dialog_add_button (GTK_DIALOG (dialog),
                                 _("Log out"), GTK_RESPONSE_ACCEPT);
          
          gtk_window_set_position (GTK_WINDOW (dialog),
                                   GTK_WIN_POS_CENTER); 

          dialog_add_details (GTK_DIALOG (dialog),
                              err->message);
          
          g_error_free (err);

          gtk_dialog_run (GTK_DIALOG (dialog));

          gtk_widget_destroy (dialog);
        }
    }
  
#endif
  msm_quit ();
}

void
msm_server_drop_client (MsmServer *server,
                        MsmClient *client)
{
  msm_verbose ("Dropping client '%s'\n",
               msm_client_get_description (client));

  if (server->expired_client == client)
    {
      gtk_widget_destroy (server->client_expired_dialog);
      server->expired_client = NULL;
      server->client_expired_dialog = NULL;
    }
  
  server->clients = g_list_remove (server->clients, client);

  if (server->currently_interacting == client)
    msm_server_next_pending_interaction (server);

  if (!server->in_shutdown)
    {
      switch (msm_client_get_restart_style (client))
        {
        case SmRestartNever:
          msm_session_remove_client (server->session, client);
          break;
        case SmRestartImmediately:
          msm_session_restart_client (server->session, client);
          break;
        default:
          break;
        }
    }
  
  msm_client_free (client);

  msm_server_consider_phase_change (server);

  /* We can quit after all clients have been dropped. */
  if (server->in_shutdown &&
      server->clients == NULL)
    {
      msm_verbose ("Last client dropped, exiting\n");
      msm_server_shut_down (server);
    }
}

void
msm_server_next_pending_interaction (MsmServer *server)
{
  MsmClient *client = NULL;
  MsmClientState state;

  if (server->currently_interacting != NULL)
    {
      msm_client_end_interact (server->currently_interacting);
      server->currently_interacting = NULL;
    }

  while (server->interact_pending)
    {
      client = (MsmClient *) server->interact_pending->data;
      state = msm_client_get_state (client);

      server->interact_pending =
        g_list_remove (server->interact_pending, client);

      if (state == MSM_CLIENT_STATE_INTERACTION_REQUESTED)
        break;

      msm_warning ("Client '%s' is queued for interaction but is not "
                   "requesting it\n", msm_client_get_description (client));

      client = NULL;
      server->interact_pending = server->interact_pending->next;
    }

  if (client)
    {
      /* Start up the next interaction */
      server->currently_interacting = client;

      msm_client_begin_interact (client);
    }
}

static Status
register_client_callback              (SmsConn         cnxn,
                                       SmPointer       manager_data,
                                       char           *previous_id)
{
  /* This callback should:
   *  a) if previous_id is NULL, this is a new client; register
   *     it and return TRUE
   *  b) if previous_id is non-NULL and is an ID we know about,
   *     register client and return TRUE
   *  c) if previous_id is non-NULL and we've never heard of it,
   *     return FALSE
   *
   *  Whenever previous_id is non-NULL we need to free() it.
   *  (What an incredibly broken interface...)
   */
  MsmClient *client;
  MsmServer *server;
  
  client = manager_data;
  server = msm_client_get_server (client);

  msm_verbose ("Client '%s' registering with previous_id = '%s'\n",
               msm_client_get_description (client),
               previous_id ? previous_id : "(null)");
  
  if (previous_id == NULL)
    {
      char *id;

      id = SmsGenerateClientID (msm_client_get_connection (client));

      msm_client_register (client, id);

      free (id);

      /* SM spec requires this initial SaveYourself. */
      msm_client_initial_save (client);
      
      return TRUE;
    }
  else
    {
      /* check for pending/known client IDs and register the client,
       * return TRUE if we know about this previous_id, return FALSE
       * if we do not know about it or it's already being used.   
       */
      if (msm_server_client_id_in_use (server, previous_id) ||
          !msm_session_client_id_known (server->session, previous_id))
        {
          msm_verbose ("Client registration rejected, invalid previous_id\n");
          free (previous_id);
          return FALSE;
        }
      else
        {
          msm_verbose ("Client registration accepted using previous_id\n");
          msm_client_register (client, previous_id);
          msm_session_client_registered (server->session, previous_id);
          free (previous_id);
          return TRUE;
        }
    }
}

static void
interact_request_callback (SmsConn         cnxn,
                           SmPointer       manager_data,
                           int             dialog_type)
{
  MsmClient *client;
  MsmServer *server;
  
  client = manager_data;
  server = msm_client_get_server (client);

  msm_verbose ("Interaction request from client '%s'\n",
               msm_client_get_description (client));
  
  if (!server->save_allows_interaction)
    {
      msm_warning (_("Client '%s' requested interaction, but interaction is not allowed right now.\n"),
                   msm_client_get_description (client));

      return;
    }
  
  msm_client_interact_request (client);
}

static void
interact_done_callback (SmsConn         cnxn,
                        SmPointer       manager_data,
                        Bool            cancel_shutdown)
{
  MsmClient *client;
  MsmServer *server;

  client = manager_data;
  server = msm_client_get_server (client);

  msm_verbose ("Interact done from client '%s', cancel_shutdown = %d\n",
               msm_client_get_description (client),
               cancel_shutdown);
  
  if (cancel_shutdown &&
      server->in_shutdown &&
      server->save_allows_interaction)
    {
      msm_server_cancel_shutdown (server);
    }
  else
    {
      if (server->currently_interacting == client)
        {
          msm_server_next_pending_interaction (server);
        }
      else
        {
          msm_warning (_("Received InteractDone from client '%s' which should not be interacting right now\n"),
                       msm_client_get_description (client));
        }
    }
}

static void
save_yourself_request_callback (SmsConn         cnxn,
                                SmPointer       manager_data,
                                int             save_type,
                                Bool            shutdown,
                                int             interact_style,
                                Bool            fast,
                                Bool            global)
{
  /* The spec says we "may" honor this exactly as requested;
   * we decide not to, because some of the fields are stupid
   * and/or useless
   */
  /* See thread
   * http://mail.gnome.org/archives/gnome-hackers/2001-November/msg00369.html
   * http://mail.gnome.org/archives/gnome-hackers/2001-December/msg00004.html
   *
   * Specifically:
   * 
   *   This means apps really need only three kinds of message:
   *   
   *    a) save current state, but don't ask about saving unsaved 
   *       documents, we aren't logging out. don't worry about 
   *       saving temporary document files; just handle it gracefully 
   *       if files change (e.g. open an empty window if a document 
   *       has disappeared when we restore)
   *    b) we're logging out, so ask about saving documents, but 
   *       don't save session state as user didn't want that
   *    c) we're logging out, so ask about saving documents, but do 
   *       save session state since user requested it
   *   
   *   So the question is how do we encode those in the flags actually passed
   *   to SaveYourself. SaveYourself has fast = TRUE/FALSE, shutdown =
   *   TRUE/FALSE, and style = Local/Global/Both. That allows 
   *   12 different messages, right, and we only need 3. ;-)
   *   
   *   I think your suggestion is:
   *    Local = a)
   *    Global = b)
   *    Both = c)
   *   
   *   And shutdown/fast are just hints that are mostly ignored, I guess.
   *   
   *   Then we also have to specify that if the style includes Global,
   *   interaction must be allowed; Global save with interact = None would be
   *   nonsense.
   * 
   * Note that Global save style != the global bool; the above holds when
   * the global bool is True. global bool of False means just save the
   * one app.
   * 
   */
  MsmClient *client;
  MsmServer *server;

  client = manager_data;
  server = msm_client_get_server (client);

  msm_verbose ("Save yourself request from client '%s', global = %d interact_style = %d shutdown = %d save_type = %d fast = %d\n",
               msm_client_get_description (client),
               global, interact_style, shutdown,
               save_type, fast);
  
  if (global)
    {
      if (save_type == SmSaveLocal)
        {
          if (interact_style == SmInteractStyleNone)
            msm_server_save_all (server, MSM_SAVE_JUST_SAVE_NO_INTERACTION);
          else
            msm_server_save_all (server, MSM_SAVE_JUST_SAVE_WITH_INTERACTION);
        }
      else if (save_type == SmSaveGlobal)
        msm_server_save_all (server, MSM_SAVE_JUST_LOGOUT);
      else if (save_type == SmSaveBoth)
        msm_server_save_all (server, MSM_SAVE_LOGOUT_AND_SAVE);
      else
        msm_warning ("Unknown save_type %d from client '%s'\n",
                     save_type, msm_client_get_description (client));
    }
  else
    {
      if (msm_client_get_state (client) == MSM_CLIENT_STATE_IDLE)
        msm_client_save (client,
                         interact_style != SmInteractStyleNone,
                         shutdown != FALSE);
      else
        msm_warning (_("Client '%s' requested save, but is not currently in the idle state\n"),
                     msm_client_get_description (client));
    }
}

static void
save_yourself_phase2_request_callback (SmsConn         cnxn,
                                       SmPointer       manager_data)
{
  MsmClient *client;
  MsmServer *server;

  client = manager_data;
  server = msm_client_get_server (client);

  msm_verbose ("Phase2 request from client '%s'\n",
               msm_client_get_description (client));
  
  msm_client_phase2_request (client);

  msm_server_consider_phase_change (server);
}

static void
save_yourself_done_callback           (SmsConn         cnxn,
                                       SmPointer       manager_data,
                                       Bool            success)
{
  MsmClient *client;
  MsmServer *server;

  client = manager_data;
  server = msm_client_get_server (client);

  msm_verbose ("SaveYourselfDone from client '%s'\n",
               msm_client_get_description (client));
  
  msm_client_save_confirmed (client, success != FALSE);
  
  msm_server_consider_phase_change (server);
}

static void
close_connection_callback             (SmsConn         cnxn,
                                       SmPointer       manager_data,
                                       int             count,
                                       char          **reason_msgs)
{
  MsmClient *client;
  MsmServer *server;

  client = manager_data;
  server = msm_client_get_server (client);

  msm_verbose ("Close connection from client '%s'\n",
               msm_client_get_description (client));
  {
    int i;
    i = 0;
    while (i < count)
      {
        msm_verbose (" close reason: '%s'\n", reason_msgs[i]);
        ++i;
      }
  }
  
  msm_server_drop_client (server, client);

  /* I'm assuming these messages would be on crack, and therefore not
   * displaying them.
   */
  SmFreeReasons (count, reason_msgs);
}

static void
set_properties_callback (SmsConn     cnxn,
                         SmPointer   manager_data,
                         int         num_props,
                         SmProp    **props)
{
  int i;
  MsmClient *client;
  MsmServer *server;

  client = manager_data;
  server = msm_client_get_server (client);

  msm_verbose ("Set properties from client '%s'\n",
               msm_client_get_description (client));

  /* If RestartCommand is set, we need to run
   * the *old* DiscardCommand. (Not the new one
   * in this props array.)
   */
  i = 0;
  while (i < num_props)
    {
      if (strcmp (props[i]->name, SmRestartCommand) == 0)        
        {
          msm_verbose ("  RestartCommand changed, running DiscardCommand\n");
          msm_run_discard (msm_client_get_description (client),
                           msm_client_get_proplist (client));
          break;
        }
      
      ++i;
    }
  
  i = 0;
  while (i < num_props)
    {
      msm_verbose ("  set prop '%s'\n", props[i]->name);
      
      msm_client_set_property_taking_ownership (client, props[i]);

      /* Client owns it, so don't do this. */
      /* SmFreeProperty (props[i]);  */ 

      ++i;
    }

  free (props);
}

static void
delete_properties_callback (SmsConn     cnxn,
                            SmPointer   manager_data,
                            int         num_props,
                            char      **prop_names)
{
  int i;
  MsmClient *client;
  MsmServer *server;

  client = manager_data;
  server = msm_client_get_server (client);

  msm_verbose ("Delete properties from '%s'\n",
               msm_client_get_description (client));
  
  i = 0;
  while (i < num_props)
    {
      msm_verbose ("  delete prop '%s'\n", prop_names[i]);
      
      msm_client_unset_property (client, prop_names[i]);

      ++i;
    }
}

static void
get_properties_callback (SmsConn     cnxn,
                         SmPointer   manager_data)
{
  MsmClient *client;
  MsmServer *server;

  client = manager_data;
  server = msm_client_get_server (client);

  msm_verbose ("Get properties request from '%s'\n",
               msm_client_get_description (client));
  
  msm_client_send_properties (client);
}

static Status
new_client_callback                   (SmsConn         cnxn,
                                       SmPointer       manager_data,
                                       unsigned long  *mask_ret,
                                       SmsCallbacks   *callbacks_ret,
                                       char          **failure_reason_ret)
{
  MsmClient *client;
  MsmServer *server;
  SmProp *prop;

  server = manager_data;

  msm_verbose ("New client connecting\n");
  
  /* If we want to disallow the new client, here we fill
   * failure_reason_ret with a malloc'd string and return FALSE
   */
  if (server->in_shutdown)
    {
      msm_verbose ("In shutdown, rejecting new client\n");
      
      *failure_reason_ret =
        msm_non_glib_strdup (_("Refusing new client connection because the session is currently being shut down\n"));
      return FALSE;
    }
  
  client = msm_client_new (server, cnxn);
  server->clients = g_list_prepend (server->clients, client);

  msm_verbose ("New client is '%s'\n",
               msm_client_get_description (client));

  prop = smprop_new_card8 (MSM_SERVER_PROPERTY_SHUTDOWN_TYPE, 
                           (guint8) server->shutdown_types);

  msm_client_set_property_taking_ownership (client, prop);
  
  *mask_ret = 0;
  
  *mask_ret |= SmsRegisterClientProcMask;
  callbacks_ret->register_client.callback = register_client_callback;
  callbacks_ret->register_client.manager_data  = client;

  *mask_ret |= SmsInteractRequestProcMask;
  callbacks_ret->interact_request.callback = interact_request_callback;
  callbacks_ret->interact_request.manager_data = client;

  *mask_ret |= SmsInteractDoneProcMask;
  callbacks_ret->interact_done.callback = interact_done_callback;
  callbacks_ret->interact_done.manager_data = client;

  *mask_ret |= SmsSaveYourselfRequestProcMask;
  callbacks_ret->save_yourself_request.callback = save_yourself_request_callback;
  callbacks_ret->save_yourself_request.manager_data = client;

  *mask_ret |= SmsSaveYourselfP2RequestProcMask;
  callbacks_ret->save_yourself_phase2_request.callback = save_yourself_phase2_request_callback;
  callbacks_ret->save_yourself_phase2_request.manager_data = client;

  *mask_ret |= SmsSaveYourselfDoneProcMask;
  callbacks_ret->save_yourself_done.callback = save_yourself_done_callback;
  callbacks_ret->save_yourself_done.manager_data = client;

  *mask_ret |= SmsCloseConnectionProcMask;
  callbacks_ret->close_connection.callback = close_connection_callback;
  callbacks_ret->close_connection.manager_data  = client;

  *mask_ret |= SmsSetPropertiesProcMask;
  callbacks_ret->set_properties.callback = set_properties_callback;
  callbacks_ret->set_properties.manager_data = client;

  *mask_ret |= SmsDeletePropertiesProcMask;
  callbacks_ret->delete_properties.callback = delete_properties_callback;
  callbacks_ret->delete_properties.manager_data = client;

  *mask_ret |= SmsGetPropertiesProcMask;
  callbacks_ret->get_properties.callback = get_properties_callback;
  callbacks_ret->get_properties.manager_data = client;

  return TRUE;
}

static Bool
host_auth_callback (char *hostname)
{

  /* not authorized */
  return False;
}

void
msm_server_queue_interaction (MsmServer *server,
                              MsmClient *client)
{
  msm_verbose ("Queuing an interaction for '%s'\n",
               msm_client_get_description (client));
  
  if (server->currently_interacting == client ||
      g_list_find (server->interact_pending, client) != NULL)
    return;   /* Already queued */  

  server->interact_pending = g_list_prepend (server->interact_pending,
                                             client);

  msm_server_next_pending_interaction (server);
}

static const char*
msm_save_all_mode_to_string (MsmSaveAllMode mode)
{
  switch (mode)
    {
    case MSM_SAVE_JUST_SAVE_NO_INTERACTION:
      return "JUST_SAVE_NO_INTERACTION";
    case MSM_SAVE_JUST_SAVE_WITH_INTERACTION:
      return "JUST_SAVE_WITH_INTERACTION";
    case MSM_SAVE_JUST_LOGOUT:
      return "JUST_LOGOUT";
    case MSM_SAVE_LOGOUT_AND_SAVE:
      return "LOGOUT_AND_SAVE";
    }
  return "(unknown)";
}

void
msm_server_save_all (MsmServer     *server,
                     MsmSaveAllMode mode)
{
  GList *tmp;

  msm_verbose ("Saving session mode = %s\n",
               msm_save_all_mode_to_string (mode));

  if (mode == MSM_SAVE_JUST_SAVE_NO_INTERACTION)
    server->save_allows_interaction = FALSE;
  else
    server->save_allows_interaction = TRUE;

  if (mode == MSM_SAVE_JUST_LOGOUT ||
      mode == MSM_SAVE_LOGOUT_AND_SAVE) /* never cancel a shutdown here */
    server->in_shutdown = TRUE;

  if (mode == MSM_SAVE_JUST_LOGOUT)
    server->user_disabled_save = TRUE;
  else
    server->user_disabled_save = FALSE;
  
  server->in_global_save = TRUE;

  /* The reentrancy here is supposed to be safe, because
   * we'll ignore any attempts to save during a shut down,
   * and other things (such as dropping a client) should not
   * cause any issues.
   */
  if (server->in_shutdown && server->save_allows_interaction)
    ask_user_about_logout (server); /* NOTE REENTRANCY HERE - THIS RUNS MAIN LOOP */

  /* Shutdown may have been cancelled during reentrancy, so check
   * that we're still in the save
   */
  if (server->in_global_save)
    {
      tmp = server->clients;
      while (tmp != NULL)
        {
          MsmClient *client;
          
          client = tmp->data;
          
          if (msm_client_get_state (client) == MSM_CLIENT_STATE_IDLE)
            {
              if (mode == MSM_SAVE_JUST_LOGOUT)
                msm_client_die (client);
              else
                msm_client_save (client,
                                 server->save_allows_interaction,
                                 server->in_shutdown);
            }
          
          tmp = tmp->next;
        }
    }
}

void
msm_server_cancel_shutdown (MsmServer *server)
{
  GList *tmp;

  msm_verbose ("Cancelling shutdown\n");
  
  if (!server->in_shutdown)
    return;

  server->in_global_save = FALSE;
  server->in_shutdown = FALSE;
  server->post_logout = MSM_SHUTDOWN_TYPE_CANCEL;
  server->user_disabled_save = FALSE;
  
  /* Cancel any interactions in progress */
  g_list_free (server->interact_pending);
  server->interact_pending = NULL;
  server->currently_interacting = NULL;

  tmp = server->clients;
  while (tmp != NULL)
    {
      MsmClient *client;

      client = tmp->data;

      switch (msm_client_get_state (client))
        {
        case MSM_CLIENT_STATE_SAVING:
        case MSM_CLIENT_STATE_INTERACTION_REQUESTED:
        case MSM_CLIENT_STATE_INTERACT:
        case MSM_CLIENT_STATE_INTERACTION_DONE:
        case MSM_CLIENT_STATE_PHASE2_REQUESTED:
        case MSM_CLIENT_STATE_SAVING_PHASE2:
        case MSM_CLIENT_STATE_SAVE_DONE:
        case MSM_CLIENT_STATE_SAVE_FAILED:
          if (msm_client_get_in_shutdown (client))
            msm_client_shutdown_cancelled (client);
          break;
        default:
          break;
        }
      
      tmp = tmp->next;
    }
}

/* Think about whether to move to phase 2, return to idle state,
 * save session to disk, or shut down
 */
void
msm_server_consider_phase_change (MsmServer *server)
{
  GList *tmp;
  gboolean some_phase1;
  gboolean some_phase2;
  gboolean some_phase2_requested;
  gboolean some_alive;
  
  some_phase1 = FALSE;
  some_phase2 = FALSE;
  some_phase2_requested = FALSE;
  some_alive = FALSE;
  
  tmp = server->clients;
  while (tmp != NULL)
    {
      MsmClient *client;

      client = tmp->data;
      
      switch (msm_client_get_state (client))
        {
        case MSM_CLIENT_STATE_SAVING:
          some_phase1 = TRUE;
          break;
        case MSM_CLIENT_STATE_SAVING_PHASE2:
          some_phase2 = TRUE;
          break;
        case MSM_CLIENT_STATE_PHASE2_REQUESTED:
          some_phase2_requested = TRUE;
          break;
        default:
          break;
        }

      if (msm_client_get_state (client) != MSM_CLIENT_STATE_DEAD)
        some_alive = TRUE;
      
      tmp = tmp->next;
    }
  
  if (some_phase1)
    {
      msm_verbose ("Some clients still pending in phase 1 save\n");
      handle_timeouts (server);
      return; /* still saving phase 1 */
    }

  if (some_phase2)
    {
      msm_verbose ("Some clients still pending in phase 2 save\n");
      handle_timeouts (server);
      return; /* we are in phase 2 */
    }

  if (some_phase2_requested)
    {
      msm_verbose ("Some clients requested phase 2 save\n");
      
      tmp = server->clients;
      while (tmp != NULL)
        {
          MsmClient *client;
          
          client = tmp->data;
          
          if (msm_client_get_state (client) == MSM_CLIENT_STATE_PHASE2_REQUESTED)
            msm_client_save_phase2 (client);
          
          tmp = tmp->next;
        }

      handle_timeouts (server);
      
      return;
    }

  if (server->in_global_save)
    {
      GList *tmp;
      
      msm_verbose ("All clients done, saving session\n");
      
      tmp = server->clients;
      while (tmp != NULL)
        {
          MsmClient *client = tmp->data;

          switch (msm_client_get_state (client))
            {
            case MSM_CLIENT_STATE_SAVE_DONE:
              /* Update it in the session, since it saved successfully.
               * But for RestartNever don't bother putting them
               * in the session.
               */
              if (msm_client_get_restart_style (client) != SmRestartNever)
                {
                  msm_verbose ("Client '%s' saved OK, updating in session\n",
                               msm_client_get_description (client));
                  msm_session_update_client (server->session, client);
                }
              else
                {
                  msm_verbose ("Client '%s' is SmRestartNever, not saving\n",
                               msm_client_get_description (client));
                }
              break;
            default:
              break;
            }
          
          tmp = tmp->next;
        }

      /* Purge clients that are RestartNever, or RestartIfRunning clients
       * that are not currently connected
       */
      msm_session_purge_clients (server->session, server);
      
      /* Write to disk, unless user unchecked the "save session" box */
      if (server->user_disabled_save)
        msm_verbose ("Not actually writing state to disk, because user didn't ask to save session\n");
      else
        msm_session_save (server->session, server);
    }

  /* msm_session_save() may have cancelled any shutdown that was in progress,
   * so don't assume here that we are still in_global_save or in_shutdown.
   * Also, client states may have changed.
   */
  
  if (server->in_shutdown)
    {      
      /* We are shutting down, and all clients are in the idle state.
       * Tell all clients to die. When they all close their connections,
       * we can exit.
       */
      
      if (some_alive)
        {
          msm_verbose ("Asking remaining clients to exit\n");
          
          tmp = server->clients;
          while (tmp != NULL)
            {
              MsmClient *client = tmp->data;
              
              if (msm_client_get_state (client) != MSM_CLIENT_STATE_DEAD)
                msm_client_die (client);
              
              tmp = tmp->next;
            }
        }

      /* Flip this off, so that we don't save again prior to shutdown.
       * But leave in_shutdown on, so we don't allow connections, etc.
       */
      server->in_global_save = FALSE;
    }
  else if (server->in_global_save)
    {
      /* send SaveComplete to all clients that are finished saving */
      GList *tmp;

      msm_verbose ("Notifying clients of completed save\n");
      
      tmp = server->clients;
      while (tmp != NULL)
        {
          MsmClient *client = tmp->data;

          switch (msm_client_get_state (client))
            {
            case MSM_CLIENT_STATE_SAVE_DONE:
            case MSM_CLIENT_STATE_SAVE_FAILED:
              msm_client_save_complete (client);
              break;
            default:
              break;
            }
          
          tmp = tmp->next;
        }

      /* Leave in_global_save state */
      server->in_global_save = FALSE;
    }

  handle_timeouts (server);
}

void
msm_server_foreach_client (MsmServer *server,
                           MsmClientFunc func)
{
  GList *tmp;
  
  tmp = server->clients;
  while (tmp != NULL)
    {
      MsmClient *client = tmp->data;

      (* func) (client);

      tmp = tmp->next;
    }
}

gboolean
msm_server_client_id_in_use (MsmServer  *server,
                             const char *id)
{
  GList *tmp;
  
  tmp = server->clients;
  while (tmp != NULL)
    {
      MsmClient *client = tmp->data;
      const char *cid;

      cid = msm_client_get_id (client);
      
      if (cid && strcmp (cid, id) == 0)
        return TRUE;

      tmp = tmp->next;
    }

  return FALSE;
}

void
msm_server_launch_session (MsmServer *server)
{
  msm_session_launch (server->session);
}

gboolean
msm_server_in_shutdown (MsmServer *server)
{
  return server->in_shutdown;
}

enum
{
  RESPONSE_KEEP_WAITING = 1,
  RESPONSE_DISABLE_CLIENT = 2
};

static void
expired_dialog_callback (GtkDialog *dialog,
                         int        response_id,
                         gpointer   data)
{
  MsmServer *server;

  server = data;

  if (response_id == RESPONSE_DISABLE_CLIENT)
    msm_client_disable (server->expired_client);
  else
    msm_client_keep_waiting (server->expired_client);

  server->expired_client = NULL;
  server->client_expired_dialog = NULL;
  gtk_widget_destroy (GTK_WIDGET (dialog));

  /* See if any other clients are being pesky */
  handle_timeouts (server);
}

static gboolean
client_timeout_handler (gpointer data)
{
  MsmServer *server;

  server = data;

  handle_timeouts (server);

  return FALSE;
}

static void
handle_timeouts (MsmServer *server)
{
  GList *tmp;
  MsmClient *expired;
  GTime now;
  int min_pending;

  msm_verbose ("Checking on client timeouts\n");
  
  if (server->client_timeout_handler != 0)
    {
      g_source_remove (server->client_timeout_handler);
      server->client_timeout_handler = 0;
    }

  now = time (NULL);
  expired = NULL;
  min_pending = G_MAXINT;
  
  tmp = server->clients;
  while (tmp != NULL)
    {
      MsmClient *client = tmp->data;
      int will_expire;
      
      /* We handle only one expired client at a time
       * via the GUI
       */
      if (expired == NULL &&
          msm_client_timed_out (client, now))
        {
          msm_verbose ("Client '%s' in state %s is timed out\n",
                       msm_client_get_description (client),
                       msm_client_state_as_string (client));
          expired = client;
        }

      will_expire = msm_client_timeout_time (client, now);
      
      if (will_expire >= 0)
        {
          msm_verbose ("Client '%s' in state %s will time out in %d seconds\n",
                       msm_client_get_description (client),
                       msm_client_state_as_string (client),
                       will_expire);
          min_pending = MIN (will_expire, min_pending);
        }
      else
        {
          msm_verbose ("Client '%s' in state %s will not expire\n",
                       msm_client_get_description (client),
                       msm_client_state_as_string (client));
        }
      
      tmp = tmp->next;
    }

  if (expired == NULL)
    msm_verbose ("No clients expired, currently expired: %s\n",
                 server->expired_client ?
                 msm_client_get_description (server->expired_client) :
                 "none");

  /* if expired != NULL, then once our dialog is closed we'll
   * handle any additional timeouts (see expired_dialog_callback)
   */
  if (expired == NULL &&
      min_pending < G_MAXINT)
    {
      g_assert (min_pending >= 0);
      
      msm_verbose ("Adding timeout handler for %d msecs\n",
                   min_pending);
      server->client_timeout_handler = g_timeout_add (min_pending,
                                                      client_timeout_handler,
                                                      server);
    }
  
  if (expired && server->expired_client == NULL)
    {
      char *name;
      char *details;
      
      server->expired_client = expired;

      msm_verbose ("Showing dialog for expired client '%s' in state %s\n",
                   msm_client_get_description (server->expired_client),
                   msm_client_state_as_string (server->expired_client));
      
      name = msm_client_get_user_visible_name (server->expired_client);

      if (name)
        server->client_expired_dialog =
          gtk_message_dialog_new (NULL,
                                  0,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_NONE,
                                  _("Application '%s' is not responding. Keep waiting for it to respond, or ignore it for now?"),
                                  name);
      else
        server->client_expired_dialog =
          gtk_message_dialog_new (NULL,
                                  0,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_NONE,
                                  _("An application is not responding. Keep waiting for it to respond, or ignore it for now?"));

      g_free (name);
      
      gtk_window_set_position (GTK_WINDOW (server->client_expired_dialog),
                               GTK_WIN_POS_CENTER);

      gtk_dialog_add_buttons (GTK_DIALOG (server->client_expired_dialog),
                              _("_Keep waiting"),
                              RESPONSE_KEEP_WAITING,
                              _("_Ignore it"),
                              RESPONSE_DISABLE_CLIENT,
                              NULL);

      gtk_dialog_set_default_response (GTK_DIALOG (server->client_expired_dialog),
                                       RESPONSE_KEEP_WAITING);

      details = msm_client_get_timeout_details (server->expired_client);
      if (details)
        dialog_add_details (GTK_DIALOG (server->client_expired_dialog),
                            details);
      g_free (details);
      
      g_signal_connect (G_OBJECT (server->client_expired_dialog),
                        "response",
                        G_CALLBACK (expired_dialog_callback),
                        server);

      gtk_widget_show (server->client_expired_dialog);
    }
}

static void
ask_user_about_logout (MsmServer *server)
{
  GtkWidget *dialog;
  MsmShutdownTypeFlags action, selected_shutdown_type;
  gboolean save_session = FALSE;
  int response;

  dialog = msm_shutdown_dialog_new (server->shutdown_types);

  if (server->post_logout != MSM_SHUTDOWN_TYPE_CANCEL)
    selected_shutdown_type = server->post_logout;
  else
    selected_shutdown_type = MSM_SHUTDOWN_TYPE_LOGOUT;

  g_object_set (G_OBJECT (dialog), 
                "selected-shutdown-type", selected_shutdown_type,
                "save-session", save_session,
                NULL);

  response = gtk_dialog_run (GTK_DIALOG (dialog));

  action = (MsmShutdownTypeFlags) response;

  save_session = 
    msm_shutdown_dialog_get_save_session (MSM_SHUTDOWN_DIALOG (dialog));

  gtk_widget_destroy (dialog);

  if (response == GTK_RESPONSE_NONE 
      || action == MSM_SHUTDOWN_TYPE_CANCEL)
    {
      msm_server_cancel_shutdown (server);
      return;
    }

  server->post_logout = action;

  server->user_disabled_save = !save_session;
}

static void
mark_ice_connection_closed (MsmServer     *server,
                            IceConn        connection)
{
  GList *tmp;
      
  msm_verbose ("Marking ICE connection on descriptor %d closed\n",
               IceConnectionNumber (connection));

  tmp = server->clients;
  while (tmp != NULL)
    {
      MsmClient *client = tmp->data;

      if (SmsGetIceConnection (msm_client_get_connection (client)) ==
          connection)
        msm_client_mark_ice_connection_gone (client);
          
      tmp = tmp->next;
    }
}

/*
 * ICE utility code, cut-and-pasted from Metacity, and in turn
 * from libgnomeui, and also some merged in from gsm, and xsm,
 * and even ksm
 */

static void ice_io_error_handler (IceConn connection);

static void new_ice_connection (IceConn connection, IcePointer client_data, 
				Bool opening, IcePointer *watch_data);

typedef struct _ProcessMessagesData ProcessMessagesData;
struct _ProcessMessagesData
{
  MsmServer *server;
  IceConn connection;
};

/* This is called when data is available on an ICE connection.  */
static gboolean
process_ice_messages (GIOChannel *channel,
                      GIOCondition condition,
                      gpointer client_data)
{
  ProcessMessagesData *pmd = client_data;
  IceConn connection = pmd->connection;
  IceProcessMessagesStatus status;
  
  int has_data, fd;
  fd_set fd_set;
  struct timeval poll_timeout = {0};
  
  /* FIXME: This callback sometimes gets called when there are no new messages.
   * (Maybe they are getting processed somewhere else also and there is a race 
   * condition?).  This causes IceProcessMessages to block until there is a
   * new message.  The solution for now is to poll the fd and check to see  
   * if there is data waiting.
   */
  fd = IceConnectionNumber (connection);
  FD_ZERO (&fd_set);
  FD_SET (fd, &fd_set);

  has_data = select (fd + 1, &fd_set, NULL, NULL, &poll_timeout); 

  if (!has_data) 
    {
       msm_verbose ("Message processing invoked, but no data on socket\n");
       return TRUE;
    }

  status = IceProcessMessages (connection, NULL, NULL);

  if (status == IceProcessMessagesIOError)
    {      
      /* We were disconnected */
      mark_ice_connection_closed (pmd->server, connection);
      IceSetShutdownNegotiation (connection, False);
      IceCloseConnection (connection);
      
      return FALSE;
    }
  else if (status == IceProcessMessagesConnectionClosed)
    {
      /* connection is now invalid */
      mark_ice_connection_closed (pmd->server, connection);
      
      return FALSE;
    }

  return TRUE;
}

typedef struct _WatchData WatchData;
struct _WatchData
{
  guint input_handler;
  MsmServer *server;
};

/* This is called when a new ICE connection is made and again when
 * it's closed. ICE connections may be used by multiple clients. The
 * function arranges for the ICE connection to be handled or stop
 * being handled via the event loop.
 */
static void
new_ice_connection (IceConn connection, IcePointer client_data, Bool opening,
		    IcePointer *watch_data)
{
  WatchData *wd;
  MsmServer *server;

  server = client_data;
  
  if (opening)
    {
      /* Make sure we don't pass on these file descriptors to any
       * exec'ed children
       */
      GIOChannel *channel;
      guint input_id;
      ProcessMessagesData *pmd;
      
      msm_verbose ("New ICE connection on descriptor %d\n",
                   IceConnectionNumber (connection));
      
      PRINT_TIME ("new connection");
      
      fcntl (IceConnectionNumber (connection), F_SETFD,
             fcntl (IceConnectionNumber (connection), F_GETFD, 0) | FD_CLOEXEC);

      channel = g_io_channel_unix_new (IceConnectionNumber (connection));

      pmd = g_new (ProcessMessagesData, 1);
      pmd->server = server;
      pmd->connection = connection;
      
      input_id = g_io_add_watch_full (channel,
                                      G_PRIORITY_DEFAULT,
                                      G_IO_IN | G_IO_ERR,
                                      process_ice_messages,
                                      pmd,
                                      (GDestroyNotify) g_free);

      g_io_channel_unref (channel);

      wd = g_new (WatchData, 1);
      wd->input_handler = input_id;
      wd->server = server;
      
      *watch_data = wd;
    }
  else 
    {
      GList *tmp;
      GList *dead;
      
      msm_verbose ("Closing ICE connection on descriptor %d\n",
                   IceConnectionNumber (connection));
      
      PRINT_TIME ("connection closed");
      
      wd = (gpointer) *watch_data;

      mark_ice_connection_closed (wd->server, connection);
      
      dead = NULL;
      tmp = wd->server->clients;
      while (tmp != NULL)
        {
          MsmClient *client = tmp->data;

          if (SmsGetIceConnection (msm_client_get_connection (client)) ==
              connection)
            dead = g_list_prepend (dead, client);
          
          tmp = tmp->next;
        }

      tmp = dead;
      while (tmp != NULL)
        {
          MsmClient *client = tmp->data;
          
          msm_verbose ("Client %s determined to be dead because its ICE connection is closing\n",
                       msm_client_get_description (client));
          
          msm_server_drop_client (server, client);
          
          tmp = tmp->next;
        }

      g_list_free (dead);
      
      g_source_remove (wd->input_handler);
      g_free (wd);
    }

  PRINT_TIME ("leaving new_connection");
}

typedef struct _AcceptData AcceptData;
struct _AcceptData
{
  IceListenObj listen_obj;
  MsmServer *server;
};

static gboolean
accept_connection (GIOChannel *channel,
                   GIOCondition condition,
                   gpointer client_data)
{
  AcceptData *ad = client_data;
  IceListenObj listen_obj;
  IceAcceptStatus status;
  IceConnectStatus cstatus;
  IceProcessMessagesStatus pstatus;
  IceConn cnxn;

  PRINT_TIME ("begin accept connection");
  
  listen_obj = ad->listen_obj;

  cnxn = IceAcceptConnection (listen_obj,
                              &status);
  
  if (cnxn == NULL || status != IceAcceptSuccess)
    {
      msm_warning (_("Failed to accept new ICE connection\n"));
      return TRUE;
    }
  
  /* I believe this means we refuse to argue with clients over
   * whether we are going to shut their ass down. But I could
   * be wrong.
   */
  IceSetShutdownNegotiation (cnxn, False);

  /* FIXME This is a busy wait, I believe. The libSM docs say we need
   * to select on all the ICE file descriptors. We could do that by
   * reentering the main loop as gnome-session does; but that would
   * complicate everything.  So for now, copying ksm and doing it this
   * way.
   *
   * If this causes problems, we can try adding a g_main_iteration()
   * in here.
   *
   * FIXME time this out eventually
   */
  cstatus = IceConnectionStatus (cnxn);
  while (cstatus == IceConnectPending)
    {
      pstatus = IceProcessMessages (cnxn, NULL, NULL);

      if (pstatus == IceProcessMessagesIOError)
        {      
          /* We were disconnected */
          mark_ice_connection_closed (ad->server, cnxn);
          IceCloseConnection (cnxn);
          cnxn = NULL;
          break;
        }
      else if (pstatus == IceProcessMessagesConnectionClosed)
        {
          /* cnxn is now invalid */
          mark_ice_connection_closed (ad->server, cnxn);
          cnxn = NULL;
          break;
        }

      cstatus = IceConnectionStatus (cnxn);
    }

  if (cstatus != IceConnectAccepted)
    {
      if (cstatus == IceConnectIOError)
        msm_warning (_("IO error trying to accept new connection (client may have crashed trying to connect to the session manager, or client may be broken, or someone yanked the ethernet cable)\n"));
      else
        msm_warning (_("Rejecting new connection (some client was not allowed to connect to the session manager)\n"));

      if (cnxn)
        {
          mark_ice_connection_closed (ad->server, cnxn);
          IceCloseConnection (cnxn);
        }
    }

  PRINT_TIME ("end accept connection");
  
  return TRUE;
}

static void
ice_io_error_handler (IceConn connection)
{
  /* do nothing; next IceProcessMessages on the connection wil get
   * an IOError resulting in us calling IceCloseConnection()
   */
}

static void
ice_error_handler (IceConn       connection,
                   Bool          swap,
                   int           offending_minor_opcode,
                   unsigned long offending_sequence,
                   int           error_class,
                   int           severity,
                   IcePointer    values)
{
  /* The default error handler would give a more informative message
   * than we are, but it would also exit most of the time, which
   * is not appropriate here.
   *
   * gnome-session closes the connection in here, but I'm not sure that
   * counts as a good idea. the default handler doesn't do that.
   */

  msm_warning (_("ICE error received, opcode: %d sequence: %lu class: %d severity: %d\n"),
               offending_minor_opcode,
               offending_sequence,
               error_class,
               severity);
}


static void
ice_init (MsmServer *server)
{
  static gboolean ice_initted = FALSE;

  if (! ice_initted)
    {
      int saved_umask;
      int n_listen_objs;
      IceListenObj *listen_objs;
      char errbuf[256];
      char *ids;
      char *p;
      int i;

      PRINT_TIME ("Initializing ICE");
      
      IceSetIOErrorHandler (ice_io_error_handler);
      IceSetErrorHandler (ice_error_handler);
      
      IceAddConnectionWatch (new_ice_connection, server);

      /* Some versions of IceListenForConnections have a bug which causes
       * the umask to be set to 0 on certain types of failures.  So we
       * work around this by saving and restoring the umask.
       */
      saved_umask = umask (0);
      umask (saved_umask);

      PRINT_TIME ("Before IceListenForConnections");
      if (!IceListenForConnections (&n_listen_objs,
                                    &listen_objs,
                                    sizeof (errbuf),
                                    errbuf))
        msm_fatal (_("Could not initialize ICE: %s\n"), errbuf);

      PRINT_TIME ("After IceListenForConnections");
      
      /* See above.  */
      umask (saved_umask);

      i = 0;
      while (i < n_listen_objs)
        {
          GIOChannel *channel;
          AcceptData *ad;
          
          channel = g_io_channel_unix_new (IceGetListenConnectionNumber (listen_objs[i]));

          ad = g_new (AcceptData, 1);
          ad->listen_obj = listen_objs[i];
          ad->server = server;
          
          g_io_add_watch_full (channel, G_PRIORITY_DEFAULT,
                               G_IO_IN,
                               accept_connection,
                               ad,
                               (GDestroyNotify) g_free);

          g_io_channel_unref (channel);
          
          ++i;
        }

      PRINT_TIME ("Setting up ICE authentication");
      
      if (!create_auth_entries (server, listen_objs, n_listen_objs))
        {
          msm_fatal (_("Could not set up authentication\n"));
          return;
        }
      
      ids = IceComposeNetworkIdList (n_listen_objs, listen_objs);
      
      p = g_strconcat ("SESSION_MANAGER=", ids, NULL);
      putenv (p);

      /* example code I can find doesn't free "ids", and we don't free "p"
       * since putenv is lame
       */
      
      ice_initted = TRUE;
    }
}

/* Create iceauth scripts for adding and removing our
 * auth entries; call those on startup and on exit.
 */
static GString *add_script = NULL;
static GString *remove_script = NULL;

static gboolean
run_iceauth_script (GString *script)
{
  char *argv[4];
  GError *err;
  int status;
  int child_stdin;
  int child_pid;
  int ret;

  PRINT_TIME ("Running iceauth script");
  
  argv[0] = (char*) "iceauth";
  argv[1] = (char*) "source";
  argv[2] = (char*) "-"; /* stdin */
  argv[3] = NULL;
  
  err = NULL;
  status = 1;
  child_stdin = -1;
  if (!g_spawn_async_with_pipes (NULL, argv, NULL,
                                 G_SPAWN_SEARCH_PATH |
                                 G_SPAWN_DO_NOT_REAP_CHILD,
                                 NULL, NULL,
                                 &child_pid,
                                 &child_stdin,
                                 NULL, NULL,
                                 &err))
    {
      msm_warning (_("Failed to run 'iceauth': %s\n"),
                   err->message);

      g_error_free (err);

      return FALSE;
    }

 write_again:
  if (write (child_stdin, script->str, script->len) < 0)
    {
      if (errno == EINTR)
        goto write_again;
      else
        {
          msm_warning (_("Failed to write iceauth script: %s\n"),
                       g_strerror (errno));

          close (child_stdin);
          
          return FALSE;
        }
    }

  close (child_stdin);
  
 waitpid_again:
  ret = waitpid (child_pid, &status, 0);

  if (ret < 0)
    {
      if (errno == EINTR)
        goto waitpid_again;
      else
        {
          msm_warning (_("Unexpected error in waitpid() executing iceauth (%s)\n"),
                       g_strerror (errno));
          
          return FALSE;
        }
    }
  
  PRINT_TIME ("Done running iceauth script");
  
  if (WIFEXITED (status))
    {
      if (WEXITSTATUS (status) == 0)
        return TRUE;
      else
        {
          msm_warning (_("iceauth exited with failure status %d\n"),
                       WEXITSTATUS (status));
          return FALSE;
        }
    }
  else if (WIFSIGNALED (status))
    {
      msm_warning (_("iceauth exited abnormally with signal %d\n"),
                   WTERMSIG (status));
      return FALSE;
    }
  else
    {
      /* should not happen */
      msm_warning (_("iceauth exited abnormally, unknown error\n"));
      return FALSE;
    }
}

/* Goofy thing from xsm */
static char *hex_table[] = {
    "00", "01", "02", "03", "04", "05", "06", "07", 
    "08", "09", "0a", "0b", "0c", "0d", "0e", "0f", 
    "10", "11", "12", "13", "14", "15", "16", "17", 
    "18", "19", "1a", "1b", "1c", "1d", "1e", "1f", 
    "20", "21", "22", "23", "24", "25", "26", "27", 
    "28", "29", "2a", "2b", "2c", "2d", "2e", "2f", 
    "30", "31", "32", "33", "34", "35", "36", "37", 
    "38", "39", "3a", "3b", "3c", "3d", "3e", "3f", 
    "40", "41", "42", "43", "44", "45", "46", "47", 
    "48", "49", "4a", "4b", "4c", "4d", "4e", "4f", 
    "50", "51", "52", "53", "54", "55", "56", "57", 
    "58", "59", "5a", "5b", "5c", "5d", "5e", "5f", 
    "60", "61", "62", "63", "64", "65", "66", "67", 
    "68", "69", "6a", "6b", "6c", "6d", "6e", "6f", 
    "70", "71", "72", "73", "74", "75", "76", "77", 
    "78", "79", "7a", "7b", "7c", "7d", "7e", "7f", 
    "80", "81", "82", "83", "84", "85", "86", "87", 
    "88", "89", "8a", "8b", "8c", "8d", "8e", "8f", 
    "90", "91", "92", "93", "94", "95", "96", "97", 
    "98", "99", "9a", "9b", "9c", "9d", "9e", "9f", 
    "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7", 
    "a8", "a9", "aa", "ab", "ac", "ad", "ae", "af", 
    "b0", "b1", "b2", "b3", "b4", "b5", "b6", "b7", 
    "b8", "b9", "ba", "bb", "bc", "bd", "be", "bf", 
    "c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7", 
    "c8", "c9", "ca", "cb", "cc", "cd", "ce", "cf", 
    "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7", 
    "d8", "d9", "da", "db", "dc", "dd", "de", "df", 
    "e0", "e1", "e2", "e3", "e4", "e5", "e6", "e7", 
    "e8", "e9", "ea", "eb", "ec", "ed", "ee", "ef", 
    "f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", 
    "f8", "f9", "fa", "fb", "fc", "fd", "fe", "ff", 
};

static void
printhex (GString *str, const char *data, int len)
{
  int i;
  const guchar *ucp;

  ucp = (const guchar*) data;
  
  i = 0;
  while (i < len)
    {
      g_string_append (str, hex_table[*ucp]);

      ++ucp;
      ++i;
    }
}

static void
write_iceauth (GString *add_str,
               GString *remove_str,
               IceAuthDataEntry *entry)
{
  g_string_append_printf (add_str,
                          "add %s \"\" %s %s ",
                          entry->protocol_name,
                          entry->network_id,
                          entry->auth_name);

  printhex (add_str, entry->auth_data, entry->auth_data_length);
  g_string_append_c (add_str, '\n');
  
  g_string_printf (remove_str, 
                   "remove protoname=%s protodata=\"\" netid=%s authname=%s\n",
                   entry->protocol_name,
                   entry->network_id,
                   entry->auth_name);
}

static gboolean
create_auth_entries (MsmServer       *server,
                     IceListenObj    *listen_objs,
                     int              n_listen_objs)
{
  int i;
  IceAuthDataEntry *entries;
  
  g_return_val_if_fail (add_script == NULL, FALSE);
  g_return_val_if_fail (remove_script == NULL, FALSE);
  
  add_script = g_string_new ("");
  remove_script = g_string_new ("");
  
  server->n_auth_entries = n_listen_objs * 2;
  server->auth_entries = g_new (IceAuthDataEntry, server->n_auth_entries);
  entries = server->auth_entries;
  
  for (i = 0; i <  server->n_auth_entries; i += 2)
    {
      entries[i].network_id =
        IceGetListenConnectionString (listen_objs[i/2]);
      entries[i].protocol_name = "ICE";
      entries[i].auth_name = "MIT-MAGIC-COOKIE-1";

      entries[i].auth_data =
        IceGenerateMagicCookie (MAGIC_COOKIE_LEN);
      entries[i].auth_data_length = MAGIC_COOKIE_LEN;

      entries[i+1].network_id =
        IceGetListenConnectionString (listen_objs[i/2]);
      entries[i+1].protocol_name = "XSMP";
      entries[i+1].auth_name = "MIT-MAGIC-COOKIE-1";

      entries[i+1].auth_data = 
        IceGenerateMagicCookie (MAGIC_COOKIE_LEN);
      entries[i+1].auth_data_length = MAGIC_COOKIE_LEN;

      write_iceauth (add_script, remove_script, &entries[i]);
      write_iceauth (add_script, remove_script, &entries[i+1]);
      
      IceSetPaAuthData (2, &entries[i]);

      IceSetHostBasedAuthProc (listen_objs[i/2], host_auth_callback);
    }
  
  if (!run_iceauth_script (add_script))
    return FALSE;

  g_string_free (add_script, TRUE);
  add_script = NULL;
  
  return TRUE;
}

static void
free_auth_entries (IceAuthDataEntry *entries,
                   int               n_entries)
{
  int i;

  for (i = 0; i < n_entries; i++)
    {
      g_free (entries[i].network_id);
      g_free (entries[i].auth_data);
    }

  g_free (entries);

  run_iceauth_script (remove_script);
  
  g_string_free (remove_script, TRUE);
  remove_script = NULL;  
}
