/* msm startup progress indicator */

/* 
 * Copyright (C) 2001 Havoc Pennington
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef MSM_STARTUP_H
#define MSM_STARTUP_H

#include "msm-startup-module.h"

/* NULL for fallback module, "default" for default module */
void msm_startup_set_module (const char *name);

void msm_startup_begin     (MsmStartupInfo *info);
void msm_startup_end       (MsmStartupInfo *info);
void msm_startup_begin_app (MsmStartupInfo *info,
                            int             app_index);
void msm_startup_end_app   (MsmStartupInfo *info,
                            int             app_index);

#endif

