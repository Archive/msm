#include "gdmremote.h"
#include "util.h"
#include <glib/gstring.h>
#include <glib.h>
#include <gdk/gdk.h>
#include <X11/Xauth.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

#ifndef UNIX_PATH_MAX
#define UNIX_PATH_MAX 108
#endif

struct _GdmRemote 
  {
    char *socket;
    int fd;

    char *version;
    GdmRemoteLogoutFlags logout_actions;
    guint is_authenticated : 1;
  };

static gboolean gdm_remote_connect (GdmRemote *remote, GError **error);
static const char *get_display_number (void);
static gboolean gdm_remote_auth (GdmRemote *remote, GError **error);

GdmRemote *
gdm_remote_new (const char *socket, GError **error)
{
  GdmRemote *remote;
  struct stat file_status = { 0 };

  g_return_val_if_fail (socket != NULL, NULL);
  g_return_val_if_fail (strlen (socket) < UNIX_PATH_MAX, NULL);


  if (stat (socket, &file_status) < 0)
    {
      g_set_error (error, 0, 0,
                   _("Failed to stat socket '%s': %s\n"),
                   socket, g_strerror (errno));
      return NULL;
    }

  if (access (socket, R_OK | W_OK) < 0) 
    {
      g_set_error (error, 0, 0,
                   _("Failed to verify access permissions of socket '%s': %s\n"),
                   socket, g_strerror (errno));
      return NULL;
    }

  remote = g_new0 (GdmRemote, 1);

  remote->socket = g_strdup (socket);
  remote->fd = -1;
  remote->is_authenticated = FALSE;

  if (!gdm_remote_connect (remote, error)) 
    {
      g_free (remote);
      return NULL;
    }

  remote->version = gdm_remote_send_command (remote, "VERSION", error);

  if (remote->version == NULL || *error)
    {
      g_free (remote);
      return NULL;
    }

  if (gdm_remote_auth (remote, error))
    remote->is_authenticated = TRUE;

  return remote;
}

gboolean 
gdm_remote_is_authenticated (GdmRemote *remote)
{
  g_return_val_if_fail (remote != NULL, FALSE);

  return remote->is_authenticated;
}

gboolean
gdm_remote_set_logout_action (GdmRemote *remote, 
                              GdmRemoteLogoutFlags logout_action, 
                              GError **error)
{
  char *command, *answer, *action_str;
  gboolean is_successful;

  switch (logout_action)
    {
      case GDM_REMOTE_LOGOUT_NONE:
        action_str = "NONE";
      break;

      case GDM_REMOTE_LOGOUT_REBOOT:
        action_str = "REBOOT";
      break;

      case GDM_REMOTE_LOGOUT_HALT:
        action_str = "HALT";
      break;

      case GDM_REMOTE_LOGOUT_SUSPEND:
        action_str = "SUSPEND";
      break;

      default:
      return FALSE;
    }

  command = g_strdup_printf ("SET_SAFE_LOGOUT_ACTION %s", action_str);

  answer = gdm_remote_send_command (remote, command, error); 
  g_free (command);

  is_successful = answer != NULL;
  g_free (answer);

  return is_successful;
}

GdmRemoteLogoutFlags 
gdm_remote_query_logout_actions (GdmRemote *remote, GError **error)
{
  char *answer, **logout_actions = NULL;
  GdmRemoteLogoutFlags logout_flags = GDM_REMOTE_LOGOUT_NONE;
  int i;

  if (!gdm_remote_is_authenticated (remote))
    return logout_flags;

  answer = gdm_remote_send_command (remote, 
                                    "QUERY_LOGOUT_ACTION", 
                                    error);

  if (*error || !answer)
    {
      g_free (answer);

      return logout_flags;
    }

  logout_actions = g_strsplit (answer, ";", 0);
  g_free (answer);

  for (i = 0; logout_actions[i]; i++) 
    {
      if (!strcmp (logout_actions[i], "HALT"))
        logout_flags |= GDM_REMOTE_LOGOUT_HALT;
      else if (!strcmp (logout_actions[i], "REBOOT"))
        logout_flags |= GDM_REMOTE_LOGOUT_REBOOT;
      else if (!strcmp (logout_actions[i], "SUSPEND"))
        logout_flags |= GDM_REMOTE_LOGOUT_SUSPEND;
    }
  g_strfreev (logout_actions);

  return logout_flags;
}

static gboolean 
gdm_remote_connect (GdmRemote *remote, GError **error)
{
  struct sockaddr_un addr = { AF_UNIX };

  remote->fd = socket (AF_UNIX, SOCK_STREAM, 0);

  if (remote->fd < 0) 
    {
      g_set_error (error, 0, 0,
                   _("Failed to retrieve socket descriptor: %s\n"),
                   g_strerror (errno));
      return FALSE;
    }

  strcpy (addr.sun_path, remote->socket);

  if (connect (remote->fd, (struct sockaddr *) &addr, sizeof (addr)) < 0) 
    {
      g_set_error (error, 0, 0,
                   _("Failed to connect to socket '%s': %s\n"),
                   remote->socket, g_strerror (errno));

      close (remote->fd);
      remote->fd = -1;
      g_free (remote);

      return FALSE;
    }

  return TRUE;
}

/* Next two functions stolen from gdm
 */
static const char *
get_display_number (void)
{
    static char *number = NULL;
                                                                                
    if (!number) 
      {
        char *p;
        number = gdk_get_display ();

        if (!number)
          {
            number = 0;
            return number;
          }
                                                                                
        number = strrchr (number, ':');
        if (number) 
          {
            number++;
            p = strrchr (number, '.');

            if (p)
              *p = '\0';
          } 
        else 
          {
            number = "0";
          }
      }
                                                                                
    return number;
}

static gboolean
gdm_remote_auth (GdmRemote *remote, GError **error)
{
  FILE *fp;
  const char *number;
  Xauth *xau;
  gboolean retval = FALSE;
  GError *auth_error = NULL;

  fp = fopen (XauFileName (), "r");

  if (!fp) 
    {
      g_set_error (error, 0, 0,
                   _("Failed to open X authorization file '%s': %s\n"),
                   XauFileName (), g_strerror (errno));
      return FALSE;
    }

  number = get_display_number ();

  while ((xau = XauReadAuth (fp)))
    {
      char *cmd;
      char *ret;
      int i;
      char buffer[40 /* 2*16 == 32, so 40 is enough */];

      /* Only Family local things are considered, all console
       * logins DO have this family (and even some local xdmcp
       * logins, though those will not pass by gdm itself of
       * course) 
       */
      if (xau->family != FamilyLocal ||
          xau->number_length != strlen (number) ||
          strncmp (xau->number, number, xau->number_length) != 0 ||
          /* gdm sends MIT-MAGIC-COOKIE-1 cookies of length 16,
           * so just do those
           */
          xau->data_length != 16 ||
          xau->name_length != strlen ("MIT-MAGIC-COOKIE-1") ||
          strncmp (xau->name, "MIT-MAGIC-COOKIE-1",
               xau->name_length) != 0 ||
          xau->data_length != 16) {
          XauDisposeAuth (xau);
          continue;
      }

      buffer[0] = '\0';
      for (i = 0; i < 16; i++) {
          char sub[3];
          g_snprintf (sub, sizeof (sub), "%02x", (guint)(guchar)xau->data[i]);
          strcat (buffer, sub);
      }
                                                                              
      XauDisposeAuth (xau);
                                                                              
      cmd = g_strdup_printf ("AUTH_LOCAL %s", buffer);
      ret = gdm_remote_send_command (remote, cmd, &auth_error);

      if (!ret)
        {
          if (error)
            {
              msm_verbose ("Authentication cookie '%s' failed to authenticate: "
                           "%s\n", buffer, auth_error->message);
              g_error_free (auth_error);
              auth_error = NULL;
            }
          continue;
        }

      g_free (ret);
      retval = TRUE;
      break;
    }
  fclose (fp);

  if (!retval)
    {
      g_set_error (error, 0, 0,
                   _("All authentication cookies failed to authenticate with "
                     "GDM socket '%s'"), remote->socket);

    }
  return retval;
}

char *
gdm_remote_send_command (GdmRemote *remote, const char *command, GError **error)
{
  void (* sigpipe_handler) (int);
  GString *string;
  char read_chunk[256] = { 0 }, *answer, *end_of_answer,
       **answer_vector = NULL;
  
  int num_bytes;

  g_return_val_if_fail (remote != NULL, NULL);

  string = g_string_new (command);
  g_string_append_c (string, '\n');

  sigpipe_handler = signal (SIGPIPE, SIG_IGN);
  num_bytes = send (remote->fd, string->str, string->len, 0);
  signal (SIGPIPE, sigpipe_handler);

  if (num_bytes < 0)
    {
      g_set_error (error, 0, 0,
                   _("Failed to send command '%s' to socket: %s\n"),
                   command, g_strerror (errno));
      g_string_free (string, TRUE);
      return NULL;
    }

  g_string_erase (string, 0, -1);
  end_of_answer = NULL;
  while (!end_of_answer)
    {
      num_bytes = recv (remote->fd, read_chunk, 256, 0);
      
      if (num_bytes < 0)
        {
          if (errno == EINTR)
            continue;

          g_set_error (error, 0, 0,
                       _("Failed to receive answer from socket: %s\n"),
                       g_strerror (errno));
          g_string_free (string, TRUE);
          return NULL;
        }

      end_of_answer = (char *) g_strrstr_len (read_chunk, num_bytes, "\n");

      if (end_of_answer)
        *end_of_answer = '\0';
       
      g_string_append (string, read_chunk);
    }

  answer = string->str;
  g_string_free (string, FALSE);

  answer_vector = g_strsplit (answer, " ", 2);
  g_free (answer);

  if (!answer_vector || (strcmp (answer_vector[0], "OK") 
      && strcmp (answer_vector[0], 
                 "GDM" /* VERSION returns GDM instead of OK */)))
    {
      g_set_error (error, 0, 0,
                   _("Command '%s' failed: %s\n"),
                   command, answer_vector[1] ? 
                            answer_vector[1] : _("unknown error"));
      g_strfreev (answer_vector);
      return NULL;
    }
  g_free (answer_vector[0]);
  
  answer = answer_vector[1]? answer_vector[1] : g_strdup ("");

  g_free (answer_vector);
  
  return answer;
}

void
gdm_remote_free (GdmRemote *remote)
{
  g_free (remote->socket);
  remote->socket = NULL;

  close (remote->fd);
  remote->fd = -1;

  g_free (remote->version);
  remote->version = NULL;
  
  g_free (remote);
}

const char *
gdm_remote_get_version (GdmRemote *remote)
{
  g_return_val_if_fail (remote != NULL, NULL);

  return remote->version;
}
