/* msm default startup module */

/* 
 * Copyright (C) 2001 Havoc Pennington
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "msm-startup-module.h"
#include <gtk/gtkwindow.h>
#include <gtk/gtkprogressbar.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkimage.h>
#include <gtk/gtktable.h>
#include <gtk/gtkframe.h>

#include <config.h>
#include <libintl.h>
#define _(x) gettext (x)
#define N_(x) x

static void     raise_our_windows         (void);

static gboolean default_init              (void);
static void     default_shutdown          (void);
static int      default_get_shutdown_time (void);
static void     default_begin             (MsmStartupInfo *info);
static void     default_end               (MsmStartupInfo *info);
static void     default_begin_app         (MsmStartupInfo *info,
                                           int             app_index);
static void     default_end_app           (MsmStartupInfo *info,
                                           int             app_index);

static MsmStartupModule default_module = {
  MSM_STARTUP_MODULE_VERSION,
  default_init,
  default_shutdown,
  default_get_shutdown_time,
  default_begin,
  default_end,
  default_begin_app,
  default_end_app
};

/* our exported function */
MsmStartupModule*
msm_startup_module_get_vtable (void)
{
  return &default_module;
}

static GtkWidget *cover_window;
static GtkWidget *window;
static GtkWidget *progress_bar;
static GtkWidget *app_name_label;
static GtkWidget *app_icon_image;
static int finished_apps;

static gboolean
default_init (void)
{  
  return TRUE;
}

static void
default_shutdown (void)
{
  if (cover_window)
    gtk_widget_destroy (cover_window);
  gtk_widget_destroy (window);
  window = NULL;
  cover_window = NULL;
}

static int
default_get_shutdown_time (void)
{
  return 0;
}

static void
default_begin (MsmStartupInfo *info)
{
  GtkWidget *table;
  GtkWidget *frame;
  char *msg;
  GdkColor color;

  finished_apps = 0;

  cover_window = gtk_window_new (GTK_WINDOW_POPUP);
  gtk_window_resize (GTK_WINDOW (cover_window),
                     gdk_screen_width (), gdk_screen_height ());
  gdk_color_parse ("white", &color);
  gtk_widget_modify_bg (cover_window, GTK_STATE_NORMAL,
                        &color);

  gtk_widget_show (cover_window);
  
  window = gtk_window_new (GTK_WINDOW_POPUP);
  /* If we weren't override redirect (POPUP), this would be _bad_ */
  gtk_window_set_position (GTK_WINDOW (window), GTK_WIN_POS_CENTER_ALWAYS);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_OUT);
  gtk_container_add (GTK_CONTAINER (window), frame);
  
  /* Make ourselves a little grid to lock things to */
  table = gtk_table_new (20, 20, TRUE);
  gtk_container_add (GTK_CONTAINER (frame), table);

  progress_bar = gtk_progress_bar_new ();
  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress_bar),
                                 0.0);
  gtk_table_attach (GTK_TABLE (table),
                    progress_bar,
                    1, 19,                 14, 18,
                    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
                    0,                     0);
  
  app_icon_image = gtk_image_new ();
  gtk_misc_set_alignment (GTK_MISC (app_icon_image), 0.0, 0.0);
  gtk_table_attach (GTK_TABLE (table),
                    app_icon_image,
                    1, 4,                  2, 13,
                    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
                    0,                     0);

  msg = g_strdup_printf (_("Starting session \"%s,\" please wait"),
                         info->translated_session_name);
  app_name_label = gtk_label_new (msg);
  g_free (msg);
  gtk_label_set_line_wrap (GTK_LABEL (app_name_label), TRUE);
  gtk_label_set_justify (GTK_LABEL (app_name_label), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (app_name_label), 0.0, 0.0);
  gtk_table_attach (GTK_TABLE (table),
                    app_name_label,
                    5, 19,                 2, 13,
                    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
                    0,                     0);

  gtk_widget_show_all (window);
}

static void
default_end (MsmStartupInfo *info)
{
  gtk_label_set_text (GTK_LABEL (app_name_label),
                      _("Finished starting applications"));
  gtk_image_set_from_pixbuf (GTK_IMAGE (app_icon_image), NULL);
  
  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress_bar), 1.0);

  gtk_widget_destroy (cover_window);
  cover_window = NULL;
  
  raise_our_windows ();
}

static void
default_begin_app (MsmStartupInfo *info,
                    int             app_index)
{
  char *msg;
  MsmStartupAppInfo *app = &info->apps[app_index];

  msg = g_strdup_printf (_("Starting \"%s\""), app->name);
  gtk_label_set_text (GTK_LABEL (app_name_label),
                      msg);
  g_free (msg);
  gtk_image_set_from_pixbuf (GTK_IMAGE (app_icon_image),
                             app->icon);

  raise_our_windows ();
}

static void
default_end_app (MsmStartupInfo *info,
                  int             app_index)
{
  char *msg;
  MsmStartupAppInfo *app = &info->apps[app_index];

  msg = g_strdup_printf (_("Successfully started \"%s\""), app->name);
  gtk_label_set_text (GTK_LABEL (app_name_label),
                      msg);
  g_free (msg);
  gtk_image_set_from_pixbuf (GTK_IMAGE (app_icon_image),
                             app->icon);

  finished_apps += 1;
  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress_bar),
                                 (double) finished_apps / (double) info->n_apps);

  raise_our_windows ();
}

static void
raise_our_windows (void)
{
  /* This is really broken - it should flicker, though in practice it
   * doesn't seem to... and of course we'd expect apps to keep appearing
   * on top of our windows, etc. - we need WM support.
   */
  if (cover_window)
    gdk_window_raise (cover_window->window);
  gdk_window_raise (window->window);
}
