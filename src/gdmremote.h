#ifndef GDM_REMOTE
#define GDM_REMOTE

#include <glib/gerror.h>

typedef struct _GdmRemote GdmRemote;

typedef enum 
{
  GDM_REMOTE_LOGOUT_NONE = 0,
  GDM_REMOTE_LOGOUT_REBOOT = 1 << 0,
  GDM_REMOTE_LOGOUT_HALT = 1 << 1,
  GDM_REMOTE_LOGOUT_SUSPEND = 1 << 2
} GdmRemoteLogoutFlags;

GdmRemote  *gdm_remote_new (const char *socket, GError **error);
void        gdm_remote_free (GdmRemote *remote);
const char *gdm_remote_get_version (GdmRemote *remote);
char       *gdm_remote_send_command (GdmRemote *remote, 
                                     const char *command, 
                                     GError **error);
gboolean    gdm_remote_is_authenticated (GdmRemote *remote);
GdmRemoteLogoutFlags gdm_remote_query_logout_actions (GdmRemote *remote, 
                                                      GError **error);
gboolean
gdm_remote_set_logout_action (GdmRemote *remote, 
                              GdmRemoteLogoutFlags logout_action, 
                              GError **error);

#endif
