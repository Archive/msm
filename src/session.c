/* msm session */

/* 
 * Copyright (C) 2001 Havoc Pennington
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "session.h"
#include "util.h"
#include "props.h"
#include "dialogs.h"
#include "startup.h"
#include "inlinepixbufs.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>

#include <gtk/gtk.h>

#define MAX_RESPAWNS 5
 /* 5 seconds */
#define MAX_RESPAWN_TIMEOUT 5000

typedef struct _MsmSavedClient MsmSavedClient;
typedef struct _FailedLaunch FailedLaunch;

struct _FailedLaunch
{
  char   *name;
  GError *error;
  char   *id;
};

struct _MsmSavedClient
{
  MsmSession *session;
  char *id;
  int priority;
  GList *properties;
  GTime last_launch_time;
  int launch_count;
  /* to avoid dialog spam in case of multiple errors,
   * and to take down dialogs when their client goes away
   */
  GtkWidget *error_dialog;
  int serial;
  /* index into startup_info in the session */
  int startup_index;
};

struct _MsmSession
{
  char *name;
  GList *clients;
  char *filename;
  char *full_filename;
  int lock_fd;
  /* Non-NULL if we are doing a launch, and it isn't completed yet. */
  MsmStartupInfo *startup_info;
  /* TRUE if we've properly indicated to startup module that
   * we are done starting up, so it's safe to free startup_info
   */
  guint startup_ended : 1;

  /* TRUE if session file is not user writable.
   */
  guint read_only : 1;
};

struct MsmTimeoutClient
{
  MsmSession *session;
  MsmSavedClient *saved;
};

typedef enum
{
  MSM_SESSION_FAILURE_OPENING_FILE,
  MSM_SESSION_FAILURE_LOCKING,
  MSM_SESSION_FAILURE_BAD_FILE,
  MSM_SESSION_FAILURE_EMPTY
} MsmSessionFailureReason;

typedef enum
{
  MSM_SPECIAL_SESSION_DEFAULT,
  MSM_SPECIAL_SESSION_FAILSAFE
} MsmSpecialSession;

static GHashTable *sessions = NULL;
 
static MsmSavedClient *saved_new    (MsmSession *session);
static void            saved_free   (MsmSavedClient *saved);
static FailedLaunch   *saved_launch (MsmSavedClient *saved);
static char           *saved_get_program (MsmSavedClient *saved);
static char           *saved_get_name (MsmSavedClient *saved);
static char           *saved_get_icon_name (MsmSavedClient *saved);
static void            saved_restart (MsmSavedClient *saved);
static void            saved_remove  (MsmSavedClient *saved);
static GdkPixbuf      *get_icon_from_name (const char *icon_name);
static char           *saved_get_fallback_name (MsmSavedClient *saved);
static GdkPixbuf      *saved_get_fallback_icon (MsmSavedClient *saved);
static void            msm_session_cache_client_priorities (MsmSession *session);
static void            msm_session_wait_for_clients (MsmSession *session, 
                                                     GList      *sorted, 
                                                     int         last_priority);

static void failed_launch_free      (FailedLaunch *fl);
static void free_startup_info       (MsmSession *session);

static MsmSession* recover_failed_session (MsmSession              *session,
                                           MsmSessionFailureReason  reason,
                                           const char              *details);

static gboolean    parse_session_file     (MsmSession *session,
                                           GError    **error);

static char* decode_text_from_utf8        (const char *text);
static char* encode_text_as_utf8_markup   (const char *text);

static char* build_session_path (const char *base_path);
static const char* user_session_dir (void);
static const char* global_session_dir (void);
static MsmSession* msm_session_get_cached (const char *filename);
static void msm_session_cache (MsmSession *session);
static void msm_session_free (MsmSession *session);
static MsmSession* msm_session_revert_to_defaults (MsmSession *session);

static char * get_special_session_path (MsmSpecialSession session, 
                                        gboolean *global);

static gboolean all_clients_finished (MsmSession *session);

static GList*
find_client_link_by_id_in_list (GList      *list,
                                const char *client_id)
{
  GList *tmp;
  
  g_return_val_if_fail (client_id != NULL, NULL);
  
  tmp = list;
  while (tmp != NULL)
    {
      MsmSavedClient *saved;

      saved = tmp->data;

      if (strcmp (client_id, saved->id) == 0)
        return tmp;

      tmp = tmp->next;
    }

  return NULL;
}

static GList*
find_client_link_by_id (MsmSession *session,
                        const char *client_id)
{
  g_return_val_if_fail (client_id != NULL, NULL);

  return find_client_link_by_id_in_list (session->clients, client_id);
}

static GList*
find_client_link (MsmSession *session,
                  MsmClient  *client)
{
  return find_client_link_by_id (session,
                                 msm_client_get_id (client));
}

static MsmSavedClient*
find_client (MsmSession *session,
             MsmClient  *client)
{
  GList *list;

  list = find_client_link (session, client);

  if (list)
    return list->data;
  else
    return NULL;
}

void
msm_session_update_client (MsmSession  *session,
                           MsmClient   *client)
{
  MsmSavedClient *saved;
  const char *client_id;

  client_id = msm_client_get_id (client);

  if (client_id == NULL) /* client not registered yet */
    return;
  
  saved = find_client (session, client);

  if (saved == NULL)
    {
      saved = saved_new (session);
      saved->id = g_strdup (client_id);
      session->clients = g_list_prepend (session->clients, saved);
    }

  if (saved->properties)
    proplist_free (saved->properties);

  saved->properties = msm_client_copy_proplist (client);
}

static void
remove_link (GList *link)
{
  MsmSavedClient *saved = link->data;
  char *name;

  name = saved_get_program (saved);

  msm_verbose ("Removing client '%s'\n", name);
  
  saved->session->clients = g_list_delete_link (saved->session->clients,
                                                link);

  /* Clean up the saved session data */
  msm_run_discard (name, saved->properties);

  g_free (name);
  saved_free (saved);
}

static void
saved_remove  (MsmSavedClient *saved)
{
  GList *link;

  link = g_list_find (saved->session->clients,
                      saved);

  if (link)
    remove_link (link);
}

static GdkPixbuf *
get_icon_from_name (const char *icon_name)
{
    GdkPixbuf *icon;
    static GtkIconTheme *icon_theme = NULL;
    char *icon_no_extension;
    char *p;

    icon_no_extension = g_strdup (icon_name);
    p = strrchr (icon_no_extension, '.');
    if (p &&
        (strcmp (p, ".png") == 0 ||
         strcmp (p, ".xpixmap") == 0 ||
         strcmp (p, ".svg") == 0)) 
      {
        *p = 0;
      }

    if (icon_theme == NULL)
      icon_theme = gtk_icon_theme_new ();

    icon = gtk_icon_theme_load_icon (icon_theme, 
                                     icon_no_extension,
                                     24, GTK_ICON_LOOKUP_USE_BUILTIN,
                                     NULL);
    g_free (icon_no_extension);

    if (icon == NULL)
      {
        char *icon_file;

        icon_file = g_build_filename (GNOME_ICONDIR, icon_name, NULL);

        icon = gdk_pixbuf_new_from_file (icon_file, NULL);
        g_free (icon_file);
      }
    
    
    return icon;
}

typedef struct
{
  char *name;
  char *program;
  char *icon;
} MsmProgramLookupRecord;

static const MsmProgramLookupRecord msm_program_lookup_table[] = {
    { N_("Sawfish Window Manager"),
      "sawfish",
      "gnome-ccwindowmanager.png" },
    { N_("Metacity Window Manager"),
      "metacity",
      "gnome-ccwindowmanager.png" },
    { N_("Window Manager"),
      "gnome-wm",
      "gnome-ccwindowmanager.png" },
    { N_("The Panel"),
      "gnome-panel",
      "gnome-panel.png" },
    { N_("Session Manager Proxy"),
      "gnome-smproxy",
      "gnome-session.png" },
    { N_("Nautilus"),
      "nautilus",
      "gnome-ccdesktop.png" },
    { N_("Desktop Settings"),
      "gnome-settings-daemon",
      "gnome-settings.png" }, 
    { NULL, NULL, NULL}
};

static char*
saved_get_fallback_name (MsmSavedClient *saved)
{
  char *program;
  int i;
  
  program = saved_get_program (saved);

  i = 0;
  while (msm_program_lookup_table[i].program)
    {
      if (!g_ascii_strcasecmp (msm_program_lookup_table[i].program, 
                              program))
        {
          g_free (program);
          return g_strdup (msm_program_lookup_table[i].name);
        }

      ++i;
    }

  return program;
}

static GdkPixbuf*
saved_get_fallback_icon (MsmSavedClient *saved)
{
  char *program;
  int i;
  GdkPixbuf *icon;
  
  program = saved_get_program (saved);

  i = 0;
  while (msm_program_lookup_table[i].program)
    {
      if (g_ascii_strcasecmp (msm_program_lookup_table[i].program, 
                              program) == 0)
        {
          icon = get_icon_from_name (msm_program_lookup_table[i].icon);

          if (icon)
            {
              g_free (program);
            return icon;
            }
          else
            break;
        }
      
      ++i;
    }

  icon = gdk_pixbuf_new_from_inline (-1, generic_app_icon_data, FALSE,
                                       NULL);
  
  g_free (program);

  return icon;
}

void
msm_session_remove_client (MsmSession  *session,
                           MsmClient   *client)
{
  GList *link;

  link = find_client_link (session, client);

  if (link)
    remove_link (link);
}

void
msm_session_restart_client (MsmSession *session,
                            MsmClient  *client)
{
  MsmSavedClient *saved;
  
  saved = find_client (session, client);

  if (saved == NULL)
    return; /* client has never been saved, or was removed */

  saved_restart (saved);
}

enum
{
  RESPONSE_KEEP_TRYING,
  RESPONSE_REMOVE,
  RESPONSE_DISABLE
};

static void
respawn_error_response (GtkWidget *dialog,
                        int        response_id,
                        gpointer   data)
{
  MsmSavedClient *saved = data;

  /* first, because saved_remove also destroys it if it isn't */
  gtk_widget_destroy (dialog);
  
  switch (response_id)
    {
    case RESPONSE_KEEP_TRYING:
      saved->last_launch_time = 0;
      saved->launch_count = 0;
      saved_restart (saved);
      break;
    case RESPONSE_REMOVE:
      saved_remove (saved);
      break;
    case RESPONSE_DISABLE:
      /* Be sure it's disabled for the next timeout at least.
       * We might try again on next session save though.
       */
      saved->last_launch_time = time (NULL);
      saved->launch_count = MAX_RESPAWNS + 1;
    default:
      break;
    }
}


static void
saved_restart (MsmSavedClient *saved)
{
  FailedLaunch *fl;

  if (saved->error_dialog)
    return; /* currently complaining about this client, don't restart it */
  
  if (saved->launch_count > MAX_RESPAWNS)
    {
      GTime now;

      now = time (NULL);
      if ((now - saved->last_launch_time) > MAX_RESPAWN_TIMEOUT)
        {
          /* OK to try again */
          saved->last_launch_time = 0;
          saved->launch_count = 0;
        }
    }

  if (saved->launch_count > MAX_RESPAWNS)
    {
      /* not timed out, we don't try to restart again */
      char *name;
      
      if (saved->error_dialog)
        return; /* already complaining about this client */

      name = saved_get_program (saved);
      
      saved->error_dialog =
        gtk_message_dialog_new (NULL,
                                0,
                                GTK_MESSAGE_ERROR,
                                GTK_BUTTONS_NONE,
                                _("The application '%s' has been restarted several times in the last few seconds, but crashed or exited each time. What do you want to do with this application?"),
                                name);

      gtk_dialog_add_buttons (GTK_DIALOG (saved->error_dialog),
                              _("_Keep restarting"),
                              RESPONSE_KEEP_TRYING,
                              _("_Never restart again"),
                              RESPONSE_REMOVE,
                              _("_Stop restarting for now"),
                              RESPONSE_DISABLE,
                              NULL);

      gtk_dialog_set_default_response (GTK_DIALOG (saved->error_dialog),
                                       RESPONSE_DISABLE);
      
      gtk_window_set_position (GTK_WINDOW (saved->error_dialog),
                               GTK_WIN_POS_CENTER);
      
      g_signal_connect (G_OBJECT (saved->error_dialog),
                        "response",
                        G_CALLBACK (respawn_error_response),
                        NULL);

      g_object_add_weak_pointer (G_OBJECT (saved->error_dialog),
                                 (gpointer*) &saved->error_dialog);
      
      gtk_widget_show (saved->error_dialog);

      return;
    }

  fl = saved_launch (saved);

  if (fl != NULL)
    {
      /* Failed to launch this thing */      
      saved->error_dialog =
        gtk_message_dialog_new (NULL,
                                0,
                                GTK_MESSAGE_ERROR,
                                GTK_BUTTONS_CLOSE,
                                _("The application '%s' could not be launched."),
                                fl->name);

      g_signal_connect (GTK_OBJECT (saved->error_dialog), "response", 
                        G_CALLBACK (gtk_widget_destroy), NULL);

      dialog_add_details (GTK_DIALOG (saved->error_dialog), fl->error->message);
      
      gtk_window_set_position (GTK_WINDOW (saved->error_dialog),
                               GTK_WIN_POS_CENTER);      

      g_object_add_weak_pointer (G_OBJECT (saved->error_dialog),
                                 (gpointer *) &saved->error_dialog);

      gtk_widget_show (saved->error_dialog);

      failed_launch_free (fl);
    }
}

gboolean
msm_session_client_id_known (MsmSession *session,
                             const char *previous_id)
{
  g_return_val_if_fail (previous_id != NULL, FALSE);

  return find_client_link_by_id (session, previous_id) != NULL;
}

enum
{
  COLUMN_NAME,
  COLUMN_ERROR,
  COLUMN_SESSION_ID,
  COLUMN_LAST
};

static GtkWidget*
create_failure_list (GList *failures)
{
  GtkTreeSelection *selection;
  GtkCellRenderer *cell;
  GtkWidget *tree_view;
  GtkTreeViewColumn *column;
  GtkListStore *model;
  GtkTreeIter iter;
  GList *tmp;
  
  model = gtk_list_store_new (COLUMN_LAST,
                              G_TYPE_STRING,
                              G_TYPE_STRING,
                              G_TYPE_STRING);
  
  tree_view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (model));

  g_object_unref (G_OBJECT (model));
  
  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));

  gtk_tree_selection_set_mode (GTK_TREE_SELECTION (selection),
			       GTK_SELECTION_NONE);

  tmp = failures;
  while (tmp != NULL)
    {
      FailedLaunch *fl = tmp->data;
      
      gtk_list_store_append (model, &iter);

      gtk_list_store_set (model,
			  &iter,
                          COLUMN_NAME, fl->name,
                          COLUMN_ERROR, fl->error->message,
                          COLUMN_SESSION_ID, fl->id,
                          -1);
      
      tmp = tmp->next;
    }
  
  cell = gtk_cell_renderer_text_new ();

  g_object_set (G_OBJECT (cell),
                "weight", PANGO_WEIGHT_BOLD,
                "xpad", 2,
                NULL);
  
  column = gtk_tree_view_column_new_with_attributes (_("Application"),
						     cell,
						     "text", COLUMN_NAME,
						     NULL);
  
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view),
			       GTK_TREE_VIEW_COLUMN (column));

  cell = gtk_cell_renderer_text_new ();
  
  column = gtk_tree_view_column_new_with_attributes (_("Error details"),
						     cell,
						     "text", COLUMN_ERROR,
						     NULL);
  
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view),
			       GTK_TREE_VIEW_COLUMN (column));

  return tree_view;
}


static char *
saved_get_name (MsmSavedClient *saved)
{  
  char *name;
  
  proplist_find_string (saved->properties,
                        MSM_CLIENT_PROPERTY_NAME,
                        &name);
  if (!name)
    {
      name = saved_get_fallback_name (saved);
    }
 
  return name;

}

static char *
saved_get_icon_name (MsmSavedClient *saved)
{
  char *icon_name;
  
  proplist_find_string (saved->properties,
                        MSM_CLIENT_PROPERTY_ICON,
                        &icon_name);
  
  return icon_name;
}

static char*
saved_get_program (MsmSavedClient *saved)
{
  char *name = NULL;
  
  proplist_find_string (saved->properties,
                        SmProgram,
                        &name);
  
  if (name == NULL)
    name = g_strdup_printf ("Unknown %d", saved->serial);

  return name;
}

static void
failed_launch_free (FailedLaunch *fl)
{
  g_free (fl->name);
  g_error_free (fl->error);
  g_free (fl->id);
  g_free (fl);
}

static FailedLaunch*
saved_launch (MsmSavedClient *saved)
{
  GError *err;
  char *program;
  FailedLaunch *fl;
  int property_value;
  MsmClientRoleFlags role;

  if (proplist_find_card8 (saved->properties, MSM_CLIENT_PROPERTY_ROLES, 
                           &property_value))
    role = (MsmClientRoleFlags) property_value;
  else
    role = MSM_CLIENT_ROLE_NORMAL;

  if ((role & MSM_CLIENT_ROLE_WINDOW_MANAGER)
      && saved->session->startup_info->wm_is_running)
    {
      err = g_error_new (MSM_ERROR, MSM_ERROR_FAILED,
                         _("There can be only one window manager loaded at once"));

      fl = g_new (FailedLaunch, 1);
      fl->name = saved_get_name (saved);
      fl->error = err; 
      fl->id = g_strdup (saved->id);

      msm_verbose ("Attempted to load more than one window manager\n");
      return fl;
    }

  program = saved_get_program (saved);

  msm_verbose ("Launching '%s'\n", program);

  fl = NULL;
  err = NULL;
  if (!msm_run_command (SmRestartCommand,
                        saved->properties,
                        &err))
    {
      if (err == NULL)
        err = g_error_new (MSM_ERROR, MSM_ERROR_FAILED,
                           _("Internal session manager error, did not record an error for failed client launch"));

      fl = g_new (FailedLaunch, 1);
      fl->name = saved_get_name (saved);
      fl->error = err;
      fl->id = g_strdup (saved->id);
      
      msm_verbose ("Launch of '%s' failed: %s\n", program, err->message);
    }
  else if (role & MSM_CLIENT_ROLE_WINDOW_MANAGER)
    saved->session->startup_info->wm_is_running = TRUE;

  g_free (program);
  
  saved->launch_count += 1;
  saved->last_launch_time = time (NULL);
  
  return fl;
}

static int
priority_cmp (gconstpointer a,
              gconstpointer b)
{
  MsmSavedClient *saved_a = (MsmSavedClient*) a;
  MsmSavedClient *saved_b = (MsmSavedClient*) b;

  if (saved_a->priority < saved_b->priority)
    return -1; /* A is higher priority, front of list */
  else if (saved_a->priority > saved_b->priority)
    return 1;
  else
    return 0;
}

static void
free_startup_info (MsmSession *session)
{
  if (session->startup_info)
    {
      int i;

      g_return_if_fail (session->startup_ended);
      
      i = 0;
      while (i < session->startup_info->n_apps)
        {
          g_free ((char*) session->startup_info->apps[i].name);
          if (session->startup_info->apps[i].icon)
            g_object_unref (G_OBJECT (session->startup_info->apps[i].icon));
          ++i;
        }
      
      g_free (session->startup_info->apps);
      g_free ((char*) session->startup_info->translated_session_name);

      g_free (session->startup_info);

      session->startup_info = NULL;
    }
}

static void
end_startup (MsmSession *session)
{
  if (session->startup_info)
    {
      int i;
      
      i = 0;
      while (i < session->startup_info->n_apps)
        {
          if (!session->startup_info->apps[i].finished)
            {
              session->startup_info->apps[i].registered = FALSE;
              session->startup_info->apps[i].finished = TRUE;
              msm_startup_end_app (session->startup_info, i);
            }

          ++i;
        }
      
      msm_startup_end (session->startup_info);

      session->startup_ended = TRUE;
      
      free_startup_info (session);
    }
}

static gboolean
session_startup_timeout (MsmSession *session)
{
  end_startup (session);

  return FALSE;
}

static gboolean
app_startup_timeout (struct MsmTimeoutClient *client)
{
  MsmSession *session;
  MsmSavedClient *saved;

  session = client->session;
  saved = client->saved;

  if (session && session->startup_info && session->startup_info->apps &&
      saved && saved->startup_index >= 0 &&
      !session->startup_info->apps[saved->startup_index].finished) 
    {
      session->startup_info->apps[saved->startup_index].registered = FALSE;
      session->startup_info->apps[saved->startup_index].finished = TRUE;
      msm_startup_end_app (session->startup_info, saved->startup_index);

      if (all_clients_finished (session))
        end_startup (session);
    }

  g_free (client);

  return FALSE;
}

static void
msm_session_cache_client_priorities (MsmSession *session)
{
  GList *tmp = NULL;

  tmp = session->clients;
  while (tmp)
    {
      MsmSavedClient *saved = tmp->data;

      if (!proplist_find_card8 (saved->properties,
                                MSM_CLIENT_PROPERTY_PRIORITY,
                                &saved->priority))
       {
         int property_value;

         saved->priority = MSM_CLIENT_PRIORITY_NORMAL;

         if (proplist_find_card8 (saved->properties,
                                   MSM_CLIENT_PROPERTY_ROLES,
                                   &property_value))
           {
             MsmClientRoleFlags role;

             role = (MsmClientRoleFlags) property_value;

             if (role == MSM_CLIENT_ROLE_NORMAL)
               {
                 msm_verbose ("Client '%s' has normal role.\n", saved->id);
               }
             else
               {
                 if (role & MSM_CLIENT_ROLE_WINDOW_MANAGER)
                   {
                     msm_verbose ("Client '%s' has window manager role.\n", 
                                   saved->id);
                     saved->priority = MIN (saved->priority,
                                            MSM_CLIENT_PRIORITY_WINDOW_MANAGER);
                     role = role & ~MSM_CLIENT_ROLE_WINDOW_MANAGER;
                   }
                 if (role & MSM_CLIENT_ROLE_DESKTOP_HANDLER)
                   {
                     msm_verbose ("Client '%s' has desktop manager role.\n", 
                                   saved->id);
                     saved->priority = MIN (saved->priority,
                                           MSM_CLIENT_PRIORITY_DESKTOP_HANDLER);
                     role = role & ~MSM_CLIENT_ROLE_DESKTOP_HANDLER;
                   }
                 if (role & MSM_CLIENT_ROLE_PANEL)
                   {
                     msm_verbose ("Client '%s' has panel role.\n", saved->id);
                     saved->priority = MIN (saved->priority,
                                           MSM_CLIENT_PRIORITY_PANEL);
                     role = role & ~MSM_CLIENT_ROLE_PANEL;
                   }
                 if (role & MSM_CLIENT_ROLE_DESKTOP_COMPONENT)
                   {
                     msm_verbose ("Client '%s' has desktop component role.\n", 
                                   saved->id);
                     saved->priority = MIN (saved->priority,
                                         MSM_CLIENT_PRIORITY_DESKTOP_COMPONENT);
                    role = role & ~MSM_CLIENT_ROLE_DESKTOP_COMPONENT;
                   }
                 if (role & MSM_CLIENT_ROLE_SETUP)
                   {
                     msm_verbose ("Client '%s' has setup role.\n", saved->id);
                     saved->priority = MIN (saved->priority,
                                            MSM_CLIENT_PRIORITY_SETUP);
                     role = role & ~MSM_CLIENT_ROLE_SETUP;
                   }

                 if (role != MSM_CLIENT_ROLE_NORMAL)
                   msm_warning ("Client '%s' has unknown roles '0x%x'.\n",
                                saved->id, role);
               }
           }
         else
           {
             msm_verbose ("Client '%s' has no role.\n", saved->id);
           }
       }

      msm_verbose ("Client '%s' has priority '%d'\n", 
                   saved->id, saved->priority);
      tmp = tmp->next;
    }
}

static void
msm_session_wait_for_clients (MsmSession *session, GList *sorted, 
                              int last_priority)
{
  gboolean clients_starting = TRUE;

  msm_verbose ("Waiting for clients of priority '%d'\n", last_priority);
  while (clients_starting)
    {
      GList *tmp;

      tmp = sorted;
      while (tmp)
        {
          MsmSavedClient *saved = tmp->data;

          if (!session->startup_info->apps[saved->startup_index].finished 
              && saved->priority <= last_priority
              && session->startup_info->apps[saved->startup_index].started)
            {
              clients_starting = TRUE;
              g_main_iteration (FALSE);
              break;
            }
          else
            clients_starting = FALSE;
          tmp = tmp->next;
        }

    }
  msm_verbose ("Clients of priority '%d' finished starting.\n", last_priority);
}

void
msm_session_launch (MsmSession *session)
{
  GList *tmp;
  GList *failed;
  GList *sorted;
  int i, last_priority;
  
  if (session->clients == NULL)
    msm_verbose ("No clients in this session, can't launch\n");

  if (session->startup_info != NULL)
    {
      g_warning ("Session manager tried to launch the same session while it was already launching!");
      return;
    }

  msm_session_cache_client_priorities (session);

  sorted = g_list_copy (session->clients);
  sorted = g_list_sort (sorted, priority_cmp);
  
  session->startup_info = g_new (MsmStartupInfo, 1);
  session->startup_info->translated_session_name = g_strdup (session->name);
  session->startup_info->current_app = -1;
  session->startup_info->n_apps = g_list_length (sorted);
  session->startup_info->apps = g_new (MsmStartupAppInfo,
                                       session->startup_info->n_apps);
  session->startup_info->wm_is_running = FALSE;

  tmp = sorted;
  i = 0;
  while (i < session->startup_info->n_apps)
    {
      MsmSavedClient *saved = tmp->data;
      char *name = NULL, *icon_name = NULL;
      GdkPixbuf *icon = NULL;

      saved->startup_index = i;

      name = saved_get_name (saved);

      icon_name = saved_get_icon_name (saved);

      if (icon_name)
        icon = get_icon_from_name (icon_name);
      else 
        msm_verbose ("Client '%s' does not supply an icon.\n", saved->id);

      if (!icon)
        icon = saved_get_fallback_icon (saved); 

      session->startup_info->apps[i].name = name;
      name = NULL;
      session->startup_info->apps[i].icon = icon;
      icon = NULL;

      session->startup_info->apps[i].started = FALSE;
      session->startup_info->apps[i].finished = FALSE;
      session->startup_info->apps[i].registered = FALSE;
      
      ++i;
      tmp = tmp->next;
    }

  msm_startup_begin (session->startup_info);
  
  failed = NULL;
  
  /* Add timeout to stop splash screen if loading doesn't get done in a
   * reasonable amount of time.
   */
  g_timeout_add (2 /* mins */ * 60000, 
                 (GSourceFunc) session_startup_timeout, 
                 session);

  last_priority = -1;
  tmp = sorted;
  while (tmp != NULL)
    {
      MsmSavedClient *saved = tmp->data;
      struct MsmTimeoutClient *timeout_client;
      FailedLaunch *fl;

      if (last_priority < 0)  /* first iteration of loop */
        last_priority = saved->priority;
      else if (last_priority != saved->priority)
        {
          msm_session_wait_for_clients (session, sorted, last_priority);
          last_priority = saved->priority;
        }

      session->startup_info->apps[saved->startup_index].started = TRUE;
      msm_startup_begin_app (session->startup_info,
                             saved->startup_index);
      
      fl = saved_launch (saved);

      if (fl)
        {
          failed = g_list_append (failed, fl);

          session->startup_info->apps[saved->startup_index].started = FALSE;
          session->startup_info->apps[saved->startup_index].registered = FALSE;
          session->startup_info->apps[saved->startup_index].finished = TRUE;
          msm_startup_end_app (session->startup_info,
                               saved->startup_index);
        }

      /* Add timeout to advance the progress meter even if some
       * app is stuck, crashes, or isn't sm aware.
       */
      timeout_client = g_new (struct MsmTimeoutClient, 1);
      timeout_client->saved = saved;
      timeout_client->session = session;
      g_timeout_add (8 /* secs */ * 1000, 
                     (GSourceFunc) app_startup_timeout, 
                     timeout_client);

      tmp = tmp->next;
    }

  g_list_free (sorted);

  if (failed)
    {
      GtkWidget *dialog;
      GtkWidget *list;
      GtkWidget *sw;
      
      dialog = gtk_message_dialog_new (NULL,
                                       0,
                                       GTK_MESSAGE_ERROR,
                                       GTK_BUTTONS_CLOSE,
                                       _("Some applications were not successfullly started."));

      gtk_window_set_position (GTK_WINDOW (dialog),
                               GTK_WIN_POS_CENTER);
      
      g_signal_connect (G_OBJECT (dialog),
                        "response",
                        G_CALLBACK (gtk_widget_destroy),
                        NULL);
      
      list = create_failure_list (failed);

      sw = gtk_scrolled_window_new (NULL, NULL);
      gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
                                      GTK_POLICY_AUTOMATIC,
                                      GTK_POLICY_AUTOMATIC);
      gtk_container_set_border_width (GTK_CONTAINER (sw), 3);
      
      gtk_container_add (GTK_CONTAINER (sw), list);

      /* sw as geometry widget */
      gtk_window_set_geometry_hints (GTK_WINDOW (dialog),
                                     sw, NULL, 0);
      /* applies to geometry widget; try to avoid scrollbars,
       * but don't make the window huge
       */
      gtk_window_set_default_size (GTK_WINDOW (dialog),
                                   400, 225);

      gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
                          sw,
                          TRUE, TRUE, 0);
      
      gtk_widget_show_all (dialog);

      /* FIXME should try to do recovery instead of just showing
       * an error, perhaps
       */

      /* Free the failures */
      tmp = failed;
      while (tmp)
        {
          FailedLaunch *fl = tmp->data;

          failed_launch_free (fl);
          
          tmp = g_list_remove (tmp, fl);
        }
    }
}

static gboolean
all_clients_finished (MsmSession *session)
{
  int i;
  
  if (session->startup_info == NULL)
    return FALSE;

  i = 0;
  while (i < session->startup_info->n_apps)
    {
      if (!session->startup_info->apps[i].finished)
        return FALSE;

      ++i;
    }

  return TRUE;
}

void
msm_session_client_registered (MsmSession *session,
                               const char *previous_id)
{
  GList *link;

  if (session->startup_info == NULL)
    return; /* we already timed out this launch */
  
  link = find_client_link_by_id (session, previous_id);

  if (link)
    {
      MsmSavedClient *saved = link->data;
      
      session->startup_info->apps[saved->startup_index].registered = TRUE;
      session->startup_info->apps[saved->startup_index].finished = TRUE;
      msm_startup_end_app (session->startup_info,
                           saved->startup_index);

      if (all_clients_finished (session))
        end_startup (session);
    }
}

MsmSavedClient*
saved_new (MsmSession *session)
{
  MsmSavedClient *saved;
  static int next_serial = 1;
  
  saved = g_new (MsmSavedClient, 1);

  saved->session = session; /* session may be NULL, when parsing */
  saved->id = NULL;
  saved->properties = NULL;
  saved->last_launch_time = 0;
  saved->launch_count = 0;
  saved->error_dialog = NULL;
  saved->serial = next_serial;
  ++next_serial;
  
  return saved;
}

void
saved_free (MsmSavedClient *saved)
{
  g_free (saved->id);

  proplist_free (saved->properties);

  if (saved->error_dialog)
    gtk_widget_destroy (saved->error_dialog);
  
  g_free (saved);
}

static void
set_close_on_exec (int fd)
{
  int val;

  val = fcntl (fd, F_GETFD, 0);
  if (val < 0)
    {
      msm_warning ("couldn't F_GETFD: %s\n", g_strerror (errno));
      return;
    }

  val |= FD_CLOEXEC;

  if (fcntl (fd, F_SETFD, val) < 0)
    msm_warning ("couldn't F_SETFD: %s\n", g_strerror (errno));
}

/* Your basic Stevens cut-and-paste */
static int
lock_reg (int fd, int cmd, int type, off_t offset, int whence, off_t len)
{
  struct flock lock;

  lock.l_type = type; /* F_RDLCK, F_WRLCK, F_UNLCK */
  lock.l_start = offset; /* byte offset relative to whence */
  lock.l_whence = whence; /* SEEK_SET, SEEK_CUR, SEEK_END */
  lock.l_len = len; /* #bytes, 0 for eof */

  return fcntl (fd, cmd, &lock);
}

#define lock_entire_file(fd) \
  lock_reg ((fd), F_SETLK, F_WRLCK, 0, SEEK_SET, 0)
#define unlock_entire_file(fd) \
  lock_reg ((fd), F_SETLK, F_UNLCK, 0, SEEK_SET, 0)

static MsmSession*
msm_session_get_for_filename (const char *name,
                              const char *filename)
{
  MsmSession *session;
  char *full_filename;
  int fd = -1;
  GError *dir_error = NULL;
  GError *err;

  if (!g_path_is_absolute (filename))
    full_filename = g_build_filename (user_session_dir (), 
                                      filename, NULL);
  else
    full_filename = g_strdup (filename);

  session = msm_session_get_cached (full_filename);

  if (session)
    {
      g_free (full_filename);
      return session;
    }

  session = g_new0 (MsmSession, 1);
  session->name = g_strdup (name);
  session->clients = NULL;
  session->filename = g_path_get_basename (filename);
  session->full_filename = full_filename;
  session->lock_fd = -1;
  session->startup_info = NULL;
  
  dir_error = NULL;
  msm_create_dir_and_parents (user_session_dir (), 0700, &dir_error);
  /* We save dir_error for later; if creating the file fails,
   * we give dir_error in the reason.
   */
  
  /* To use a session, we need to lock the file in the user's
   * save dir (by default in .msm/sessions/).
   * 
   * If the file didn't previously exist, then we
   * init the session from the global session of the same name,
   * if any.
   *
   */
  
  fd = open (session->full_filename, O_RDWR | O_CREAT, 0700);
  
  if (fd < 0 && errno == EACCES)
    {
      fd = open (session->full_filename, O_RDONLY);

      if (fd >= 0)
        {
          session->read_only = TRUE;
          msm_verbose (_("Session file '%s' is read-only.\n"), 
                       session->full_filename);
        }
    }

  if (fd < 0)
    {
      char *message;
      
      if (dir_error)
        {
          message = g_strdup_printf (_("Failed to open the session file '%s': "
                                       "%s (%s)"), session->full_filename,
                                                   g_strerror (errno),
                                                   dir_error->message);
                                 
          g_error_free (dir_error);
          dir_error = NULL;
        }
      else
        {
          message = g_strdup_printf (_("Failed to open the session file '%s': "
                                       "%s"), session->full_filename,
                                              g_strerror (errno));
        }
      
      session = recover_failed_session (session,
                                        MSM_SESSION_FAILURE_OPENING_FILE,
                                        message);

      g_free (message);

      return session;
    }
  
  if (dir_error)
    {
      g_error_free (dir_error);
      dir_error = NULL;
    }

  if (!session->read_only && lock_entire_file (fd) < 0)
    {
      char *message;

      close (fd);
      
      message = g_strdup_printf (_("Failed to lock the session file '%s': %s"),
                                 session->full_filename,
                                 g_strerror (errno));
      
      session = recover_failed_session (session,
                                        MSM_SESSION_FAILURE_LOCKING,
                                        message);

      g_free (message);
      
      return session;
    }

  session->lock_fd = fd;
  set_close_on_exec (fd);
  
  err = NULL;
  if (!parse_session_file (session, &err))
    {
      char *message;

      message = g_strdup_printf (_("Failed to parse the session file '%s': %s\n"),
                                 session->full_filename,
                                 err->message);

      g_error_free (err);
      
      session = recover_failed_session (session,
                                        MSM_SESSION_FAILURE_BAD_FILE,
                                        message);

      g_free (message);
      
      return session;
    }

  if (session->clients == NULL)
    {
      session = recover_failed_session (session,
                                        MSM_SESSION_FAILURE_EMPTY,
                                        NULL);

      return session;
    }

  msm_session_cache (session);
  return session;
}

MsmSession*
msm_session_get (const char *name)
{  
  if (name == NULL)
    {
      return msm_session_get_for_filename (_("Default"), "Default.session");
    }
  else
    {
      char *filename;
      char *p; 
      MsmSession *session;
      
      filename = g_strconcat (name, ".session", NULL);

      /* Remove path separators from the filename */
      p = filename;
      while (*p)
        {
          if (*p == G_DIR_SEPARATOR)
            *p = '_';
          ++p;
        }

      session = msm_session_get_for_filename (name, filename);

      g_free (filename);

      return session;
    }
}

MsmSession*
msm_session_get_failsafe  (void)
{  
  MsmSession *session;
  char *filename;
  gboolean global = TRUE;

  /* FIXME: Should user-specific failsafe sessions be allowed?  It complicates
   * things because if the user-specific failsafe session is corrupt then the
   * global failsafe needs to be used instead.  For now just don't allow it.
   */
  filename = get_special_session_path (MSM_SPECIAL_SESSION_FAILSAFE, &global);

  session = msm_session_get_for_filename (_("Failsafe"), filename);

  g_free (filename);

  return session;
}

/* We need this weird fd stuff instead of FILE* because we
 * can't close a file descriptor on the locked session file
 */

static int
fdputs (const char *str, int fd)
{
  return write (fd, str, strlen (str));
}

static int
fdprintf (int fd, const char *format,
          ...)
{
  va_list args;
  gchar *str;
  int retval;
  
  g_return_val_if_fail (format != NULL, -1);
  
  va_start (args, format);
  str = g_strdup_vprintf (format, args);
  va_end (args);

  retval = fdputs (str, fd);
  
  g_free (str);

  return retval;
}

static int
write_proplist (int    fd,
                GList *properties)
{
  GList *tmp;
  int ret;

  ret = 0;
  
  tmp = properties;
  while (tmp != NULL)
    {
      SmProp *prop = tmp->data;
      char *name_encoded;
      char *type_encoded;
      
      name_encoded = encode_text_as_utf8_markup (prop->name);
      type_encoded = encode_text_as_utf8_markup (prop->type);

      ret = fdprintf (fd, "    <prop name=\"%s\" type=\"%s\">\n",
                      name_encoded, type_encoded);

      g_free (name_encoded);
      g_free (type_encoded);

      if (ret < 0)
        return -1;
      
      if (strcmp (prop->type, SmCARD8) == 0)
        {
          int val = 0;
          smprop_get_card8 (prop, &val);
          if (fdprintf (fd, "      <value>%d</value>\n", val) < 0)
            return -1;
        }
      else if (strcmp (prop->type, SmARRAY8) == 0)
        {
          char *str = NULL;
          char *encoded = NULL;
          smprop_get_string (prop, &str);
          if (str)
            encoded = encode_text_as_utf8_markup (str);
          if (encoded)
            ret = fdprintf (fd, "      <value>%s</value>\n", encoded);

          g_free (encoded);
          g_free (str);

          if (ret < 0)
            return ret;
        }
      else if (strcmp (prop->type, SmLISTofARRAY8) == 0)
        {
          char **vec;
          int vec_len;
          int i;
          
          vec = NULL;
          vec_len = 0;
          
          smprop_get_vector (prop, &vec_len, &vec);

          i = 0;
          while (i < vec_len)
            {
              char *encoded;

              encoded = encode_text_as_utf8_markup (vec[i]);
              
              ret = fdprintf (fd, "      <value>%s</value>\n", encoded);

              g_free (encoded);

              if (ret < 0)
                break;
              
              ++i;
            }

          g_strfreev (vec);

          if (ret < 0)
            return -1;
        }
      else
        {
          msm_warning (_("Not saving unknown property type '%s'\n"),
                       prop->type);
        }
      
      if (fdputs ("    </prop>\n", fd) < 0)
        return -1;

      tmp = tmp->next;
    }

  return 0;
}

void
msm_session_save (MsmSession  *session,
                  MsmServer   *server)
{
  /* We save to a secondary file then copy over, to handle
   * out-of-disk-space robustly
   */
  int new_fd;
  char *new_filename = NULL;
  GError *error = NULL;
  char *err_str;
  gboolean write_failed;

  msm_verbose ("Writing session to disk\n");
  
  err_str = NULL;
  new_fd = -1;
  
  if (session->read_only)
    {
      err_str = g_strdup_printf (_("Session '%s' is read-only\n"), 
                                 session->name);

      goto out;
    }

  new_fd = g_file_open_tmp (NULL, &new_filename, &error);

  if (new_fd < 0 && error)
    {
      err_str = g_strdup_printf (_("Failed to open '%s': %s\n"),
                                 new_filename, error->message);
      goto out;
    }

  if (lock_entire_file (new_fd) < 0)
    {
      err_str = g_strdup_printf (_("Failed to lock file '%s': %s"),
                                 new_filename,
                                 g_strerror (errno));
      goto out;
    }  

  write_failed = FALSE;
  
  if (fdputs ("<msm_session>\n", new_fd) < 0)
    {
      write_failed = TRUE;
      goto done_writing;
    }

  {
    GList *tmp;
    tmp = session->clients;
    while (tmp != NULL)
      {
        MsmSavedClient *saved = tmp->data;
        char *encoded;

        encoded = encode_text_as_utf8_markup (saved->id);
        
        if (fdprintf (new_fd, "  <client id=\"%s\">\n",
                      encoded) < 0)
          {
            write_failed = TRUE;
            goto done_writing;
          }

        g_free (encoded);

        if (write_proplist (new_fd, saved->properties) < 0)
          {
            write_failed = TRUE;
            goto done_writing;
          }
        
        if (fdputs ("  </client>\n", new_fd) < 0)
          {
            write_failed = TRUE;
            goto done_writing;
          }

        tmp = tmp->next;
      }
  }

  if (fdputs ("</msm_session>\n", new_fd) < 0)
    {
      write_failed = TRUE;
      goto done_writing;
    }

 done_writing:
  
  if (write_failed)
    {
      err_str = g_strdup_printf (_("Error writing new session file '%s': %s"),
                                 new_filename, g_strerror (errno));
      goto out;
    }
  
  if (rename (new_filename, session->full_filename) < 0)
    {
      if (errno == EXDEV)
        {
          error = NULL;
          if (!msm_copy_file (new_filename, session->full_filename, &error))
            {
              err_str = g_strdup_printf (_("Failed to replace the old session file '%s' with the new session contents in the temporary file '%s': %s"),
                                      session->full_filename,
                                      new_filename, error->message);
              g_error_free (error);
              error = NULL;
              goto out;
            }
          else if (unlink (new_filename) < 0)
            {
              msm_warning (_("Could not remove temporary file '%s': %s\n"),
                           new_filename, g_strerror (errno));
            }
        }
      else
        {
          err_str = g_strdup_printf (_("Failed to replace the old session file '%s' with the new session contents in the temporary file '%s': %s"),
                                 session->full_filename,
                                 new_filename, g_strerror (errno));
          goto out;
        }
    }
  
 out:
  g_free (new_filename);
  
  if (err_str)
    {
      if (new_fd >= 0)
        close (new_fd);
      
      dialog_failed_to_save_session (err_str);

      g_free (err_str);
    }
  else
    {
      if (session->lock_fd >= 0)
        close (session->lock_fd);
      session->lock_fd = new_fd;
      set_close_on_exec (new_fd);

      dialog_save_complete ();
    }
}

void
msm_session_purge_clients (MsmSession *session,
                           MsmServer  *server)
{
  /* Drop all clients that have the wrong restart style
   * or have IfRunning and aren't running
   */
  GList *tmp;
  GList *copy;

  copy = g_list_copy (session->clients); /* since we can remove in-loop */
  tmp = copy;
  while (tmp != NULL)
    {
      MsmSavedClient *saved = tmp->data;
      int restart_style;

      if (!proplist_find_card8 (saved->properties,
                                SmRestartStyleHint,
                                &restart_style))
        restart_style = SmRestartIfRunning;

      switch (restart_style)
        {
        case SmRestartNever:
          saved_remove (saved);
          break;
        case SmRestartIfRunning:
          if (saved->id &&
              !msm_server_client_id_in_use (server,
                                            saved->id))
            {
              char *name;

              name = saved_get_program (saved);
              
              msm_verbose ("Removing client '%s' because it isn't running and it's RestartIfRunning\n",
                           name);

              g_free (name);
              
              saved_remove (saved);
            }
          break;
        default:
          /* Leave it in session */
          break;
        }
      
      tmp = tmp->next;
    }

  g_list_free (copy);
}

static GList *
get_session_list (void)
{
  static GList *session_path = NULL;
  GList *tmp = NULL, *session_list = NULL;

  /* FIXME: This should be globally accessible and optionally created from an 
   * environment variable (like MSM_SESSION_PATH or something).
   */
  if (session_path == NULL)
    {
      session_path = g_list_prepend (session_path, 
                                     (gpointer) user_session_dir ());
      session_path = g_list_prepend (session_path, 
                                     (gpointer) global_session_dir ()); 
    }

  tmp = session_path;
  while (tmp != NULL) 
    {
      GDir *dir;
      const char *filename, *dir_path;
      GError *error = NULL;

      dir_path = (const char *) tmp->data;
      dir = g_dir_open (dir_path, 0, &error);

      if (error != NULL)
        {
          msm_warning ("Could not open directory '%s': %s\n", 
                       dir_path, error->message);
          g_error_free (error);
          continue;
        }

      while ((filename = g_dir_read_name (dir)) != NULL)
        {
          char *full_filename = NULL;

          if (!g_str_has_suffix (filename, ".session"))
            continue;

          full_filename = g_build_filename (dir_path, filename, NULL);

          session_list = g_list_prepend (session_list, full_filename);
        }

      g_dir_close (dir);

      tmp = tmp->next;
    }

  session_list = g_list_reverse (session_list);

  return session_list;
}

static MsmSession*
msm_session_get_from_user (void)
{
  GtkListStore *store; 
  GtkTreeIter iter;
  GtkWidget *window, *label, *tree; 
  GtkBox *vbox;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  GList *tmp;
  int response;
  char *filename, *session_name;
  MsmSession *session = NULL;
  
  enum 
    {
      MSM_SESSION_RESPONSE_CANCEL,
      MSM_SESSION_RESPONSE_SELECT
    };

  enum
    {
      MSM_SESSION_LIST_ITEM_NAME=0,
      MSM_SESSION_LIST_ITEM_STATUS,
      MSM_SESSION_LIST_ITEM_FILENAME,
      MSM_SESSION_NUMBER_OF_LIST_ITEMS
    };
  
  window = gtk_dialog_new_with_buttons (_("Select a session"), NULL, 
                                        GTK_DIALOG_MODAL | 
                                        GTK_DIALOG_NO_SEPARATOR,
                                        GTK_STOCK_CANCEL, 
                                        MSM_SESSION_RESPONSE_CANCEL,
                                        _("Load Session"), 
                                        MSM_SESSION_RESPONSE_SELECT,
                                        NULL);
  gtk_container_set_border_width (GTK_CONTAINER (window), 6);
  
  vbox = GTK_BOX (GTK_DIALOG (window)->vbox);
  
  gtk_box_set_spacing (vbox, 12);
  
  gtk_window_set_position (GTK_WINDOW (window), GTK_WIN_POS_CENTER_ALWAYS);
  
  label = gtk_label_new (_("<span weight=\"bold\" size=\"larger\">"
                           "Select a session"
                           "</span>\n\n"
                           "Please select a session to load from the "
                           "list below."));
  gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_misc_set_padding (GTK_MISC (label), 6, 0);
  
  gtk_box_pack_start (vbox, label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  
  store = gtk_list_store_new (MSM_SESSION_NUMBER_OF_LIST_ITEMS, 
                              G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
  
  for (tmp = get_session_list (); tmp != NULL; tmp = tmp->next)
    {
      GError *error = NULL;
      char *basename, *status = "";

      filename = (char *) tmp->data; 
      
      basename = g_path_get_basename (filename);

      session_name = basename;

      basename = g_strrstr (session_name, ".session");
      
      if (basename == NULL)
        {
          g_free (session_name);
          continue;
        }

      basename[0] = '\0';

      if (!msm_file_is_readable (filename, &error))
        {
          if (error != NULL)
            {
              msm_warning ("%s", error->message);
              g_error_free (error);
              error = NULL;
            }
          continue;
        }

      if (!msm_file_is_writable (filename, &error))
        {
          if (error != NULL)
            {
              msm_warning ("%s", error->message);
              g_error_free (error);
              error = NULL;
            }
          else
            {
              status = _("(read-only)");
            }
        }
      else
        {
          status = "";
        }

      gtk_list_store_append (store, &iter);
      gtk_list_store_set (store, &iter, 
                          MSM_SESSION_LIST_ITEM_NAME, session_name, 
                          MSM_SESSION_LIST_ITEM_STATUS, status, 
                          MSM_SESSION_LIST_ITEM_FILENAME, filename, -1); 

      g_free (session_name);
    }
  g_list_foreach (tmp, (GFunc) g_free, NULL);
  g_list_free (tmp);
  
  tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
  
  gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree), FALSE);
  
  renderer = gtk_cell_renderer_text_new ();
  
  column = gtk_tree_view_column_new_with_attributes (_("Session"), renderer,
                                                     "text", 
                                                     MSM_SESSION_LIST_ITEM_NAME,
                                                     NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

  column = gtk_tree_view_column_new_with_attributes (_("Status"), renderer,
                                                   "text", 
                                                   MSM_SESSION_LIST_ITEM_STATUS,
                                                   NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

  gtk_box_pack_start (vbox, tree, TRUE, TRUE, 6);
  gtk_widget_show (tree);
  
  response = gtk_dialog_run (GTK_DIALOG (window));

  switch (response)
    {
    case MSM_SESSION_RESPONSE_CANCEL:
      session = NULL;
      gtk_widget_destroy (window);
      break;
    case MSM_SESSION_RESPONSE_SELECT: 
      {
        GtkTreeSelection *selection = 
            gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
        GtkTreeModel *model = GTK_TREE_MODEL (store);

        if (selection 
              && gtk_tree_selection_get_selected (selection, &model, &iter))
          {
            gtk_tree_model_get (model, &iter, 
                                MSM_SESSION_LIST_ITEM_NAME, &session_name,
                                MSM_SESSION_LIST_ITEM_FILENAME, &filename, 
                                -1);

            gtk_widget_destroy (window);
            session = msm_session_get_for_filename (session_name, filename);
          }
        else
          {
            gtk_widget_destroy (window);
            session = NULL;
          }
     
        g_object_unref (store);
      }
      break;
    }
  
  return session;
}

static MsmSession *
msm_session_revert_to_defaults (MsmSession *session)
{
  MsmSession *new_session;
  char *default_session_file;
  gboolean global = FALSE;
  GError *error = NULL;

  default_session_file = get_special_session_path (MSM_SPECIAL_SESSION_DEFAULT,
                                                   &global);

  if (global)
    {
      /* FIXME: Ask the user if it's okay. 
       */
      msm_warning ("User default session is unavailable, using global default "
                   "session");
    }

  if (!msm_copy_file (default_session_file, session->full_filename, &error))
    {
      /* FIXME: handle error case more responsibly.
       */
      msm_warning ("Could not copy file '%s' to '%s': %s\n",
                   default_session_file, session->full_filename, 
                   error->message? error->message : "unknown error");

      g_error_free (error);
      error = NULL;
    }

  new_session = msm_session_get_for_filename (session->name, 
                                              session->full_filename);

  return new_session;
}

static MsmSession*
recover_failed_session (MsmSession             *session,
                        MsmSessionFailureReason reason,
                        const char             *details)
{
  MsmSession *recovery_session = NULL;
  GtkWidget *dialog;
  int response;

  enum {
    MSM_SESSION_RESPONSE_ABORT = 0,
    MSM_SESSION_RESPONSE_IGNORE,
    MSM_SESSION_RESPONSE_SELECT,
    MSM_SESSION_RESPONSE_TRY_AGAIN,
    MSM_SESSION_RESPONSE_DEFAULT,
    MSM_SESSION_RESPONSE_FAILSAFE,
    MSM_SESSION_RESPONSE_REVERT
  };

  switch (reason)
    {
    case MSM_SESSION_FAILURE_OPENING_FILE:
      dialog = gtk_message_dialog_new (NULL,
                                       0,
                                       GTK_MESSAGE_ERROR,
                                       GTK_BUTTONS_NONE,
                                       _("Could not open the session \"%s.\""),
                                       session->name);

      gtk_dialog_add_buttons (GTK_DIALOG (dialog), 
                              _("Abort login"), 
                              MSM_SESSION_RESPONSE_ABORT,
                              _("Log into other session"), 
                              MSM_SESSION_RESPONSE_SELECT, 
                              _("Log into default session"), 
                              MSM_SESSION_RESPONSE_DEFAULT,
                              _("Log into failsafe session"),
                              MSM_SESSION_RESPONSE_FAILSAFE,
                              NULL);
      break;
      
    case MSM_SESSION_FAILURE_LOCKING:
      dialog = gtk_message_dialog_new (NULL,
                                       0,
                                       GTK_MESSAGE_ERROR,
                                       GTK_BUTTONS_NONE,
                                       _("You are already logged in elsewhere, "
                                         "using the session \"%s.\" You can "
                                         "only use a session from one location "
                                         "at a time."), session->name);
      gtk_dialog_add_buttons (GTK_DIALOG (dialog), 
                              _("Log in anyway"),
                              MSM_SESSION_RESPONSE_IGNORE,
                              _("Try again"),
                              MSM_SESSION_RESPONSE_TRY_AGAIN,
                              _("Log into other session"),
                              MSM_SESSION_RESPONSE_SELECT,
                              _("Log into failsafe session"),
                              MSM_SESSION_RESPONSE_FAILSAFE,
                              NULL);
      break;
      
    case MSM_SESSION_FAILURE_BAD_FILE:
      dialog = gtk_message_dialog_new (NULL,
                                       0,
                                       GTK_MESSAGE_ERROR,
                                       GTK_BUTTONS_NONE,
                                       _("The session file for session \"%s\" "
                                         "appears to be invalid or corrupted."),
                                         session->name);

      gtk_dialog_add_buttons (GTK_DIALOG (dialog), 
                              _("Revert session to defaults"),
                              MSM_SESSION_RESPONSE_REVERT,
                              _("Log into other session"),
                              MSM_SESSION_RESPONSE_SELECT,
                              _("Log into default session"),
                              MSM_SESSION_RESPONSE_DEFAULT,
                              NULL);
      break;
      
    case MSM_SESSION_FAILURE_EMPTY:
      dialog = gtk_message_dialog_new (NULL,
                                       0,
                                       GTK_MESSAGE_ERROR,
                                       GTK_BUTTONS_NONE,
                                       _("The session \"%s\" contains no "
                                         "applications."), session->name);

      gtk_dialog_add_buttons (GTK_DIALOG (dialog), 
                              _("Revert session to defaults"),
                              MSM_SESSION_RESPONSE_REVERT,
                              _("Log into other session"),
                              MSM_SESSION_RESPONSE_SELECT,
                              _("Log into default session"),
                              MSM_SESSION_RESPONSE_DEFAULT,
                              NULL);
      break;

    default:
      dialog = gtk_message_dialog_new (NULL,
                                       0,
                                       GTK_MESSAGE_ERROR,
                                       GTK_BUTTONS_NONE,
                                       _("The session \"%s\" has encountered "
                                         "an unrecoverable error."),
                                         session->name);
      gtk_dialog_add_buttons (GTK_DIALOG (dialog), 
                              _("Revert session to defaults"),
                              MSM_SESSION_RESPONSE_REVERT,
                              _("Log into other session"),
                              MSM_SESSION_RESPONSE_SELECT,
                              _("Log into default session"),
                              MSM_SESSION_RESPONSE_DEFAULT,
                              NULL);

      break;
    }
  
  gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_CENTER);

  if (details)
    dialog_add_details (GTK_DIALOG (dialog), details);

  gtk_widget_show (dialog);
  
  response = gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);

  switch (response)
    {
    case MSM_SESSION_RESPONSE_ABORT:
      msm_verbose ("Aborting login\n");
      recovery_session = NULL;
      msm_session_free (session);
      msm_quit ();
      break;
    case MSM_SESSION_RESPONSE_IGNORE:
      msm_verbose ("Ignoring login problem\n");
      recovery_session = session;
      break;
    case MSM_SESSION_RESPONSE_SELECT:
      msm_verbose ("Querying user for different login session\n");
      recovery_session = msm_session_get_from_user ();

      if (recovery_session == NULL)
        recovery_session = recover_failed_session (session, 
                                               MSM_SESSION_FAILURE_OPENING_FILE,
                                               _("User selected session could "
                                                 "not be loaded."));

      msm_session_free (session);
      break;
    case MSM_SESSION_RESPONSE_TRY_AGAIN:
      msm_verbose ("Trying session again\n");
      recovery_session = msm_session_get_for_filename (session->name,
                                                       session->full_filename);
      msm_session_free (session);
      break;
    case MSM_SESSION_RESPONSE_DEFAULT: 
      {
        char *filename;

        msm_verbose ("Using default read-only session\n");
        filename = g_build_filename (global_session_dir (),
                                     "Default.session", NULL);

        recovery_session = msm_session_get_for_filename (_("Default"), 
                                                         filename);
        g_free (filename);
        recovery_session->read_only = TRUE;
        msm_session_free (session);
        break;
      }
    case MSM_SESSION_RESPONSE_FAILSAFE:
      msm_verbose ("Using failsafe session\n");
      recovery_session = msm_session_get_failsafe ();
      msm_session_free (session);
      break;
    case MSM_SESSION_RESPONSE_REVERT:
      msm_verbose ("Reverting session to default values\n");
      recovery_session = msm_session_revert_to_defaults (session);
      msm_session_free (session);
      break;
    }

  return recovery_session;
}

typedef struct _ParseData ParseData;

typedef enum
{
  PARSE_STATE_START,
  PARSE_STATE_IN_SESSION,
  PARSE_STATE_IN_CLIENT,
  PARSE_STATE_IN_PROP,
  PARSE_STATE_IN_VALUE,
  PARSE_STATE_DONE
} ParseState;

struct _ParseData
{
  ParseState state;
  GList *completed_clients;
  MsmSavedClient *current_client;
  SmProp *current_prop;
  char *value_text;
  int value_count;
};

static void
set_parse_error (GError             **error,
                 GMarkupParseContext *context,
                 const char          *format,
                 ...)
{
  va_list args;
  gchar *str;
  int l, c;
  
  g_return_if_fail (format != NULL);
  
  va_start (args, format);
  str = g_strdup_vprintf (format, args);
  va_end (args);

  g_markup_parse_context_get_position (context, &l, &c);

  g_set_error (error, MSM_ERROR, MSM_ERROR_FAILED,
               _("line %d character %d: %s\n"),
               l, c, str);

  g_free (str);
}

static MsmSavedClient*
start_client (GMarkupParseContext *context,
              ParseData           *pd,
              const char         **attribute_names,
              const char         **attribute_values,
              GError             **error)
{
  if (attribute_names[0] == NULL)
    {
      set_parse_error (error, context,
                       _("<client> element must have an 'id' attribute giving the session ID"));
      return NULL;
    }
  else if (strcmp (attribute_names[0], "id") != 0)
    {
      set_parse_error (error, context,
                       _("<client> element does not allow the attribute '%s', only an 'id' attribute"),
                       attribute_names[0]);
      return NULL;
    }
  else if (attribute_names[1])
    {
      set_parse_error (error, context,
                       _("<client> element does not allow the attribute '%s', only an 'id' attribute"),
                       attribute_names[1]);
      return NULL;
    }
  else if (*(attribute_values[0]) == '\0')
    {
      set_parse_error (error, context,
                       _("'id' attribute of <client> element has empty value"));
      return NULL;
    }
  else
    {
      MsmSavedClient *saved;

      if (find_client_link_by_id_in_list (pd->completed_clients,
                                          attribute_values[0]))
        {
          set_parse_error (error, context,
                           _("'id' attribute of <client> element duplicates another <client> element's id (duplicate id is '%s')"),
                           attribute_values[0]);
          return NULL;
        }

      
      saved = saved_new (NULL);
      saved->id = g_strdup (attribute_values[0]);
      
      return saved;
    }
}

static SmProp*
start_property (GMarkupParseContext *context,
                const char         **attribute_names,
                const char         **attribute_values,
                GError             **error)
{
  int i;
  char *name;
  char *type;
  char *decoded;
  SmProp *prop;

  prop = NULL;
  name = NULL;
  type = NULL;
  i = 0;
  while (attribute_names[i])
    {
      const char *attr = attribute_names[i];
      const char *val = attribute_values[i];
      
      if (strcmp (attr, "name") == 0)
        {
          if (name != NULL)
            {
              set_parse_error (error, context,
                               _("'name' attribute specified twice on <prop> element"));
              goto out;
            }
          
          name = g_strdup (val);
        }
      else if (strcmp (attr, "type") == 0)
        {
          if (type != NULL)
            {
              set_parse_error (error, context,
                               _("'type' attribute specified twice on <prop> element"));
              goto out;
            }

          type = g_strdup (val);
        }
      else
        {
          set_parse_error (error, context,
                           _("Attribute '%s' not allowed on <prop> element"),
                           attr);
          goto out;
        }

      ++i;
    }

  if (type == NULL)
    {
      set_parse_error (error, context,
                       _("<prop> element must have a 'type' attribute"));
      goto out;
    }
  else if (name == NULL)
    {
      set_parse_error (error, context,
                       _("<prop> element must have a 'name' attribute"));
      goto out;
    }

  decoded = decode_text_from_utf8 (type);
  g_free (type);
  type = decoded;

  decoded = decode_text_from_utf8 (name);
  g_free (name);
  name = decoded;
  
  if (strcmp (type, SmCARD8) == 0)
    {
      prop = smprop_new_card8 (name, 0);
    }
  else if (strcmp (type, SmARRAY8) == 0)
    {
      prop = smprop_new_string (name, "", 0);

    }
  else if (strcmp (type, SmLISTofARRAY8) == 0)
    {
      prop = smprop_new_vector (name, 0, NULL);
    }
  else
    {
      set_parse_error (error, context,
                       _("<prop> type '%s' is unknown, should be %s, %s, or %s"),
                       type, SmCARD8, SmARRAY8, SmLISTofARRAY8);
      goto out;
    }
  
 out:
  if (name)
    g_free (name);
  if (type)
    g_free (type);
  return prop;
}

/* Called for open tags <foo bar="baz"> */
static void
parse_start_element  (GMarkupParseContext *context,
                      const gchar         *element_name,
                      const gchar        **attribute_names,
                      const gchar        **attribute_values,
                      gpointer             user_data,
                      GError             **error)
{
  ParseData *pd = user_data;

  switch (pd->state)
    {
    case PARSE_STATE_START:
      if (strcmp (element_name, "msm_session") != 0)
        {
          set_parse_error (error, context,
                           _("File does not appear to be a session file; outermost element is <%s> rather than <msm_session>"),
                           element_name);
        }
      else
        {
          pd->state = PARSE_STATE_IN_SESSION;
        }
      break;

    case PARSE_STATE_IN_SESSION:
      if (strcmp (element_name, "client") != 0)
        {
          set_parse_error (error, context,
                           _("Element <%s> may not appear inside the <msm_session> element"),
                           element_name);
        }
      else
        {
          /* Create new client */
          pd->state = PARSE_STATE_IN_CLIENT;
          g_assert (pd->current_client == NULL);
          pd->current_client = start_client (context,
                                             pd,
                                             attribute_names,
                                             attribute_values,
                                             error);
        }
      break;      

    case PARSE_STATE_IN_CLIENT:
      if (strcmp (element_name, "prop") != 0)
        {
          set_parse_error (error, context,
                           _("Element <%s> may not appear inside a <client> element"),
                           element_name);
        }
      else
        {
          /* Create new property */
          pd->state = PARSE_STATE_IN_PROP;
          g_assert (pd->current_prop == NULL);
          pd->current_prop = start_property (context,
                                             attribute_names,
                                             attribute_values,
                                             error);
        }
      break;

    case PARSE_STATE_IN_PROP:
      if (strcmp (element_name, "value") != 0)
        {
          set_parse_error (error, context,
                           _("Element <%s> may not appear inside a <prop> element"),
                           element_name);
        }
      else if (attribute_names[0] != NULL)
        {
          set_parse_error (error, context,
                           _("Attributes are not allowed on the <value> element"));
        }
      else
        {
          pd->state = PARSE_STATE_IN_VALUE;
        }
      break;

    case PARSE_STATE_IN_VALUE:
      set_parse_error (error, context,
                       _("No elements allowed inside <value> element (<%s> found)"),
                       element_name);
      break;
      
    case PARSE_STATE_DONE:
      set_parse_error (error, context,
                       _("Element <%s> appears after the <msm_session> element"),
                       element_name);
      break;
    }
}

/* Called for close tags </foo> */
static void
parse_end_element    (GMarkupParseContext *context,
                      const gchar         *element_name,
                      gpointer             user_data,
                      GError             **error)
{
  ParseData *pd = user_data;

  switch (pd->state)
    {
    case PARSE_STATE_START:
      set_parse_error (error, context,
                       _("Unexpected close element tag </%s>"),
                       element_name);
      break;

    case PARSE_STATE_IN_SESSION:
      if (strcmp (element_name, "msm_session") != 0)
        {
          set_parse_error (error, context,
                           _("Unexpected close element tag </%s>"),
                           element_name);
        }
      else
        {
          pd->state = PARSE_STATE_DONE;
        }
      break;      

    case PARSE_STATE_IN_CLIENT:
      if (strcmp (element_name, "client") != 0)
        {
          set_parse_error (error, context,
                           _("Unexpected close element tag </%s>"),
                           element_name);
        }
      else
        {
          g_assert (pd->current_client);
          
          pd->completed_clients = g_list_prepend (pd->completed_clients,
                                                  pd->current_client);
          pd->current_client = NULL;

          pd->state = PARSE_STATE_IN_SESSION;
        }
      break;

    case PARSE_STATE_IN_PROP:
      if (strcmp (element_name, "prop") != 0)
        {
          set_parse_error (error, context,
                           _("Unexpected close element tag </%s>"),
                           element_name);
        }
      else
        {
          g_assert (pd->current_client);
          g_assert (pd->current_prop);
          
          pd->current_client->properties =
            g_list_prepend (pd->current_client->properties,
                            pd->current_prop);
          pd->current_prop = NULL;
          
          pd->state = PARSE_STATE_IN_CLIENT;
          pd->value_count = 0;
        }
      break;

    case PARSE_STATE_IN_VALUE:
      if (strcmp (element_name, "value") != 0)
        {
          set_parse_error (error, context,
                           _("Unexpected close element tag </%s>"),
                           element_name);
        }
      else
        {
          g_assert (pd->current_client);
          g_assert (pd->current_prop);

          pd->value_count += 1;

          if (pd->value_text == NULL)
            {
              set_parse_error (error, context,
                               _("<value> element has no contents"));
            }
          else if (strcmp (pd->current_prop->type, SmLISTofARRAY8) != 0 &&
                   (pd->value_count == 0 || pd->value_count > 1))
            {
              set_parse_error (error, context,
                               _("Exactly one <value> element required below <prop> elements other than type LISTofARRAY8 - no more and no less"));
            }
          else
            {
              g_assert (pd->value_text);
              
              if (strcmp (pd->current_prop->type, SmCARD8) == 0)
                {
                  char *end;
                  long result;

                  result = strtol (pd->value_text, &end, 10);
                  
                  if (end == pd->value_text)
                    {
                      set_parse_error (error, context,
                                       _("Failed to parse '%s' (given as value of a CARD8 property) as an integer"),
                                       pd->value_text);
                    }
                  else if (result < 0 || result > 255)
                    {
                      set_parse_error (error, context,
                                       _("Only values between 0 and 255 are allowed in CARD8 properties (%ld too large)"),
                                       result);
                    }
                  else
                    {
                      smprop_set_card8 (pd->current_prop, (unsigned char) result);
                    }
                }
              else if (strcmp (pd->current_prop->type, SmARRAY8) == 0)
                {
                  char *text;

                  text = decode_text_from_utf8 (pd->value_text);
                  
                  smprop_set_string (pd->current_prop,
                                     text, -1);

                  g_free (text);
                }
              else if (strcmp (pd->current_prop->type, SmLISTofARRAY8) == 0)
                {
                  char *text;
                                    
                  text = decode_text_from_utf8 (pd->value_text);

                  smprop_append_to_vector (pd->current_prop,
                                           text);

                  g_free (text);
                }
              else
                {
                  g_assert_not_reached ();
                }

              g_free (pd->value_text);
              pd->value_text = NULL;
              pd->state = PARSE_STATE_IN_PROP;
            }
        }
      break;
      
    case PARSE_STATE_DONE:
      set_parse_error (error, context,
                       _("Element <%s> appears after the <msm_session> element"),
                       element_name);
      break;
    }
}

/* Called for character data */
/* text is not nul-terminated */
static void
parse_text           (GMarkupParseContext *context,
                      const gchar         *text,
                      gsize                text_len,  
                      gpointer             user_data,
                      GError             **error)
{
  ParseData *pd = user_data;

  if (pd->state == PARSE_STATE_IN_VALUE)
    {
      g_return_if_fail (pd->value_text == NULL);
          
      pd->value_text = g_strndup (text, text_len);
    }
  else
    {
      const char *p;
      const char *end;

      p = text;
      end = text + text_len;
      
      while (p != end)
        {
          if (!g_ascii_isspace (*p))
            {
              set_parse_error (error, context,
                               _("Unexpected text outside of <value> element"));
              break;
            }
          
          ++p;
        }
    }
}

/* Called for strings that should be re-saved verbatim in this same
 * position, but are not otherwise interpretable.  At the moment
 * this includes comments and processing instructions.
 */
/* text is not nul-terminated. */
static void
parse_passthrough    (GMarkupParseContext *context,
                      const gchar         *passthrough_text,
                      gsize                text_len,  
                      gpointer             user_data,
                      GError             **error)
{
  /* Ignore comments etc., though really we should try to preserve
   * them.
   */
}

/* Called on error, including one set by other
 * methods in the vtable. The GError should not be freed.
 */
static void
parse_error          (GMarkupParseContext *context,
                      GError              *error,
                      gpointer             user_data)
{
  /* Do nothing, the error is handled by the caller. */

} 

static void
fill_in_session_field (gpointer data, gpointer user_data)
{
  MsmSavedClient *saved = data;
  g_assert (saved->session == NULL);
  saved->session = user_data;
}

static const GMarkupParser session_parser = {
  parse_start_element,
  parse_end_element,
  parse_text,
  parse_passthrough,
  parse_error
};

static gboolean
parse_session_file (MsmSession *session,
                    GError    **error)
{
  char *parse_file;
  struct stat sb;
  gboolean file_empty;
  ParseData parse_data;
  GMarkupParseContext *context;
  char *text;
  int len;
  gboolean result = FALSE;

  g_return_val_if_fail (session->clients == NULL, FALSE);
  
  parse_file = NULL;
  file_empty = FALSE;
  
  /* If the file is empty, probably because we just created it or have
   * never saved our session, then parse the global session file
   * instead of the user session file for our initial state.
   */
  if (fstat (session->lock_fd, &sb) < 0)
    {
      /* Can't imagine this actually happening */
      msm_warning (_("Failed to stat new session file descriptor (%s)\n"),
                   g_strerror (errno));
    }
  else
    {
      if (sb.st_size == 0)
        file_empty = TRUE;
    }

  if (file_empty)
    {
      parse_file = g_build_filename (global_session_dir (), 
                                     session->filename, NULL);

      if (stat (parse_file, &sb) < 0)
        {
           g_set_error (error, G_FILE_ERROR, g_file_error_from_errno (errno),
                        _("Session is empty and no global session "
                          "available.\n"));

        }
    }
  else
    parse_file = g_strdup (session->full_filename);

  if (!*error)
    result = g_file_get_contents (parse_file, &text, &len, error);

  g_free (parse_file);

  if (!result)
    return FALSE;
  
  parse_data.state = PARSE_STATE_START;
  parse_data.completed_clients = NULL;
  parse_data.current_client = NULL;
  parse_data.current_prop = NULL;
  parse_data.value_text = NULL;
  parse_data.value_count = 0;
  
  context = g_markup_parse_context_new (&session_parser,
                                        0, &parse_data, NULL);

  if (!g_markup_parse_context_parse (context,
                                     text,
                                     len,
                                     error))
    goto error;
  
  
  if (!g_markup_parse_context_end_parse (context, error))
    goto error;

  g_markup_parse_context_free (context);

  session->clients = g_list_reverse (parse_data.completed_clients);
  g_list_foreach (session->clients,
                  fill_in_session_field,
                  session);

  g_assert (parse_data.current_client == NULL);
  g_assert (parse_data.current_prop == NULL);
  g_assert (parse_data.value_text == NULL);
  
  return TRUE;

 error:

  g_list_foreach (parse_data.completed_clients,
                  (GFunc) saved_free, NULL);

  g_list_free (parse_data.completed_clients);

  if (parse_data.current_client)
    saved_free (parse_data.current_client);

  if (parse_data.current_prop)
    SmFreeProperty (parse_data.current_prop);

  g_free (parse_data.value_text);
  
  return FALSE;
}

static char*
encode_text_as_utf8_markup (const char *text)
{
  /* text can be any encoding, and is nul-terminated.
   * we pretend it's Latin-1 and encode as UTF-8
   */
  GString *str;
  const char *p;
  char *escaped;
  
  str = g_string_new ("");

  p = text;
  while (*p)
    {
      g_string_append_unichar (str, *p);
      ++p;
    }

  escaped = g_markup_escape_text (str->str, str->len);
  g_string_free (str, TRUE);
  
  return escaped;
}

static char*
decode_text_from_utf8 (const char *text)
{
  /* Convert back from the encoded UTF-8 (not the inverse of
   * encode_text because the markup parser escapes automatically)
   */
  GString *str;
  const char *p;

  str = g_string_new ("");
  
  p = text;
  while (*p)
    {
      /* obviously this barfs if the UTF-8 contains chars > 255 */
      g_string_append_c (str, g_utf8_get_char (p));

      p = g_utf8_next_char (p);
    }

  return g_string_free (str, FALSE);
}

static void
add_environment_func (gpointer data)
{
  char **envv = data;
  int i;

  i = 0;
  while (envv[i])
    {
      putenv (envv[i]);
      ++i;
    }
}

gboolean
msm_run_command (const char   *command,
                 GList        *proplist,
                 GError      **error)
{
  int argc = 0;
  char **argv = NULL;
  int envc = 0;
  char **envv = NULL;
  gboolean success = FALSE;
  char *dir = NULL;

  msm_verbose ("Running command '%s'\n", command);
  
  if (!proplist_find_vector (proplist, command,
                             &argc, &argv))
    {
      g_set_error (error,
                   MSM_ERROR, MSM_ERROR_FAILED,
                   _("No '%s' command provided by the application"), command);
      goto cleanup;
    }

  proplist_find_string (proplist, SmCurrentDirectory,
                        &dir);

  proplist_find_vector (proplist, SmEnvironment, &envc, &envv);

  /* convert from alternating-names-values to name=value format */
  if (envc > 0 && (envc % 2) == 0)
    {
      int i;

      envc /= 2;
      
      i = 0;
      while (i < envc)
        {
          char *tmp;
          
          tmp = g_strconcat (envv[2*i], "=", envv[2*i + 1], NULL);

          g_free (envv[2*i]);
          g_free (envv[2*i + 1]);
          envv[2*i] = NULL;
          envv[2*i + 1] = NULL;
          
          envv[i] = tmp;
          
          ++i;
        }
    }
  else
    {
      msm_verbose ("Client had Environment property but vector had %d elements\n",
                   envc);
    }

  msm_verbose ("argv[0] = '%s' dir = '%s'\n",
               argv[0] ? argv[0] : "null",
               dir ? dir : "null");
  
  if (!g_spawn_async (dir,
                      argv,
                      NULL,
                      G_SPAWN_SEARCH_PATH,
                      envv ? add_environment_func : NULL,
                      envv,
                      NULL,
                      error))
    goto cleanup;

  success = TRUE;
  
 cleanup:

  if (argv)
    g_strfreev (argv);
  if (envv)
    g_strfreev (envv);
  if (dir)
    g_free (dir);
  
  return success;
}

void
msm_run_discard (const char *client_name,
                 GList      *proplist)
{
  if (proplist_find_by_name (proplist, SmDiscardCommand) != NULL)
    {
      GError *err;
      
      err = NULL;
      if (!msm_run_command (SmDiscardCommand,
                            proplist,
                            &err))
        {          
          msm_warning (_("Failed to clean up saved session data for '%s': %s"),
                       client_name, err->message);
          
          g_error_free (err);
        }
    }
}

static char *
build_session_path (const char *base_path)
{
  char *path;

  g_return_val_if_fail (base_path != NULL, NULL);

  path = g_build_filename (base_path, "sessions", NULL);

  return path;
}

static const char *
user_session_dir (void)
{
  static char *dir = NULL;

  if (dir == NULL)
    {
      dir = build_session_path (msm_get_work_directory ());
    }

  return dir;
}

static const char*
global_session_dir (void)
{
  static char *dir = NULL;
               
  if (dir == NULL)
  {
    dir = build_session_path (MSM_PKGDATADIR);
  }

  return dir;
}

/* global is an optional in-out variable.  If it is set to false, then
 * sessions will first be looked for in the local user directory, otherwise
 * only the global directory will be checked. If global is NULL then it
 * defaults to false.
 */
static char *
get_special_session_path (MsmSpecialSession session, gboolean *global)
{
  char *full_filename = NULL, *filename = NULL;

  switch (session)
    {
    case MSM_SPECIAL_SESSION_DEFAULT:
      filename = "Default.session";
      break;
    case MSM_SPECIAL_SESSION_FAILSAFE:
      filename = "Failsafe.session";
      break;
    default:
      g_assert_not_reached ();
    }

  if (!global || (global && !*global))
    {
      full_filename = g_build_filename (user_session_dir (), filename, NULL);

      if (msm_file_is_readable (full_filename, NULL))
          return full_filename;

      g_free (full_filename);
    }

  full_filename = g_build_filename (global_session_dir (), filename, NULL);

  if (msm_file_is_readable (full_filename, NULL))
    {
      if (global)
        *global = TRUE;

      return full_filename;
    }

  g_free (full_filename);

  return NULL;
}

static MsmSession*
msm_session_get_cached (const char *filename)
{
   MsmSession *session = NULL;

   g_return_val_if_fail (filename != NULL, NULL);

   if (sessions)
     {
       session = g_hash_table_lookup (sessions, filename);
     }

   return session;
}

static void
msm_session_cache (MsmSession *session)
{
  MsmSession *cached_session;

  if (!sessions)
    sessions = g_hash_table_new (g_str_hash, g_str_equal);

  cached_session = msm_session_get_cached (session->full_filename);

  if (cached_session)
    {
      msm_warning ("Tried to cache duplicate session\n");
      return;
    }

  g_hash_table_insert (sessions, session->full_filename, session);
}

static void
msm_session_free (MsmSession *session)
{
  if (!session)
    return;

  if (sessions)
    g_hash_table_remove (sessions, session->full_filename);
  
  if (session->lock_fd >= 0)
    {
      unlock_entire_file (session->lock_fd);
      close (session->lock_fd);
    }

  g_free (session);
}



