/* msm startup progress indicator */

/* 
 * Copyright (C) 2001 Havoc Pennington
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "startup.h"
#include "util.h"
#include "splashwindow.h"
#include <gtk/gtkwindow.h>
#include <gtk/gtkprogressbar.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkimage.h>
#include <gtk/gtktable.h>
#include <gtk/gtkframe.h>
#include <gmodule.h>

#ifdef MSM_CLIENT_H
#error "If you include other msm files in here, you'll break compilation of test-startup"
#endif

#include <config.h>

static gboolean fallback_init              (void);
static void     fallback_shutdown          (void);
static int      fallback_get_shutdown_time (void);
static void     fallback_begin             (MsmStartupInfo *info);
static void     fallback_end               (MsmStartupInfo *info);
static void     fallback_begin_app         (MsmStartupInfo *info,
                                            int             app_index);
static void     fallback_end_app           (MsmStartupInfo *info,
                                            int             app_index);


static MsmStartupModule fallback_module = {
  MSM_STARTUP_MODULE_VERSION,
  fallback_init,
  fallback_shutdown,
  fallback_get_shutdown_time,
  fallback_begin,
  fallback_end,
  fallback_begin_app,
  fallback_end_app
};

static MsmStartupModule *current_module = NULL;
static GModule *module_object = NULL;

typedef MsmStartupModule* (* GetVtableFunc) (void);

void
msm_startup_set_module (const char *name)
{
  char *path;
  char *filebase;  

  g_return_if_fail (current_module == NULL);

  if (name == NULL)
    {
      current_module = &fallback_module;
      return;
    }
  
  filebase = g_strdup_printf ("msm-startup-%s", name);
  path = g_module_build_path (MSM_MODULEDIR, filebase);
                              
  module_object = g_module_open (path, 0);
              
  if (module_object == NULL)
    {
      g_printerr (_("Failed to open startup progress indicator module '%s': %s\n"),
                  path, g_module_error ());
      
      /* debug fallback */
      g_free (path);
      path = g_module_build_path ("./.libs", filebase);
      module_object = g_module_open (path, 0);
      if (module_object == NULL)
        g_printerr (_("Failed to open startup progress indicator module '%s': %s\n"),
                    path, g_module_error ());
    }

  if (module_object != NULL)
    {
      GetVtableFunc func;

      func = NULL;
      if (!g_module_symbol (module_object,
                            "msm_startup_module_get_vtable",
                            (gpointer) &func))
        {
          g_printerr (_("No function 'msm_startup_module_get_vtable' in module %s"),
                      path);
        }

      if (func)
        current_module = (* func) ();

      if (current_module == NULL)
        {
          g_module_close (module_object);
          module_object = NULL;
        }
      else
        {
          if (current_module->module_interface_version != MSM_STARTUP_MODULE_VERSION)
            {
              g_printerr (_("Module '%s' is for interface version %d but msm was compiled for %d"),
                          path, current_module->module_interface_version,
                          MSM_STARTUP_MODULE_VERSION);
              g_module_close (module_object);
              module_object = NULL;
            }
          else
            {
              if (!(* current_module->init) ())
                {
                  g_printerr (_("Failed to init module '%s'"),
                             path);
                  g_module_close (module_object);
                  module_object = NULL;
                }
            }
        }
    }
  
  if (current_module == NULL)
    current_module = &fallback_module;
  
  g_free (filebase);
  g_free (path);
}

void
msm_startup_begin (MsmStartupInfo *info)
{
  msm_verbose ("Telling startup module to begin displaying progress\n");
  (* current_module->begin) (info);
}

static gboolean shutdown_timeout (gpointer data);

static void
handle_shutdown (void)
{
  int shutdown_time;
  
  shutdown_time = (* current_module->get_shutdown_time) ();

  if (shutdown_time == 0)
    {
      (* current_module->shutdown) ();
    }
  else
    g_timeout_add (shutdown_time, shutdown_timeout, NULL);
}

static gboolean
shutdown_timeout (gpointer data)
{
  handle_shutdown ();

  return FALSE;
}

void
msm_startup_end (MsmStartupInfo *info)
{
  msm_verbose ("Telling startup module to end displaying progress\n");
  (* current_module->end) (info);

  handle_shutdown ();
}

void
msm_startup_begin_app (MsmStartupInfo *info,
                       int             app_index)
{
  msm_verbose ("Telling startup module that app %d is beginning\n", app_index);
  (* current_module->begin_app) (info, app_index);
}

void
msm_startup_end_app (MsmStartupInfo *info,
                     int             app_index)
{ 
  msm_verbose ("Telling startup module that app %d is ending\n", app_index);
  (* current_module->end_app) (info, app_index);
}

#include <gconf/gconf-client.h>

#define MSM_SPLASH_WINDOW_BACKGROUND_GCONF_KEY \
           "/apps/gnome-session/options/splash_image"

static GtkWidget *window;

/*
 * Icon brightening code stolen from gnome-panel
 */
#define ICON_BRIGHTNESS_INCREMENT 60
GdkPixbuf *
fallback_generate_end_app_icon (GdkPixbuf *icon, gboolean failed)
{
    GdkPixbuf *end_app_icon = NULL;
    gboolean has_alpha = FALSE;
    gint i, j;
    gint width, height, srcrowstride, destrowstride;
    guchar *target_pixels;
    guchar *original_pixels;
    guchar *pixsrc;
    guchar *pixdest;
    int val;
    guchar r, g, b;

    if (!icon)
      return NULL;

    has_alpha = gdk_pixbuf_get_has_alpha (icon);
    width = gdk_pixbuf_get_width (icon);
    height = gdk_pixbuf_get_height (icon);
    srcrowstride = gdk_pixbuf_get_rowstride (icon);
    original_pixels = gdk_pixbuf_get_pixels (icon);

    end_app_icon = 
        gdk_pixbuf_new (gdk_pixbuf_get_colorspace (icon),
                        has_alpha,
                        gdk_pixbuf_get_bits_per_sample (icon),
                        width, height);

    destrowstride = gdk_pixbuf_get_rowstride (end_app_icon);
    target_pixels = gdk_pixbuf_get_pixels (end_app_icon);

    for (i = 0; i < height; i++)
      {
        pixdest = target_pixels + i * destrowstride;
        pixsrc = original_pixels + i * srcrowstride;
        for (j = 0; j < width; j++)
          {
            r = *(pixsrc++);
            g = *(pixsrc++);
            b = *(pixsrc++);
            val = r + ICON_BRIGHTNESS_INCREMENT;
            *(pixdest++) = CLAMP(val, 0, 255);
            val = g + (failed? 0 : ICON_BRIGHTNESS_INCREMENT);
            *(pixdest++) = CLAMP(val, 0, 255);
            val = b + (failed? 0 : ICON_BRIGHTNESS_INCREMENT);
            *(pixdest++) = CLAMP(val, 0, 255);
            if (has_alpha)
              *(pixdest++) = *(pixsrc++);
        }
    }

    return end_app_icon;
}

static gboolean
fallback_init (void)
{  
  return TRUE;
}

static void
fallback_shutdown (void)
{
  gtk_widget_destroy (window);
  window = NULL;
}

static int
fallback_get_shutdown_time (void)
{
  return 0;
}

static void
fallback_begin (MsmStartupInfo *info)
{
  GdkPixbuf *background = NULL;
  GConfClient *client;
  char *filename, *status;
  GError *error = NULL;

  client = gconf_client_get_default ();

  filename = gconf_client_get_string (client,
                                      MSM_SPLASH_WINDOW_BACKGROUND_GCONF_KEY,
                                      &error);
  g_object_unref (G_OBJECT (client));

  if (error)
    {
      msm_warning ("Could not load background key information: %s\n",
                   error->message);

      g_error_free (error);
      error = NULL;
    }

  if (!filename)
    {
      filename = g_strdup (GNOME_ICONDIR "/splash/gnome-splash.png");
      msm_verbose ("Attempting to loading default background '%s'\n",
                   filename);
    }

  if (filename)
    {
      background = gdk_pixbuf_new_from_file (filename, NULL);
      g_free (filename);
    }

  if (!background)
    {
      msm_warning ("No suitable background could be found.\n");
      background = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
                                   FALSE, 8, 480, 240);
      gdk_pixbuf_fill (background, 0xffffffff);
    }

  window = msm_splash_window_new (background);

  status = g_strdup_printf (_("Starting session \"%s,\" please wait"),
                            info->translated_session_name);
  msm_splash_window_set_status (MSM_SPLASH_WINDOW (window), status);
  g_free (status);
  gtk_widget_show (window);
}

static void
fallback_end (MsmStartupInfo *info)
{
  msm_splash_window_set_status (MSM_SPLASH_WINDOW (window), 
                                _("Finished starting applications"));
  gdk_window_raise (window->window);
}

static void
fallback_begin_app (MsmStartupInfo *info,
                    int             app_index)
{
  MsmStartupAppInfo *app = &info->apps[app_index];
  char *status = NULL;

  if (!GTK_WIDGET_VISIBLE (window)) 
    return;

  if (app->icon != NULL)
    msm_splash_window_add_icon (MSM_SPLASH_WINDOW (window), app->icon);
  else
    msm_warning ("Icon for '%s' is NULL\n", app->name ? app->name : "(null)");

  status = g_strdup_printf (_("Starting \"%s\""), app->name);

  msm_splash_window_set_status (MSM_SPLASH_WINDOW (window), status);
  g_free (status);

  gdk_window_raise (window->window);
}

static void
fallback_end_app (MsmStartupInfo *info,
                  int             app_index)
{
  MsmStartupAppInfo *app = &info->apps[app_index];
  int icon_position;
  GdkPixbuf *bright_icon = NULL;
  char *status = NULL;

  if (!GTK_WIDGET_VISIBLE (window)) 
    return;

  if (app->icon != NULL)
    {
      bright_icon = fallback_generate_end_app_icon (app->icon, 
                                                    !app->registered);

      icon_position = 
        msm_splash_window_get_icon_position (MSM_SPLASH_WINDOW (window), 
                                             app->icon);
      if (icon_position >= 0)
        msm_splash_window_replace_icon (MSM_SPLASH_WINDOW (window), 
                                        bright_icon, icon_position);
      else
        msm_warning ("Icon for '%s' is not already part of splash window\n",
                     app->name ? app->name : "(null)");
    }
  else
    msm_warning ("Icon for '%s' is NULL\n", app->name ? app->name : "(null)");


  if (app->started)
    status = g_strdup_printf (_("Successfully started \"%s\""), app->name);
  else
    status = g_strdup_printf (_("Unsuccessfully started \"%s\""), app->name);

  msm_splash_window_set_status (MSM_SPLASH_WINDOW (window), status);
  g_free (status);
  gdk_window_raise (window->window);
}
