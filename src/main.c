/* msm main() */

/* 
 * Copyright (C) 2001 Havoc Pennington
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include <popt.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

#include "server.h"
#include "util.h"
#include "startup.h"

#include "gdmremote.h"

static GMainLoop *main_loop = NULL;

static void
shutdown_cleanly_on_signal (int signo)
{
  msm_quit ();
}

void
msm_quit (void)
{
  if (main_loop && g_main_loop_is_running (main_loop))
    g_main_loop_quit (main_loop);

  exit (0);
}

int
main (int argc, char **argv)
{
  char *session_name = NULL;
  gboolean failsafe = FALSE;
  gboolean verbose = FALSE;
  struct sigaction act;
  sigset_t empty_mask;
  MsmServer *server;
  poptContext popt_context;

  struct poptOption msm_options[] = 
    {
      { "failsafe", 'f', POPT_ARG_NONE, &failsafe, 0,
        N_("Run in failsafe mode."), NULL },

      { "verbose", 'v', POPT_ARG_NONE, &verbose, 0,
        N_("Display detailed information about application progress."), NULL },

      { "session", 's', POPT_ARG_STRING, &session_name, 0,
        N_("Load specified session."), N_("session") }, 

      POPT_AUTOHELP 

      { NULL, 0, 0, NULL, 0 }
    };

  sigemptyset (&empty_mask);
  act.sa_handler = SIG_IGN;
  act.sa_mask    = empty_mask;
  act.sa_flags   = 0;
  sigaction (SIGPIPE, &act, 0);

  /* FIXME I'm really not sure why I added this code */
  act.sa_handler = shutdown_cleanly_on_signal;
  sigaction (SIGHUP, &act, 0);
  sigaction (SIGINT, &act, 0);
  
  /* connect to display */
  gtk_init (&argc, &argv);

  popt_context = poptGetContext (NULL, argc, (const char **) argv, 
                                 msm_options, 0);
  while (poptGetNextOpt (popt_context) > 0);
  poptFreeContext (popt_context);

  if (verbose)
    msm_set_verbose (TRUE);

  msm_verbose ("Server starting, pid = %d\n",
               (int) getpid ());

  /* FIXME need to read this from a config option */
  msm_startup_set_module (NULL);
  
  if (failsafe)
    server = msm_server_new_failsafe ();
  else
    server = msm_server_new (session_name);

  msm_server_launch_session (server);
  
  main_loop = g_main_loop_new (NULL, FALSE);

  g_main_loop_run (main_loop);
  
  return 0;
}
