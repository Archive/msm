/* msm logout dialog code */

/* 
 * Copyright (C) 2001 Havoc Pennington
 * Some logout GUI code (C) 2000 Red Hat, Inc.,
 *  written by Owen Taylor <otaylor@redhat.com>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "logout.h"
#include "util.h"
#include <gtk/gtk.h>

#include <errno.h>
#include <unistd.h>

#include <config.h>

/* Logout fade effect thingy - this is disabled,
 * at least for now, for several reasons:
 *  - maybe it's good to have the logout dialog be a managed
 *    window and let the user interact with other apps
 *  - I'm not sure msm will avoid popping up other dialogs
 *    in all cases while we have the server grabbed
 *    (and other dialogs wouldn't be override-redirect)
 */

#if 0

#include <gdk/gdkx.h>

static GdkRectangle iris_rect;
static gint iris_block = 0;
static GdkGC *iris_gc = NULL;
static GMainLoop *iris_loop = NULL;

static gint
iris_callback (gpointer data)
{
  gint i;
  gint width_step;
  gint height_step;

  width_step = MIN (iris_rect.width / 2, iris_block);
  height_step = MIN (iris_rect.height / 2, iris_block);


  gdk_draw_rectangle (GDK_ROOT_PARENT (), iris_gc, FALSE,
                      iris_rect.x + i, iris_rect.y + i,  
                      (gint)iris_rect.width - 2 * i,
                      (gint)iris_rect.height - 2 * i);    

  gdk_flush ();

  iris_rect.x += width_step;
  iris_rect.y += height_step;
  iris_rect.width -= MIN (iris_rect.width, iris_block * 2);
  iris_rect.height -= MIN (iris_rect.height, iris_block * 2);

  if (iris_rect.width == 0 || iris_rect.height == 0)
    {
      g_main_loop_quit (iris_loop);
      return FALSE;
    }
  else
    return TRUE;
}

static void
iris (void)
{
  iris_rect.x = 0;
  iris_rect.y = 0;
  iris_rect.width = gdk_screen_width ();
  iris_rect.height = gdk_screen_height ();

  if (!iris_gc)
    {
      GdkGCValues values;
      static gchar dash_list[2] = {1, 1};

      values.line_style = GDK_LINE_ON_OFF_DASH;
      values.subwindow_mode = GDK_INCLUDE_INFERIORS;

      iris_gc = gdk_gc_new_with_values (GDK_ROOT_PARENT (),
					&values,
                                        GDK_GC_LINE_STYLE | GDK_GC_SUBWINDOW);

      gdk_gc_set_dashes (iris_gc, 0, dash_list, 2);
    }

  /* Plan for a time of 0.5 seconds for effect */
  iris_block = iris_rect.height / (500 / 20);
  if (iris_block < 8)
    {
      iris_block = 8;
    }

  g_timeout_add (20, iris_callback, NULL);

  iris_loop = g_main_loop_new (NULL, FALSE);

  g_main_loop_run (iris_loop);

  g_main_loop_unref (iris_loop);
  iris_loop = NULL;
}

static void
refresh_screen (void)
{
  GdkWindowAttr attributes;
  GdkWindow *window;

  attributes.x = 0;
  attributes.y = 0;
  attributes.width = gdk_screen_width ();
  attributes.height = gdk_screen_height ();
  attributes.window_type = GDK_WINDOW_TOPLEVEL;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.override_redirect = TRUE;
  attributes.event_mask = 0;
  
  window = gdk_window_new (NULL, &attributes,
			   GDK_WA_X | GDK_WA_Y | GDK_WA_NOREDIR);

  gdk_window_show (window);
  gdk_flush ();
  gdk_window_hide (window);
}

static gboolean
display_gui (void)
{
  GtkWidget *box;
  GtkWidget *toggle_button = NULL;
  gint result;
  gchar *s, *t;
  GtkWidget *halt = NULL;
  GtkWidget *reboot = NULL;
  GtkWidget *invisible;

  /* It's really bad here if someone else has the pointer
   * grabbed, so we first grab the pointer and keyboard
   * to an offscreen window, and then once we have the
   * server grabbed, move that to our dialog.
   */
  gtk_rc_reparse_all ();

  invisible = gtk_invisible_new ();
  gtk_widget_show (invisible);

  while (1)
    {
      if (gdk_pointer_grab (invisible->window, FALSE, 0,
			    NULL, NULL, GDK_CURRENT_TIME) == Success)
	{
	  if (gdk_keyboard_grab (invisible->window, FALSE, GDK_CURRENT_TIME)
	      == Success)
	    break;
	  gdk_pointer_ungrab (GDK_CURRENT_TIME);
	}
      sleep (1);
    }

  XGrabServer (GDK_DISPLAY ());
  iris ();

  box = gnome_message_box_new (_("Really log out?"),
			       GNOME_MESSAGE_BOX_QUESTION,
			       GNOME_STOCK_BUTTON_YES,
			       GNOME_STOCK_BUTTON_NO,
			       GNOME_STOCK_BUTTON_HELP,
			       NULL);

  gtk_object_set (GTK_OBJECT (box),
		  "type", GTK_WINDOW_POPUP,
		  NULL);

  gnome_dialog_set_default (GNOME_DIALOG (box), 0);
  gtk_window_set_position (GTK_WINDOW (box), GTK_WIN_POS_CENTER);
  gtk_window_set_policy (GTK_WINDOW (box), FALSE, FALSE, TRUE);

  gnome_dialog_close_hides (GNOME_DIALOG (box), TRUE);

  gtk_container_set_border_width (GTK_CONTAINER (GNOME_DIALOG (box)->vbox),
				  GNOME_PAD);
  if (!autosave)
    {
      toggle_button = gtk_check_button_new_with_label (_("Save current setup"));
      gtk_widget_show (toggle_button);
      gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (box)->vbox),
			  toggle_button,
			  FALSE, TRUE, 0);
    }

  /* Red Hat specific code to check if the user has a
   * good chance of being able to shutdown the system,
   * and if so, give them that option
   */
  s = g_strconcat ("/var/lock/console/", g_get_user_name (), NULL);
  t = g_strconcat ("/var/run/console/", g_get_user_name (), NULL);
  if (((geteuid () == 0) || g_file_exists (t) || g_file_exists(s)) &&
      access (halt_command[0], X_OK) == 0)
    {
      GtkWidget *frame;
      GtkWidget *action_vbox;
      GtkWidget *r;

      frame = gtk_frame_new (_("Action"));
      gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (box)->vbox),
			  frame,
			  FALSE, TRUE, GNOME_PAD_SMALL);

      action_vbox = gtk_vbox_new (FALSE, 0);
      gtk_container_add (GTK_CONTAINER (frame), action_vbox);

      r = gtk_radio_button_new_with_label (NULL, _("Logout"));
      gtk_box_pack_start (GTK_BOX (action_vbox), r, FALSE, FALSE, 0);

      r = halt = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (r), _("Shut Down"));
      gtk_box_pack_start (GTK_BOX (action_vbox), r, FALSE, FALSE, 0);

      r = reboot = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (r), _("Reboot"));
      gtk_box_pack_start (GTK_BOX (action_vbox), r, FALSE, FALSE, 0);
    }
  g_free (s);
  g_free (t);

  gtk_widget_show_all (box);

  /* Move the grabs to our message box */
  gdk_pointer_grab (box->window, TRUE, 0,
		    NULL, NULL, GDK_CURRENT_TIME);
  gdk_keyboard_grab (invisible->window, FALSE, GDK_CURRENT_TIME);

  /* Hmm, I suppose that it's a bug, or strange behaviour on gnome-dialog's
   * part that even though the Yes is the default, Help somehow gets focus
   * and thus temporairly grabs default as well.  I dunno if this should be
   * changed in gnome-dialog, as grabbing the focus in _set_default would
   * also be wrong.  Hmm, there should be a _set_focus as well I suppose,
   * anyway, this will work */
  if (GTK_WINDOW (box)->default_widget != NULL)
	  gtk_widget_grab_focus (GTK_WINDOW (box)->default_widget);

  result = gnome_dialog_run (GNOME_DIALOG (box));

  refresh_screen ();
  XUngrabServer (GDK_DISPLAY ());

  gdk_pointer_ungrab (GDK_CURRENT_TIME);
  gdk_keyboard_ungrab (GDK_CURRENT_TIME);

  gdk_flush ();

  switch (result)
    {
    case 0:
	/* We want to know if we should trash changes (and lose forever)
	 * or save them */
	if(!autosave)
		save_selected = GTK_TOGGLE_BUTTON (toggle_button)->active;
     if (halt)
	{
	  if (GTK_TOGGLE_BUTTON (halt)->active)
	    action = HALT;
	  else if (GTK_TOGGLE_BUTTON (reboot)->active)
	    action = REBOOT;
	}
      return TRUE;
    default:
    case 1:
      return FALSE;
    case 2:
/* FIXME: port to gnome 2 help stuff */
#if 0
      {
	GnomeHelpMenuEntry help_entry = {
	  "panel", "panelbasics.html#LOGGINGOUT"
	};
	gnome_help_display (NULL, &help_entry);
      }
#endif
      return FALSE;
    }
}

#endif /* commented-out logout code from gsm */

#include "inlinepixbufs.h"

static void
add_stock_icon (GtkIconFactory *factory,
                const char     *name,
                const guint8   *data)
{

  GdkPixbuf *pixbuf;
  GtkIconSet *icon_set;
  
  pixbuf = gdk_pixbuf_new_from_inline (-1, data, FALSE,
                                       NULL);

  g_assert (pixbuf);
  icon_set = gtk_icon_set_new_from_pixbuf (pixbuf);
  gtk_icon_factory_add (factory, name, icon_set);
  gtk_icon_set_unref (icon_set);
  g_object_unref (G_OBJECT (pixbuf));
}

static void
register_stock_icons (void)
{
  GtkIconFactory *factory;
  GtkIconSet *icon_set;
  
  static GtkStockItem items[] = {
    { "msm-logout",
      N_("_Log out"),
      0, 0, NULL },
    { "msm-shut-down",
      N_("_Shut down"),
      0, 0, NULL },
    { "msm-reboot",
      N_("_Reboot"),
      0, 0, NULL },
    { "msm-cancel-logout",
      N_("_Cancel"),
      0, 0, NULL }
  };

  /* Register our stock items */
  gtk_stock_add (items, G_N_ELEMENTS (items));
  
  /* Add our custom icon factory to the list of defaults */
  factory = gtk_icon_factory_new ();
  gtk_icon_factory_add_default (factory);
  
  add_stock_icon (factory, "msm-logout", logout_icon_data);
  add_stock_icon (factory, "msm-shut-down", shutdown_icon_data);
  add_stock_icon (factory, "msm-reboot", reboot_icon_data);

  /* Link to the standard cancel icon set for the cancel button */
  icon_set = gtk_icon_factory_lookup_default (GTK_STOCK_CANCEL);
  g_assert (icon_set);
  gtk_icon_factory_add (factory, "msm-cancel-logout", icon_set);
  
  /* Drop our reference to the factory, GTK will hold a reference. */
  g_object_unref (G_OBJECT (factory));
}

static void
destroy_toplevel (GtkWidget *widget)
{
  GtkWidget *top = gtk_widget_get_toplevel (widget);

  gtk_widget_destroy (top);
}

static void
logout_clicked (GtkWidget *button,
                gpointer   data)
{
  MsmLogoutAction *action = data;

  *action = MSM_LOGOUT_NOTHING;

  destroy_toplevel (button);
}

static void
shutdown_clicked (GtkWidget *button,
                  gpointer   data)
{
  MsmLogoutAction *action = data;
    
  *action = MSM_LOGOUT_SHUT_DOWN;

  destroy_toplevel (button);
}

static void
reboot_clicked (GtkWidget *button,
                gpointer   data)
{
  MsmLogoutAction *action = data;
    
  *action = MSM_LOGOUT_REBOOT;

  destroy_toplevel (button);
}

static void
cancel_clicked (GtkWidget *button,
                gpointer   data)
{
  MsmLogoutAction *action = data;
    
  *action = MSM_LOGOUT_CANCEL;

  destroy_toplevel (button);
}

static void
save_session_toggled (GtkWidget *toggle,
                      gpointer data)
{
  gboolean *save_session = data;

  *save_session = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));
}

static void
dialog_destroyed (GtkWidget *dialog,
                  gpointer   data)
{
  GMainLoop *loop = data;

  g_main_loop_quit (loop);
}

static gboolean
show_shutdown_buttons (void)
{
  char *s;
  char *t;
  gboolean show_shutdown;
  
  s = g_strconcat ("/var/lock/console/", g_get_user_name (), NULL);
  t = g_strconcat ("/var/run/console/", g_get_user_name (), NULL);

  show_shutdown = FALSE;
  
  if (geteuid () == 0)
    {
      msm_verbose ("Assuming root can shut down\n");
      show_shutdown = TRUE;
    }
  else if (g_file_test (t, G_FILE_TEST_IS_REGULAR))
    {
      msm_verbose ("Assuming we can shut down since '%s' exists\n", t);
      show_shutdown = TRUE;
    }
  else if (g_file_test (s, G_FILE_TEST_IS_REGULAR))
    {
      msm_verbose ("Assuming we can shut down since '%s' exists\n", s);
      show_shutdown = TRUE;
    }

  if (show_shutdown && access ("/usr/bin/poweroff", X_OK) != 0)
    {
      msm_verbose ("But we can't execute /usr/bin/poweroff, so we can't shut down (%s)\n",
                   g_strerror (errno));
      show_shutdown = FALSE;
    }

  g_free (s);
  g_free (t);

  return show_shutdown;
}

void
msm_run_logout_dialog (GtkWidget         **dialog_location,
                       /* these are inout parameters indicating the default */
                       MsmLogoutAction     *action,
                       gboolean            *save_session)
{
  GtkWidget *dialog;
  GtkWidget *bbox;
  GtkWidget *table;
  GtkWidget *button;
  static gboolean stock_icons_registered = FALSE;
  GMainLoop *loop;
  
  if (!stock_icons_registered)
    {
      register_stock_icons ();
      stock_icons_registered = TRUE;
    }
  
  if (*dialog_location)
    {
      gtk_window_present (GTK_WINDOW (*dialog_location));
      *action = MSM_LOGOUT_UNCHANGED;
      return;
    }

  dialog = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  *dialog_location = dialog;
  g_object_add_weak_pointer (G_OBJECT (*dialog_location),
                             (gpointer*) dialog_location);

  gtk_window_set_type_hint (GTK_WINDOW (dialog), GDK_WINDOW_TYPE_HINT_DIALOG);
  gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_CENTER);
  gtk_window_set_title (GTK_WINDOW (dialog), _("Log out"));
  gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
  gtk_window_stick (GTK_WINDOW (dialog));
  
  /* Make ourselves a little grid to lock things to */
  table = gtk_table_new (20, 20, TRUE);
  gtk_container_add (GTK_CONTAINER (dialog), table);
  
  bbox = gtk_vbutton_box_new ();

  gtk_table_attach (GTK_TABLE (table),
                    bbox,
                    1, 19,                 1, 15,
                    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
                    0,                     0);
  
  button = gtk_button_new_from_stock ("msm-logout");
  gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, FALSE, 0);
  g_signal_connect (G_OBJECT (button), "clicked",
                    G_CALLBACK (logout_clicked), action);

  if (show_shutdown_buttons ())
    {
      button = gtk_button_new_from_stock ("msm-shut-down");
      gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, FALSE, 0);
      g_signal_connect (G_OBJECT (button), "clicked",
                        G_CALLBACK (shutdown_clicked), action);

      button = gtk_button_new_from_stock ("msm-reboot");
      gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, FALSE, 0);
      g_signal_connect (G_OBJECT (button), "clicked",
                        G_CALLBACK (reboot_clicked), action);
    }
  
  button = gtk_button_new_from_stock ("msm-cancel-logout");
  gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, FALSE, 0);
  g_signal_connect (G_OBJECT (button), "clicked",
                    G_CALLBACK (cancel_clicked), action);

  button = gtk_check_button_new_with_mnemonic (_("Sa_ve open applications to restore next time"));
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button),
                                *save_session);
  g_signal_connect (G_OBJECT (button), "toggled",
                    G_CALLBACK (save_session_toggled), save_session);

  gtk_table_attach (GTK_TABLE (table),
                    button,
                    1, 19,                 17, 19,
                    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
                    0,                     0);

  loop = g_main_loop_new (NULL, FALSE);
  g_signal_connect (G_OBJECT (dialog), "destroy",
                    G_CALLBACK (dialog_destroyed), loop);
  
  gtk_widget_show_all (dialog);

  g_main_loop_run (loop);

  /* dialog is destroyed */
  g_assert (*dialog_location == NULL);

  g_main_loop_unref (loop);
}

