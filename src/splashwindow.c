/* splashwindow.c - splash screen rendering

   Copyright (C) 1999-2002 Jacob Berkman, Ximian Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.  
*/
#include "config.h"
#include "util.h"
#include "splashwindow.h"
#include <stdio.h>
#include <string.h>

/* width / height if we have no image */
#define MSM_SPLASH_WINDOW_DEFAULT_WIDTH 480
#define MSM_SPLASH_WINDOW_DEFAULT_HEIGHT 220

/* offset from bottom of label & font */
#define MSM_SPLASH_WINDOW_LABEL_V_OFFSET 3
#define MSM_SPLASH_WINDOW_DEFAULT_STATUS_FONT_SIZE 8

/* icon border, spacing, offset from bottom and initial size */
#define MSM_SPLASH_WINDOW_DEFAULT_ICON_BORDER_WIDTH  8
#define MSM_SPLASH_WINDOW_DEFAULT_ICON_SPACING 4
#define MSM_SPLASH_WINDOW_ICON_V_OFFSET 14
#define MSM_SPLASH_WINDOW_DEFAULT_ICON_SIZE 24
#define MSM_SPLASH_WINDOW_DEFAULT_ICON_ROWS 1

#define MSM_SPLASH_WINDOW_ICON_BOUNDS "_msm_splash_window_icon_bounds"
#define MSM_SPLASH_WINDOW_SCALED_ICON "_msm_splash_window_scaled_icon"

enum
{
  PROP_0,
  PROP_ICON_SPACING,
  PROP_ICON_BORDER_WIDTH,
  PROP_ICON_SIZE,
  PROP_STATUS_FONT_SIZE,
  PROP_BACKGROUND
};

static void msm_splash_window_class_init (MsmSplashWindowClass * klass);
static void msm_splash_window_init (MsmSplashWindow * object);
static void msm_splash_window_finalize (GObject * object);
static void msm_splash_window_set_property (GObject * object,
                                            guint prop_id,
                                            const GValue * value,
                                            GParamSpec * pspec);
static void msm_splash_window_get_property (GObject * object,
                                            guint prop_id,
                                            GValue * value,
                                            GParamSpec * pspec);
static void msm_splash_window_realize (GtkWidget * widget);
static void msm_splash_window_unrealize (GtkWidget * widget);
static gboolean msm_splash_window_expose_event (GtkWidget * widget,
                                                GdkEventExpose * event);
static void msm_splash_window_size_request (GtkWidget * widget,
                                            GtkRequisition * requisition);
static void msm_splash_window_size_allocate (GtkWidget * widget,
                                             GtkAllocation * allocation);
static gboolean msm_splash_window_button_release_event (GtkWidget * widget,
                                                        GdkEventButton *
                                                        event);
static gboolean msm_splash_window_calc_n_icon_rows (MsmSplashWindow * window);
static void msm_splash_window_calculate_status_bounds (MsmSplashWindow *
                                                       window);
static void msm_splash_window_set_icon_spacing (MsmSplashWindow * window,
                                                int icon_spacing);
static int msm_splash_window_get_icon_spacing (MsmSplashWindow * window);
static void msm_splash_window_set_icon_border_width (MsmSplashWindow * window,
                                                     int icon_border_width);
static int msm_splash_window_get_icon_border_width (MsmSplashWindow * window);
static void msm_splash_window_set_icon_size (MsmSplashWindow * window,
                                             int size);
static int msm_splash_window_get_icon_size (MsmSplashWindow * window);
static void msm_splash_window_set_status_font_size (MsmSplashWindow * window,
                                                    int size);
static int msm_splash_window_get_status_font_size (MsmSplashWindow * window);
static void msm_splash_window_set_background (MsmSplashWindow *window, 
                                              GdkPixbuf *background);
static void msm_splash_window_scale_all_icons (MsmSplashWindow * window);
static void msm_splash_window_scale_icon (MsmSplashWindow * window,
                                          GdkPixbuf * icon);
static void msm_splash_window_destroy_icon (GdkPixbuf * icon);


static void msm_splash_window_layout_icon (MsmSplashWindow * window,
                                           GdkPixbuf * icon,
                                           GdkRectangle * area);
static void msm_splash_window_layout_all_icons (MsmSplashWindow * window);

struct _MsmSplashWindowPrivate
{
  GdkPixbuf *background;
  GdkPixmap *bg_pixmap;

  PangoLayout *layout;

  GList *icons;
  int icon_size;
  int icon_spacing;
  int icon_border_width;
  int icon_n_rows;

  int current_column;
  int current_row;

  char *status;
  int status_font_size;
  PangoAttribute *status_font_size_attr;

  GdkRectangle background_bounds;
  GdkRectangle status_bounds;
};

#define MSM_SPLASH_WINDOW_GET_PRIVATE(o) \
           (G_TYPE_INSTANCE_GET_PRIVATE ((o), MSM_TYPE_SPLASH_WINDOW, \
                                         MsmSplashWindowPrivate))

static GtkWindowClass *parent_class = NULL;

GType
msm_splash_window_get_type (void)
{
  static GType splash_window_type = 0;

  if (splash_window_type == 0)
    {
      static const GTypeInfo splash_window_info = {
        sizeof (MsmSplashWindowClass),
        NULL,                   /* base_init */
        NULL,                   /* base_finalize */
        (GClassInitFunc) msm_splash_window_class_init,
        NULL,                   /* class_finalize */
        NULL,                   /* class_data */
        sizeof (MsmSplashWindow),
        0,                      /* n_preallocs */
        (GInstanceInitFunc) msm_splash_window_init
      };
      splash_window_type = g_type_register_static (GTK_TYPE_WINDOW,
                                                   "MsmSplashWindow",
                                                   &splash_window_info, 0);
    }

  return splash_window_type;
}

static void
msm_splash_window_class_init (MsmSplashWindowClass * klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;
  GtkWidgetClass *widget_class = (GtkWidgetClass *) klass;

  parent_class = g_type_class_peek_parent (klass);

  gobject_class->set_property = msm_splash_window_set_property;
  gobject_class->get_property = msm_splash_window_get_property;
  gobject_class->finalize = msm_splash_window_finalize;

  widget_class->realize = msm_splash_window_realize;
  widget_class->unrealize = msm_splash_window_unrealize;
  widget_class->expose_event = msm_splash_window_expose_event;
  widget_class->size_request = msm_splash_window_size_request;
  widget_class->size_allocate = msm_splash_window_size_allocate;
  widget_class->button_release_event = msm_splash_window_button_release_event;

  g_type_class_add_private (klass, sizeof (MsmSplashWindowPrivate));

  g_object_class_install_property (gobject_class, PROP_ICON_SPACING,
                                   g_param_spec_int ("icon-spacing",
                                                     _("Icon Spacing"),
                                                     _
                                                     ("Space between splash screen icons."),
                                                     0, G_MAXINT,
                                                     MSM_SPLASH_WINDOW_DEFAULT_ICON_SPACING,
                                                     G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

  g_object_class_install_property (gobject_class, PROP_ICON_BORDER_WIDTH,
                                   g_param_spec_int ("icon-border-width",
                                                     _("Icon Border Width"),
                                                     _
                                                     ("Space around splash screen icons."),
                                                     0, G_MAXINT,
                                                     MSM_SPLASH_WINDOW_DEFAULT_ICON_BORDER_WIDTH,
                                                     G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

  g_object_class_install_property (gobject_class, PROP_ICON_SIZE,
                                   g_param_spec_int ("icon-size",
                                                     _("Icon Size"),
                                                     _
                                                     ("Size of splash screen icons."),
                                                     0, G_MAXINT,
                                                     MSM_SPLASH_WINDOW_DEFAULT_ICON_SIZE,
                                                     G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

  g_object_class_install_property (gobject_class, PROP_STATUS_FONT_SIZE,
                                   g_param_spec_int ("status-font-size",
                                                     _("Status Font Size"),
                                                     _
                                                     ("Size of splash screen status text."),
                                                     0, G_MAXINT,
                                                     MSM_SPLASH_WINDOW_DEFAULT_STATUS_FONT_SIZE,
                                                     G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
  g_object_class_install_property (gobject_class, PROP_BACKGROUND,
                                   g_param_spec_object ("background",
                                                     _("Background"),
                                                     _
                                                     ("Splash screen background image."),
                                                     GDK_TYPE_PIXBUF,
                                                     G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));

}

static void
msm_splash_window_init (MsmSplashWindow * window)
{
  MsmSplashWindowPrivate *priv = MSM_SPLASH_WINDOW_GET_PRIVATE (window);

  window->priv = priv;

  priv->current_row = MSM_SPLASH_WINDOW_DEFAULT_ICON_ROWS;
  priv->icons = NULL;
  priv->status = NULL;
  priv->icon_n_rows = 1;
  priv->background = NULL;
  priv->bg_pixmap = NULL;

  gtk_widget_add_events (GTK_WIDGET (window), GDK_BUTTON_RELEASE_MASK);

  priv->layout = gtk_widget_create_pango_layout (GTK_WIDGET (window), "");
  pango_layout_set_alignment (priv->layout, PANGO_ALIGN_CENTER);

  /* Connect after so that there is an opportunity for the code that is using
   * the splash screen to pick an icon designed for the new icon size rather
   * than resorting to scaling the icon that was designed for the old icon size.
   */
  g_signal_connect_after (G_OBJECT (window), "notify::icon-size",
                          G_CALLBACK (msm_splash_window_scale_all_icons), 
                          NULL);
}

static void
msm_splash_window_finalize (GObject * object)
{
  MsmSplashWindow *window = (MsmSplashWindow *) object;
  MsmSplashWindowPrivate *priv;

  priv = window->priv;

  if (priv->icons)
    {
      g_list_foreach (priv->icons, (GFunc) msm_splash_window_destroy_icon, NULL);
      g_list_free (priv->icons);
      priv->icons = NULL;
    }

  if (priv->layout)
    g_object_unref (priv->layout);

  if (priv->background)
    g_object_unref (priv->background);

  if (priv->bg_pixmap)
    g_object_unref (priv->bg_pixmap);

  if (G_OBJECT_CLASS (parent_class)->finalize)
    (*G_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void
msm_splash_window_set_property (GObject * object,
                                guint prop_id,
                                const GValue * value, GParamSpec * pspec)
{
  MsmSplashWindow *window = MSM_SPLASH_WINDOW (object);

  switch (prop_id)
    {
    case PROP_ICON_SPACING:
      msm_splash_window_set_icon_spacing (window, g_value_get_int (value));
      break;
    case PROP_ICON_BORDER_WIDTH:
      msm_splash_window_set_icon_border_width (window,
                                               g_value_get_int (value));
      break;
    case PROP_ICON_SIZE:
      msm_splash_window_set_icon_size (window, g_value_get_int (value));
      break;
    case PROP_STATUS_FONT_SIZE:
      msm_splash_window_set_status_font_size (window,
                                              g_value_get_int (value));
      break;
    case PROP_BACKGROUND:
      msm_splash_window_set_background (window,
                                      (GdkPixbuf *) g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
msm_splash_window_get_property (GObject * object,
                                guint prop_id,
                                GValue * value, GParamSpec * pspec)
{
  MsmSplashWindow *window = MSM_SPLASH_WINDOW (object);

  switch (prop_id)
    {
    case PROP_ICON_SPACING:
      g_value_set_int (value, msm_splash_window_get_icon_spacing (window));
      break;
    case PROP_ICON_BORDER_WIDTH:
      g_value_set_int (value,
                       msm_splash_window_get_icon_border_width (window));
      break;
    case PROP_ICON_SIZE:
      g_value_set_int (value, msm_splash_window_get_icon_size (window));
      break;
    case PROP_STATUS_FONT_SIZE:
      g_value_set_int (value,
                       msm_splash_window_get_status_font_size (window));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
msm_splash_window_set_icon_spacing (MsmSplashWindow * window,
                                    int icon_spacing)
{
  MsmSplashWindowPrivate *priv;

  g_return_if_fail (MSM_IS_SPLASH_WINDOW (window));
  g_return_if_fail (icon_spacing >= 0);

  priv = window->priv;

  if (priv->icon_spacing != icon_spacing)
    {
      priv->icon_spacing = icon_spacing;
      g_object_notify (G_OBJECT (window), "icon-spacing");
    }
}

static int
msm_splash_window_get_icon_spacing (MsmSplashWindow * window)
{
  MsmSplashWindowPrivate *priv;

  g_return_val_if_fail (MSM_IS_SPLASH_WINDOW (window), -1);

  priv = window->priv;

  return priv->icon_spacing;
}

static void
msm_splash_window_set_icon_border_width (MsmSplashWindow * window,
                                         int icon_border_width)
{
  MsmSplashWindowPrivate *priv;

  g_return_if_fail (MSM_IS_SPLASH_WINDOW (window));
  g_return_if_fail (icon_border_width >= 0);

  priv = window->priv;

  if (priv->icon_border_width != icon_border_width)
    {
      priv->icon_border_width = icon_border_width;
      g_object_notify (G_OBJECT (window), "icon-border-width");
    }
}

static int
msm_splash_window_get_icon_border_width (MsmSplashWindow * window)
{
  MsmSplashWindowPrivate *priv;

  g_return_val_if_fail (MSM_IS_SPLASH_WINDOW (window), -1);

  priv = window->priv;

  return priv->icon_border_width;
}

static void
msm_splash_window_set_icon_size (MsmSplashWindow * window, int size)
{
  MsmSplashWindowPrivate *priv;

  g_return_if_fail (MSM_IS_SPLASH_WINDOW (window));
  g_return_if_fail (size >= 0);

  priv = window->priv;

  if (priv->icon_size != size)
    {
      priv->icon_size = size;
      g_object_notify (G_OBJECT (window), "icon-size");
    }
}

static int
msm_splash_window_get_icon_size (MsmSplashWindow * window)
{
  MsmSplashWindowPrivate *priv;

  g_return_val_if_fail (MSM_IS_SPLASH_WINDOW (window), -1);

  priv = window->priv;

  return priv->icon_size;
}

static void
msm_splash_window_set_status_font_size (MsmSplashWindow * window, int size)
{
  MsmSplashWindowPrivate *priv;
  PangoAttrList *attrs;

  g_return_if_fail (MSM_IS_SPLASH_WINDOW (window));
  g_return_if_fail (size >= 0);

  priv = window->priv;

  if (priv->status_font_size != size)
    {
      attrs = pango_attr_list_new ();
      priv->status_font_size_attr = pango_attr_size_new (PANGO_SCALE * size);
      priv->status_font_size_attr->start_index = 0;
      priv->status_font_size_attr->end_index = 0;
      pango_attr_list_insert (attrs, priv->status_font_size_attr);
      pango_layout_set_attributes (priv->layout, attrs);
      pango_attr_list_unref (attrs);

      priv->status_font_size = size;

      g_object_notify (G_OBJECT (window), "status-font-size");
    }
}

static int
msm_splash_window_get_status_font_size (MsmSplashWindow * window)
{
  MsmSplashWindowPrivate *priv;

  g_return_val_if_fail (MSM_IS_SPLASH_WINDOW (window), -1);

  priv = window->priv;

  return priv->status_font_size;
}

static void
msm_splash_window_set_background (MsmSplashWindow *window, 
                                  GdkPixbuf       *background)
{
  MsmSplashWindowPrivate *priv;

  g_return_if_fail (MSM_IS_SPLASH_WINDOW (window));
  g_return_if_fail (GDK_IS_PIXBUF (background));

  priv = window->priv;

  if (priv->background)
    g_object_unref (priv->background);

  priv->background = g_object_ref (background);
}

static void
msm_splash_window_realize (GtkWidget * widget)
{
  GdkPixmap *pixmap = NULL;
  MsmSplashWindow *window = MSM_SPLASH_WINDOW (widget);
  MsmSplashWindowPrivate *priv;

  priv = window->priv;

  if (GTK_WIDGET_CLASS (parent_class)->realize)
    (*GTK_WIDGET_CLASS (parent_class)->realize) (widget);

  if (widget->window && priv->background)
    {
      int width, height;

      width = gdk_pixbuf_get_width (priv->background);
      height = gdk_pixbuf_get_height (priv->background);

      pixmap = gdk_pixmap_new (widget->window, width, height,
                               gdk_drawable_get_visual (widget->window)->
                               depth);

      if (pixmap)
        {
          gdk_pixbuf_render_to_drawable (priv->background,
                                         GDK_DRAWABLE (pixmap),
                                         widget->style->black_gc,
                                         0, 0, 0, 0, width, height,
                                         GDK_RGB_DITHER_NORMAL, 0, 0);

          gdk_window_set_back_pixmap (widget->window, pixmap, FALSE);

          if (priv->bg_pixmap)
            g_object_unref (priv->bg_pixmap);

          priv->bg_pixmap = pixmap;
        }
    }
}

static void
msm_splash_window_unrealize (GtkWidget * widget)
{
  MsmSplashWindow *window = (MsmSplashWindow *) widget;
  MsmSplashWindowPrivate *priv;

  priv = window->priv;

  if (priv->bg_pixmap)
    {
      g_object_unref (priv->bg_pixmap);
      priv->bg_pixmap = NULL;
    }

  if (GTK_WIDGET_CLASS (parent_class)->unrealize)
    (*GTK_WIDGET_CLASS (parent_class)->unrealize) (widget);
}

static gboolean
msm_splash_window_expose_event (GtkWidget * widget, GdkEventExpose * event)
{
  GList *icon = NULL;
  GdkRectangle exposed;
  MsmSplashWindow *window = MSM_SPLASH_WINDOW (widget);
  MsmSplashWindowPrivate *priv;

  if (!GTK_WIDGET_DRAWABLE (widget))
    return FALSE;

  priv = window->priv;

  if (gdk_rectangle_intersect (&event->area, 
                               &priv->background_bounds, 
                               &exposed))
    gdk_draw_drawable (GDK_DRAWABLE (widget->window), widget->style->black_gc,
                       GDK_DRAWABLE (priv->bg_pixmap), exposed.x, exposed.y,
                       exposed.x, exposed.y, exposed.width, exposed.height);

  icon = priv->icons;
  while (icon)
    {
      GdkRectangle *icon_bounds;
      GdkPixbuf *scaled_icon = NULL;

      icon_bounds = g_object_get_data (G_OBJECT (icon->data),
                                       MSM_SPLASH_WINDOW_ICON_BOUNDS);

      if (!icon_bounds)
        {
          g_warning ("Splash screen icon lacks positional information.");
          icon = icon->next;
          continue;
        }

      if (gdk_rectangle_intersect (&event->area, icon_bounds, &exposed))
        {
          scaled_icon = g_object_get_data (G_OBJECT (icon->data),
                                           MSM_SPLASH_WINDOW_SCALED_ICON);

          gdk_draw_pixbuf (widget->window,
                           widget->style->black_gc,
                           scaled_icon,
                           exposed.x - icon_bounds->x,
                           exposed.y - icon_bounds->y,
                           exposed.x, exposed.y,
                           exposed.width, exposed.height,
                           GDK_RGB_DITHER_NORMAL, exposed.x, exposed.y);
        }

      icon = icon->next;
    }

  if (priv->layout)
    {

      if (gdk_rectangle_intersect (&event->area, &priv->status_bounds,
                                   &exposed))
        {
          /* drop shadow */
          gdk_draw_layout (widget->window, widget->style->black_gc,
                           priv->status_bounds.x + 1,
                           priv->status_bounds.y + 1, priv->layout);

          /* text */
          gdk_draw_layout (widget->window, widget->style->white_gc,
                           priv->status_bounds.x, priv->status_bounds.y,
                           priv->layout);
        }
    }

  return FALSE;
}

static void
msm_splash_window_size_request (GtkWidget * widget,
                                GtkRequisition * requisition)
{
  MsmSplashWindow *window = (MsmSplashWindow *) widget;
  MsmSplashWindowPrivate *priv;

  priv = window->priv;

  if (!priv->background)
    {
      requisition->width = MSM_SPLASH_WINDOW_DEFAULT_WIDTH;
      requisition->height = MSM_SPLASH_WINDOW_DEFAULT_HEIGHT;
    }
  else
    {
      requisition->width = gdk_pixbuf_get_width (priv->background);
      requisition->height = gdk_pixbuf_get_height (priv->background);
    }
}

static void
msm_splash_window_size_allocate (GtkWidget * widget,
                                 GtkAllocation * allocation)
{
  MsmSplashWindow *window = MSM_SPLASH_WINDOW (widget);
  MsmSplashWindowPrivate *priv;

  priv = window->priv;

  priv->background_bounds = *allocation;

  if (GTK_WIDGET_CLASS (parent_class)->size_allocate)
    (*GTK_WIDGET_CLASS (parent_class)->size_allocate) (widget, allocation);
}

static gboolean
msm_splash_window_button_release_event (GtkWidget * widget,
                                        GdkEventButton * event)
{
  gtk_widget_hide (widget);
  return TRUE;
}

GtkWidget *
msm_splash_window_new (GdkPixbuf *background)
{
  GtkWidget *window;

  /* The below #if 0'd out properties are not needed if the window manager
   * supports the SPLASHSCREEN hint.  Note that the window_position property
   * is needed because the session manager will be starting the window
   * manager, so the splash screen will be running temporarily without a
   * window manager.
   *
   * FIXME:  The whole point of using semantic window types such as the
   * SPLASHSCREEN hint is so that it is up to the window manager to decide
   * how to treat the window.  If a window manager doesn't choose to center
   * splash screens (for whatever reason), then the splash screen is going to
   * jump around when the window manager gets started.  One possible solution
   * is to not load the splash screen until the window manager is loaded.
   * That means it will take longer for the user to get feedback on load
   * progress though.
   */
  window = g_object_new (MSM_TYPE_SPLASH_WINDOW,
                         "type_hint", GDK_WINDOW_TYPE_HINT_SPLASHSCREEN,
                         "window_position", GTK_WIN_POS_CENTER_ALWAYS,
#if 0
                         "decorated", FALSE,
                         "allow_shrink", FALSE, "allow_grow", FALSE,
#endif
                         "icon-size", MSM_SPLASH_WINDOW_DEFAULT_ICON_SIZE,
                         "icon-spacing", MSM_SPLASH_WINDOW_DEFAULT_ICON_SPACING,
                         "icon-border-width", MSM_SPLASH_WINDOW_DEFAULT_ICON_BORDER_WIDTH,
                         "background", background,
                         NULL);

  return window;
}

int
msm_splash_window_add_icon (MsmSplashWindow * window, GdkPixbuf * icon)
{
  MsmSplashWindowPrivate *priv;
  GdkRectangle icon_bounds;

  g_return_val_if_fail (window != NULL, -1);
  g_return_val_if_fail (icon != NULL, -1);

  priv = window->priv;

  g_return_val_if_fail (!g_list_find (priv->icons, icon), -1);

  priv->icons = g_list_append (priv->icons, g_object_ref (icon));

  msm_splash_window_layout_icon (window, icon, &icon_bounds);

  gtk_widget_queue_draw_area (GTK_WIDGET (window),
                              icon_bounds.x, icon_bounds.y,
                              icon_bounds.width, icon_bounds.height);

  return msm_splash_window_get_icon_position (window, icon);
}

void
msm_splash_window_set_status (MsmSplashWindow * window, const char *status)
{
  MsmSplashWindowPrivate *priv;

  g_return_if_fail (window != NULL);

  priv = window->priv;

  priv->status = g_strdup (status);

  pango_layout_set_text (priv->layout, status, -1);

  priv->status_font_size_attr->end_index = strlen (status) + 1;

  gtk_widget_queue_draw_area (GTK_WIDGET (window),
                              priv->status_bounds.x, priv->status_bounds.y,
                              priv->status_bounds.width,
                              priv->status_bounds.height);

  msm_splash_window_calculate_status_bounds (window);

  gtk_widget_queue_draw_area (GTK_WIDGET (window),
                              priv->status_bounds.x, priv->status_bounds.y,
                              priv->status_bounds.width,
                              priv->status_bounds.height);
}

int
msm_splash_window_get_icon_position (MsmSplashWindow * window,
                                     GdkPixbuf * icon)
{
  MsmSplashWindowPrivate *priv;

  g_return_val_if_fail (window != NULL, -1);
  g_return_val_if_fail (icon != NULL, -1);

  priv = window->priv;

  return g_list_index (priv->icons, icon);
}

static void
msm_splash_window_destroy_icon (GdkPixbuf * icon)
{
  GdkRectangle *icon_bounds;
  GdkPixbuf *scaled_icon;

  icon_bounds = g_object_get_data (G_OBJECT (icon),
                                   MSM_SPLASH_WINDOW_ICON_BOUNDS);
  g_free (icon_bounds);

  scaled_icon = g_object_get_data (G_OBJECT (icon),
                                   MSM_SPLASH_WINDOW_SCALED_ICON);
  g_object_unref (scaled_icon);

  g_object_unref (icon);
}

void
msm_splash_window_replace_icon (MsmSplashWindow * window, GdkPixbuf * icon,
                                gint position)
{
  MsmSplashWindowPrivate *priv;
  GdkRectangle *icon_bounds, *icon_bounds_copy;
  GList *tmp;

  g_return_if_fail (window != NULL);
  g_return_if_fail (icon != NULL);
  g_return_if_fail (position >= 0);

  priv = window->priv;

  g_return_if_fail (!g_list_find (priv->icons, icon));

  tmp = g_list_nth (priv->icons, position);

  icon_bounds = g_object_get_data (G_OBJECT (tmp->data),
                                   MSM_SPLASH_WINDOW_ICON_BOUNDS);
  icon_bounds_copy = g_new (GdkRectangle, 1);
  *icon_bounds_copy = *icon_bounds;
  g_object_set_data (G_OBJECT (icon), MSM_SPLASH_WINDOW_ICON_BOUNDS,
                     icon_bounds_copy);
  msm_splash_window_scale_icon (window, icon);

  msm_splash_window_destroy_icon (GDK_PIXBUF (tmp->data));
  tmp->data = g_object_ref (icon);

  gtk_widget_queue_draw_area (GTK_WIDGET (window),
                              icon_bounds_copy->x, icon_bounds_copy->y,
                              icon_bounds_copy->width,
                              icon_bounds_copy->height);
}


static gboolean
msm_splash_window_calc_n_icon_rows (MsmSplashWindow * window)
{
  int i;
  MsmSplashWindowPrivate *priv;

  priv = window->priv;

  static struct
  {
    int icon_size;
    int icon_spacing;
    int icon_n_rows;
  } scales[] =
  {
    { MSM_SPLASH_WINDOW_DEFAULT_ICON_SIZE,
      MSM_SPLASH_WINDOW_DEFAULT_ICON_SPACING,
      MSM_SPLASH_WINDOW_DEFAULT_ICON_ROWS },
    { 24, 3, 1 },
    { 24, 3, 1 },
    { 16, 2, 2 },
    { 16, 2, 3 },
    { 12, 1, 3 },
    { 8, 1, 4 },
    { 4, 1, 5 },
    { 4, 1, 4 }
  };

  for (i = 0; i < G_N_ELEMENTS (scales); i++)
    {
      if (scales[i].icon_size < priv->icon_size ||
          scales[i].icon_n_rows > priv->icon_n_rows)
        {
          msm_splash_window_set_icon_size (window, scales[i].icon_size);
          msm_splash_window_set_icon_spacing (window, scales[i].icon_spacing);
          priv->icon_n_rows = scales[i].icon_n_rows;
          msm_splash_window_layout_all_icons (window);
          break;
        }
    }

  if (i == G_N_ELEMENTS (scales))
    {
      g_warning ("Too many inits - overflowing");
      return FALSE;
    }
  else
    return TRUE;
}

static void
msm_splash_window_calculate_status_bounds (MsmSplashWindow * window)
{
  PangoRectangle pixel_rect;
  MsmSplashWindowPrivate *priv;

  priv = window->priv;

  pango_layout_get_pixel_extents (priv->layout, NULL, &pixel_rect);

  priv->status_bounds.x =
    (priv->background_bounds.x + priv->background_bounds.width / 2 -
     pixel_rect.width / 2);
  priv->status_bounds.y =
    (priv->background_bounds.y + priv->background_bounds.height -
     pixel_rect.height - MSM_SPLASH_WINDOW_LABEL_V_OFFSET);
  priv->status_bounds.width = pixel_rect.width + 1;
  priv->status_bounds.height = pixel_rect.height + 1;
}

static void
msm_splash_window_scale_icon (MsmSplashWindow * window, GdkPixbuf * icon)
{
  MsmSplashWindowPrivate *priv;
  GdkPixbuf *scaled_icon = NULL, *old_scaled_icon = NULL;
  gint icon_width, icon_height;

  icon_width = gdk_pixbuf_get_width (icon);
  icon_height = gdk_pixbuf_get_height (icon);

  priv = window->priv;

  old_scaled_icon = g_object_get_data (G_OBJECT (icon), 
                                       MSM_SPLASH_WINDOW_SCALED_ICON);

  if (old_scaled_icon)
    g_object_unref (old_scaled_icon);

  if (icon_width == priv->icon_size && icon_height == priv->icon_size)
    {
      scaled_icon = g_object_ref (icon);
    }
  else
    {
      scaled_icon = gdk_pixbuf_scale_simple (icon,
                                             priv->icon_size, priv->icon_size,
                                             GDK_INTERP_BILINEAR);
    }

  g_object_set_data (G_OBJECT (icon), MSM_SPLASH_WINDOW_SCALED_ICON,
                     scaled_icon);
}

static void
msm_splash_window_scale_all_icons (MsmSplashWindow *window)
{
  GList *icon = NULL;
  MsmSplashWindowPrivate *priv;

  priv = window->priv;

  icon = priv->icons;
  while (icon)
    {
      msm_splash_window_scale_icon (window, GDK_PIXBUF (icon->data));
      icon = icon->next;
    }

  gtk_widget_queue_draw (GTK_WIDGET (window));
}

static void
msm_splash_window_layout_icon (MsmSplashWindow * window, GdkPixbuf * icon,
                               GdkRectangle * area)
{
  GdkRectangle *icon_bounds;
  MsmSplashWindowPrivate *priv;
  int row_width;

  g_return_if_fail (window != NULL);
  g_return_if_fail (icon != NULL);

  priv = window->priv;

  msm_splash_window_scale_icon (window, icon);

  icon_bounds = g_object_get_data (G_OBJECT (icon),
                                   MSM_SPLASH_WINDOW_ICON_BOUNDS);

  if (!icon_bounds)
    icon_bounds = g_new (GdkRectangle, 1);

  row_width = priv->current_column * (priv->icon_size + priv->icon_spacing);
  icon_bounds->x =
    priv->background_bounds.x + row_width + priv->icon_border_width;
  icon_bounds->y =
    (priv->background_bounds.y + priv->background_bounds.height -
     MSM_SPLASH_WINDOW_ICON_V_OFFSET - (priv->icon_size +
                                        priv->icon_spacing) *
     priv->current_row);
  icon_bounds->width = priv->icon_size;
  icon_bounds->height = priv->icon_size;

  priv->current_column++;

  if (area)
    *area = *icon_bounds;

  g_object_set_data (G_OBJECT (icon), MSM_SPLASH_WINDOW_ICON_BOUNDS,
                     icon_bounds);

  if (row_width >=
      (priv->background_bounds.width - priv->icon_border_width * 2 -
       priv->icon_size))
    {
      if (--priv->current_row > 0)
        priv->current_column = 0;

      else
        {
          if (msm_splash_window_calc_n_icon_rows (window))
            {
              msm_splash_window_layout_all_icons (window);
            }
        }
    }
}

static void
msm_splash_window_layout_all_icons (MsmSplashWindow * window)
{
  GList *icon = NULL;
  MsmSplashWindowPrivate *priv;

  priv = window->priv;

  priv->current_column = 0;
  priv->current_row = priv->icon_n_rows;

  icon = priv->icons;
  while (icon)
    {
      msm_splash_window_layout_icon (window, GDK_PIXBUF (icon->data), NULL);
      icon = icon->next;
    }

  gtk_widget_queue_draw (GTK_WIDGET (window));
}
