/* test app for startup modules */

/* 
 * Copyright (C) 2001 Havoc Pennington
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "startup.h"
#include <gtk/gtk.h>

static MsmStartupAppInfo apps[] = {
  {
    FALSE, FALSE, FALSE,
    "Nautilus",
    NULL
  },
  {
    FALSE, FALSE, FALSE,
    "Sawfish",
    NULL
  },
  {
    FALSE, FALSE, FALSE,
    "Panel",
    NULL
  },
  {
    FALSE, FALSE, FALSE,
    "Terminal",
    NULL
  },
  {
    FALSE, FALSE, FALSE,
    "Gnumeric",
    NULL
  },
  {
    FALSE, FALSE, FALSE,
    "Frobatifier",
    NULL
  },
  {
    FALSE, FALSE, FALSE,
    "Foobar",
    NULL
  },
  {
    FALSE, FALSE, FALSE,
    "PowerGEGL 3021",
    NULL
  },
};

static gboolean
check_done (void)
{
  int i;

  i = 0;
  while (i < G_N_ELEMENTS (apps))
    {
      if (!apps[i].started || !apps[i].finished)
        return FALSE;

      ++i;
    }

  return TRUE;
}

static gboolean
any_finish_pending (void)
{
  int i;

  i = 0;
  while (i < G_N_ELEMENTS (apps))
    {
      if (apps[i].started && !apps[i].finished)
        return TRUE;

      ++i;
    }

  return FALSE;
}

static gboolean
any_start_pending (void)
{
  int i;

  i = 0;
  while (i < G_N_ELEMENTS (apps))
    {
      if (!apps[i].started)
        return TRUE;

      ++i;
    }

  return FALSE;
}

static gboolean
test_timeout (gpointer data)
{
  MsmStartupInfo *info = data;
  gboolean start;

  /* Only run a random percentage of the time */
  if (g_random_int_range (0, 3) != 0)
    return TRUE;

  if (check_done ())
    {
      gtk_main_quit ();
      return FALSE;
    }
  
  if (!any_finish_pending ())
    start = TRUE;
  else if (!any_start_pending ())
    start = FALSE;
  else
    start = g_random_boolean ();
  
  if (start)
    {
      /* Find a random unstarted app to start */
      int i = g_random_int_range (0, G_N_ELEMENTS (apps));
      while (apps[i].started)
        i = g_random_int_range (0, G_N_ELEMENTS (apps));

      apps[i].started = TRUE;
      msm_startup_begin_app (info, i);
    }
  else
    {
      /* Find a random unfinished app to finish */
      int i = g_random_int_range (0, G_N_ELEMENTS (apps));
      while (!apps[i].started || apps[i].finished)
        i = g_random_int_range (0, G_N_ELEMENTS (apps));

      apps[i].finished = TRUE;
      msm_startup_end_app (info, i);
    }
  
  return TRUE;
}

int
main (int argc, char **argv)
{
  MsmStartupInfo info;
  int i;

  gtk_init (&argc, &argv);
  
  info.translated_session_name = "Testing";
  info.n_apps = G_N_ELEMENTS (apps);
  info.apps = apps;
  info.current_app = 0;
  
  i = 0;
  while (i < info.n_apps)
    {
      /* Make up a really ugly icon */
      apps[i].icon =
        gdk_pixbuf_new (GDK_COLORSPACE_RGB,
                        FALSE,
                        8, 32, 32);
      gdk_pixbuf_fill (apps[i].icon,
                       g_random_int_range (0x0, 0xffffff + 1));
      
      ++i;
    }

  if (argc == 1)
    msm_startup_set_module (NULL);
  else if (argc == 2)
    msm_startup_set_module (argv[1]);
  else
    {
      g_printerr ("too many arguments\n");
      return 1;
    }
  
  msm_startup_begin (&info);
  
  g_timeout_add (200, test_timeout, &info);
  
  gtk_main ();

  msm_startup_end (&info);

  /* we require ctrl-c to exit, this is so we can
   * test unloading the startup module and doing end-of-startup
   * effects
   */
  gtk_main ();
  
  return 0;
}
