/* shutdowndialog.c - dialog for shutting down
 *
 * Copyright (C) 2004 Ray Strode 
 * Some logout code (C) 2001 Havoc Pennington
 * Some logout code (C) 2000 Red Hat, Inc.,
 *  written by Owen Taylor <otaylor@redhat.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.  
 */
#include "shutdowndialog.h"
#include "gdmremote.h"
#include "inlinepixbufs.h"
#include "util.h"
#include <gtk/gtk.h>
#include <stdio.h>
#include <string.h>

#define GDM_SOCKET "/tmp/.gdm_socket"
#define MSM_STOCK_LOGOUT "msm-logout"
#define MSM_STOCK_REBOOT "msm-reboot"
#define MSM_STOCK_HALT "msm-shutdown"

enum
{
  PROP_0,
  PROP_SHUTDOWN_TYPES,
  PROP_SAVE_SESSION,
  PROP_SELECTED_SHUTDOWN_TYPE
};

static void msm_shutdown_dialog_class_init (MsmShutdownDialogClass * klass);
static void msm_shutdown_dialog_init (MsmShutdownDialog * object);
static void msm_shutdown_dialog_finalize (GObject * object);
static void msm_shutdown_dialog_set_property (GObject * object,
                                              guint prop_id,
                                              const GValue * value,
                                              GParamSpec * pspec);

static void msm_shutdown_dialog_get_property (GObject * object,
                                              guint prop_id,
                                              GValue * value,
                                              GParamSpec * pspec);

static void msm_shutdown_dialog_shutdown_types_set (MsmShutdownDialog *dialog);

static void
msm_shutdown_dialog_selected_shutdown_type_changed (MsmShutdownDialog *dialog);

static void
msm_shutdown_dialog_save_session_changed (MsmShutdownDialog *dialog, 
                                          GParamSpec        *pspec, 
                                          GtkToggleButton   *toggle);
static void
msm_shutdown_dialog_save_session_button_toggled (GtkToggleButton   *toggle,
                                                 MsmShutdownDialog *dialog);

static void add_stock_icon (GtkIconFactory *factory,
                            const char     *name,
                            const guint8   *data);

static void register_stock_icons (void);

struct _MsmShutdownDialogPrivate
{
  GtkWidget *bbox;
  GdmRemote *gdm_remote;
  MsmShutdownTypeFlags shutdown_types;
  MsmShutdownTypeFlags selected_shutdown_type;
  guint save_session : 1;
};

#define MSM_SHUTDOWN_DIALOG_GET_PRIVATE(o) \
           (G_TYPE_INSTANCE_GET_PRIVATE ((o), MSM_TYPE_SHUTDOWN_DIALOG, \
                                         MsmShutdownDialogPrivate))

static GtkDialogClass *parent_class = NULL;

GType
msm_shutdown_dialog_get_type (void)
{
  static GType shutdown_dialog_type = 0;

  if (shutdown_dialog_type == 0)
    {
      static const GTypeInfo shutdown_dialog_info = {
        sizeof (MsmShutdownDialogClass),
        NULL,                        /* base_init */
        NULL,                        /* base_finalize */
        (GClassInitFunc) msm_shutdown_dialog_class_init,
        NULL,                        /* class_finalize */
        NULL,                        /* class_data */
        sizeof (MsmShutdownDialog),
        0,                        /* n_preallocs */
        (GInstanceInitFunc) msm_shutdown_dialog_init
      };
      shutdown_dialog_type = g_type_register_static (GTK_TYPE_DIALOG,
                                                     "MsmShutdownDialog",
                                                     &shutdown_dialog_info,
                                                     0);
    }

  return shutdown_dialog_type;
}

static void
msm_shutdown_dialog_class_init (MsmShutdownDialogClass * klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;

  parent_class = g_type_class_peek_parent (klass);

  gobject_class->finalize = msm_shutdown_dialog_finalize;
  gobject_class->set_property = msm_shutdown_dialog_set_property;
  gobject_class->get_property = msm_shutdown_dialog_get_property;

  g_type_class_add_private (klass, sizeof (MsmShutdownDialogPrivate));

  g_object_class_install_property (gobject_class, PROP_SHUTDOWN_TYPES,
                                   g_param_spec_flags ("shutdown-types",
                                                   _("Shutdown Types"),
                                                   _("Available shutdown types "
                                                     "to offer to user"),
                                                   MSM_TYPE_SHUTDOWN_TYPE_FLAGS,
                                                   MSM_SHUTDOWN_TYPE_LOGOUT,
                                                   G_PARAM_READWRITE |
                                                   G_PARAM_CONSTRUCT_ONLY));

   g_object_class_install_property (gobject_class, PROP_SAVE_SESSION,
                                   g_param_spec_boolean ("save-session",
                                                   _("Save Session"),
                                                   _("Whether or not to save "
                                                     "session"),
                                                   TRUE,
                                                   G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_SELECTED_SHUTDOWN_TYPE,
                                   g_param_spec_flags ("selected-shutdown-type",
                                                   _("Selected Shutdown Type"),
                                                   _("Default shutdown type"),
                                                   MSM_TYPE_SHUTDOWN_TYPE_FLAGS,
                                                   MSM_SHUTDOWN_TYPE_LOGOUT,
                                                   G_PARAM_READWRITE));

}

static void
msm_shutdown_dialog_init (MsmShutdownDialog * dialog)
{
  MsmShutdownDialogPrivate *priv = MSM_SHUTDOWN_DIALOG_GET_PRIVATE (dialog);
  GtkWidget *label, *hbox, *image, *toggle_button;

  dialog->priv = priv;

  register_stock_icons ();

  gtk_window_stick (GTK_WINDOW (dialog));

  gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog)->vbox), 12);

  hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_set_spacing (GTK_BOX (hbox), 12);
  gtk_container_set_border_width (GTK_CONTAINER (hbox), 6);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), hbox);
  gtk_widget_show (hbox);

  image = gtk_image_new_from_stock (MSM_STOCK_LOGOUT, GTK_ICON_SIZE_DIALOG);
  gtk_misc_set_alignment (GTK_MISC (image), GTK_MISC (image)->xalign, 0.0);
  gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
  gtk_widget_show (image);

  label = gtk_label_new (_("<span weight=\"bold\" size=\"larger\">"
                           "End the session"
                           "</span>\n\n"
                           "Would you like to end the session?  "));
  gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_misc_set_padding (GTK_MISC (label), 6, 0);
  gtk_misc_set_alignment (GTK_MISC (label), GTK_MISC (label)->xalign, 0.0);
  gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);

  toggle_button = gtk_check_button_new_with_label (_("Save current setup"));
  gtk_container_set_border_width (GTK_CONTAINER (toggle_button), 6);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), toggle_button,
                      FALSE, FALSE, 0);
  g_signal_connect (G_OBJECT (toggle_button), "toggled",
                   G_CALLBACK (msm_shutdown_dialog_save_session_button_toggled),
                   dialog);
  g_signal_connect (G_OBJECT (dialog), "notify::save-session",
                    G_CALLBACK (msm_shutdown_dialog_save_session_changed),
                    toggle_button);
                 
  gtk_widget_show (toggle_button);

  g_signal_connect (G_OBJECT (dialog), "notify::shutdown-types",
                    G_CALLBACK (msm_shutdown_dialog_shutdown_types_set),
                    NULL);
  g_signal_connect (G_OBJECT (dialog), "notify::selected-shutdown-type",
                  G_CALLBACK (msm_shutdown_dialog_selected_shutdown_type_changed),
                  NULL);
}

static void
msm_shutdown_dialog_save_session_changed (MsmShutdownDialog *dialog, 
                                          GParamSpec        *pspec, 
                                          GtkToggleButton   *toggle)
{
  MsmShutdownDialogPrivate *priv = dialog->priv;
  gtk_toggle_button_set_active (toggle, priv->save_session);
}

static void
msm_shutdown_dialog_selected_shutdown_type_changed (MsmShutdownDialog *dialog) 
{
  MsmShutdownDialogPrivate *priv = dialog->priv;
  gtk_dialog_set_default_response (GTK_DIALOG (dialog), 
                                   priv->selected_shutdown_type);
}

static void
msm_shutdown_dialog_save_session_button_toggled (GtkToggleButton   *toggle,
                                                 MsmShutdownDialog *dialog)
{
  gboolean save_session;
  
  save_session = gtk_toggle_button_get_active (toggle);

  g_object_set (G_OBJECT (dialog), "save-session", save_session, NULL);
}

static void 
msm_shutdown_dialog_shutdown_types_set (MsmShutdownDialog *dialog)
{
  MsmShutdownDialogPrivate *priv = dialog->priv;

  if (priv->shutdown_types & MSM_SHUTDOWN_TYPE_REBOOT)
    gtk_dialog_add_button (GTK_DIALOG (dialog), MSM_STOCK_REBOOT,
                           MSM_SHUTDOWN_TYPE_REBOOT);

  if (priv->shutdown_types & MSM_SHUTDOWN_TYPE_HALT)
    gtk_dialog_add_button (GTK_DIALOG (dialog), MSM_STOCK_HALT,
                           MSM_SHUTDOWN_TYPE_HALT);

  gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_CANCEL,
                         MSM_SHUTDOWN_TYPE_CANCEL);

  gtk_dialog_add_button (GTK_DIALOG (dialog), MSM_STOCK_LOGOUT,
                         MSM_SHUTDOWN_TYPE_LOGOUT);
}

static void
register_stock_icons (void)
{
  static gboolean run_before = FALSE;
  GtkIconFactory *factory;
  
  static GtkStockItem items[] = {
    { MSM_STOCK_LOGOUT,
      N_("_Log out"),
      0, 0, NULL },
    { MSM_STOCK_HALT,
      N_("_Power off"),
      0, 0, NULL },
    { MSM_STOCK_REBOOT,
      N_("_Reboot"),
      0, 0, NULL },
  };

  if (run_before)
    return;

  run_before = TRUE;

  /* Register our stock items */
  gtk_stock_add (items, G_N_ELEMENTS (items));
  
  /* Add our custom icon factory to the list of defaults */
  factory = gtk_icon_factory_new ();
  gtk_icon_factory_add_default (factory);
  
  add_stock_icon (factory, MSM_STOCK_LOGOUT, logout_icon_data);
  add_stock_icon (factory, MSM_STOCK_HALT, shutdown_icon_data);
  add_stock_icon (factory, MSM_STOCK_REBOOT, reboot_icon_data);

  /* Drop our reference to the factory, GTK will hold a reference. */
  g_object_unref (G_OBJECT (factory));
}

static void
add_stock_icon (GtkIconFactory *factory,
                const char     *name,
                const guint8   *data)
{

  GdkPixbuf *pixbuf;
  GtkIconSet *icon_set;
  
  pixbuf = gdk_pixbuf_new_from_inline (-1, data, FALSE,
                                       NULL);

  g_assert (pixbuf);
  icon_set = gtk_icon_set_new_from_pixbuf (pixbuf);
  gtk_icon_factory_add (factory, name, icon_set);
  gtk_icon_set_unref (icon_set);
  g_object_unref (G_OBJECT (pixbuf));
}

static void
msm_shutdown_dialog_finalize (GObject * object)
{
  MsmShutdownDialog *dialog = (MsmShutdownDialog *) object;
  MsmShutdownDialogPrivate *priv;

  priv = dialog->priv;

  if (G_OBJECT_CLASS (parent_class)->finalize)
    (*G_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void
msm_shutdown_dialog_set_property (GObject * object,
                                  guint prop_id,
                                  const GValue * value, GParamSpec * pspec)
{

  MsmShutdownDialog *dialog = MSM_SHUTDOWN_DIALOG (object);
  MsmShutdownDialogPrivate *priv;

  priv = dialog->priv;

  switch (prop_id)
    {
    case PROP_SHUTDOWN_TYPES: 
      {
        MsmShutdownTypeFlags flags;
        flags = g_value_get_flags (value);
        priv->shutdown_types = flags;
        g_object_notify (object, "shutdown-types");
        break;
      }
    case PROP_SAVE_SESSION:
      {
        gboolean save_session;
        save_session = g_value_get_boolean (value);
        priv->save_session = save_session;
        g_object_notify (object, "save-session");
      }
      break;
    case PROP_SELECTED_SHUTDOWN_TYPE:
      {
        MsmShutdownTypeFlags type;
        type = g_value_get_flags (value);
        priv->selected_shutdown_type = type;
        g_object_notify (G_OBJECT (dialog), "selected-shutdown-type");
      }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
msm_shutdown_dialog_get_property (GObject * object,
                                  guint prop_id,
                                  GValue * value, GParamSpec * pspec)
{
  MsmShutdownDialog *dialog = MSM_SHUTDOWN_DIALOG (object);
  MsmShutdownDialogPrivate *priv;

  priv = dialog->priv;

  switch (prop_id)
    {
    case PROP_SHUTDOWN_TYPES:
      g_value_set_flags (value, priv->shutdown_types);
      break;
    case PROP_SAVE_SESSION:
      g_value_set_flags (value, priv->save_session);
      break;
    case PROP_SELECTED_SHUTDOWN_TYPE:
      g_value_set_flags (value, priv->selected_shutdown_type);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

GtkWidget *
msm_shutdown_dialog_new (MsmShutdownTypeFlags shutdown_types)
{
  GtkWidget *dialog;

  dialog = g_object_new (MSM_TYPE_SHUTDOWN_DIALOG,
                         "shutdown-types", shutdown_types,
                         "has_separator", FALSE,
                         "window_position", GTK_WIN_POS_CENTER,
                         "title", _("End Session"), 
                         "resizable", FALSE, 
                         "border-width", 6,
                         NULL);

  return dialog;
}

MsmShutdownTypeFlags
msm_shutdown_dialog_get_shutdown_types (MsmShutdownDialog * dialog)
{
  MsmShutdownDialogPrivate *priv;
  priv = dialog->priv;

  return priv->shutdown_types;
}

gboolean
msm_shutdown_dialog_get_save_session (MsmShutdownDialog * dialog)
{
  MsmShutdownDialogPrivate *priv;
  priv = dialog->priv;

  return priv->save_session;
}

GType
msm_shutdown_type_flags_get_type (void)
{
  static GType type = 0;

  if (!type)
    {
      static const GFlagsValue values[] = 
        {
          {MSM_SHUTDOWN_TYPE_LOGOUT, "MSM_SHUTDOWN_TYPE_LOGOUT", "logout"},
          {MSM_SHUTDOWN_TYPE_REBOOT, "MSM_SHUTDOWN_TYPE_REBOOT", "reboot"},
          {MSM_SHUTDOWN_TYPE_HALT, "MSM_SHUTDOWN_TYPE_HALT", "halt"},
          {0, NULL, NULL}
        };

      type = g_flags_register_static ("MsmShutdownTypeFlags", values);
    }

  return type;
}
