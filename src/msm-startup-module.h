/* msm public header for login progress-indicator modules */

/* 
 * Copyright (C) 2001 Havoc Pennington
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef MSM_STARTUP_MODULE_H
#define MSM_STARTUP_MODULE_H

#include <glib.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

/* Increment if you change the startup module interface incompatibly */
#define MSM_STARTUP_MODULE_VERSION 2

typedef struct _MsmStartupModule MsmStartupModule;
typedef struct _MsmStartupInfo   MsmStartupInfo;
typedef struct _MsmStartupAppInfo   MsmStartupAppInfo;

/* startup modules implement this */
struct _MsmStartupModule
{
  int module_interface_version;
  gboolean (* init)          (void);
  void (* shutdown)          (void);
  int  (* get_shutdown_time) (void);
  void (* begin)     (MsmStartupInfo *info);
  void (* end)       (MsmStartupInfo *info);
  void (* begin_app) (MsmStartupInfo *info,
                      int             app_index);
  void (* end_app)   (MsmStartupInfo *info,
                      int             app_index);
};

struct _MsmStartupInfo
{
  const char *translated_session_name;
  int n_apps;
  MsmStartupAppInfo *apps;
  int current_app;
  gboolean wm_is_running;
};

struct _MsmStartupAppInfo
{
  guint started  : 1;   
  guint registered : 1; 
  guint finished : 1; 
  const char *name;
  GdkPixbuf *icon;
};

#endif

