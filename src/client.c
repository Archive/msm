/* msm client object */

/* 
 * Copyright (C) 2001 Havoc Pennington
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "client.h"
#include "props.h"
#include "util.h"
#include "session.h"

#include <stdlib.h>
#include <string.h>
#include <time.h>

#define update_state(client, new_state)                         \
 do {                                                           \
  msm_verbose ("State change '%s' %s -> %s\n",                  \
               client->desc,                                    \
               msm_client_state_to_string (client->state),      \
               msm_client_state_to_string (new_state));         \
               (client)->state = (new_state);                   \
 } while (0)

static int next_client_number = 1;
static GHashTable *clients_by_connection = NULL;

struct _MsmClient
{
  MsmServer *server;
  SmsConn cnxn;
  MsmClientState state;
  char *id;
  char *hostname;
  char *desc;
  int restart_style;
  GList *properties;
  int number;
  time_t timeout_time;
  int n_keep_waitings;
  guint disabled : 1;
  guint ice_connection_closed : 1;
  guint in_shutdown : 1;
};

#define DEFAULT_RESTART_STYLE SmRestartIfRunning
#define CLIENT_TIMEOUT_DELAY 3

static void reset_timeout (MsmClient *client);

MsmClient*
msm_client_new (MsmServer *server,
                SmsConn    cnxn)
{
  MsmClient *client;

  client = g_new (MsmClient, 1);

  client->server = server;
  client->cnxn = cnxn;
  client->state = MSM_CLIENT_STATE_NEW;
  client->id = NULL;
  client->hostname = NULL;
  client->restart_style = DEFAULT_RESTART_STYLE;
  client->properties = NULL;
  client->timeout_time = 0;
  client->n_keep_waitings = 0;
  client->disabled = FALSE;
  client->ice_connection_closed = FALSE;
  client->in_shutdown = FALSE;
  
  client->number = next_client_number;
  ++next_client_number;

  client->desc = g_strdup_printf ("%d", client->number);
  
  reset_timeout (client);

  if (clients_by_connection == NULL)
    clients_by_connection = g_hash_table_new (NULL, NULL);

  g_hash_table_insert (clients_by_connection, client->cnxn, client);
  
  return client;
}

void
msm_client_mark_ice_connection_gone (MsmClient *client)
{
  /* Terrible hack, this whole ICE connection thing.
   * libICE just doesn't think about reentrancy at
   * all...
   */
  client->ice_connection_closed = TRUE;
  msm_verbose ("Marked ice conn closed for '%s'\n", client->desc);
}

MsmClient*
msm_client_get_by_connection (SmsConn cnxn)
{
  if (clients_by_connection)
    return g_hash_table_lookup (clients_by_connection, cnxn);
  else
    return NULL;
}

void
msm_client_free (MsmClient *client)
{
  IceConn ice_cnxn;

  g_hash_table_remove (clients_by_connection, client->cnxn);
  
  if (client->ice_connection_closed)
    ice_cnxn = NULL;
  else
    ice_cnxn = SmsGetIceConnection (client->cnxn);

  SmsCleanUp (client->cnxn);

  if (ice_cnxn)
    {
      IceSetShutdownNegotiation (ice_cnxn, False);
      IceCloseConnection (ice_cnxn);
    }
  
  proplist_free (client->properties);
  
  g_free (client->id);
  g_free (client->hostname);
  g_free (client->desc);
    
  g_free (client);
}

SmsConn
msm_client_get_connection (MsmClient *client)
{
  return client->cnxn;
}

const char*
msm_client_get_description (MsmClient *client)
{
  return client->desc;
}

MsmClientState
msm_client_get_state (MsmClient *client)
{
  return client->state;
}

gboolean
msm_client_get_in_shutdown (MsmClient *client)
{
  return client->in_shutdown;
}

MsmServer*
msm_client_get_server (MsmClient *client)
{
  return client->server;
}

const char*
msm_client_get_id (MsmClient *client)
{
  return client->id;
}

int
msm_client_get_restart_style (MsmClient *client)
{
  return client->restart_style;
}

char*
msm_client_get_user_visible_name (MsmClient *client)
{
  char *progname;

  progname = NULL;
  if (proplist_find_string (client->properties,
                            SmProgram,
                            &progname))
    return progname;
  else
    return NULL;
}

void
msm_client_register (MsmClient  *client,
                     const char *id)
{
  char *p;

  if (client->state != MSM_CLIENT_STATE_NEW)
    {
      msm_warning (_("Client '%s' attempted to register when it was already registered\n"), client->desc);

      return;
    }
  
  update_state (client, MSM_CLIENT_STATE_IDLE);
  client->id = g_strdup (id);
  
  msm_verbose ("Client '%s' has ID '%s'\n", client->desc, client->id);
  
  SmsRegisterClientReply (client->cnxn, client->id);

  reset_timeout (client);
  
  p = SmsClientHostName (client->cnxn);
  client->hostname = g_strdup (p);
  free (p);

  msm_verbose ("Client host for '%s' is '%s'\n",
               client->desc, client->hostname);
}

void
msm_client_interact_request (MsmClient *client)
{
  if (client->state != MSM_CLIENT_STATE_SAVING &&
      client->state != MSM_CLIENT_STATE_SAVING_PHASE2)
    {
      msm_warning (_("Client '%s' requested interaction when it was not being saved\n"),
                   client->desc);

      return;
    }

  msm_verbose ("Client '%s' requested interaction\n", client->desc);

  update_state (client, MSM_CLIENT_STATE_INTERACTION_REQUESTED);
  reset_timeout (client);
  
  msm_server_queue_interaction (client->server, client);
}

void
msm_client_begin_interact (MsmClient *client)
{
  msm_verbose ("Allowing '%s' to interact\n", client->desc);

  if (client->state != MSM_CLIENT_STATE_INTERACTION_REQUESTED)
    msm_warning ("Client '%s' did not request interaction\n", client->desc);

  update_state (client, MSM_CLIENT_STATE_INTERACT);

  SmsInteract (client->cnxn);

  reset_timeout (client);
}

void
msm_client_end_interact (MsmClient *client)
{
  msm_verbose ("Client '%s' is no longer allowed to interact\n", client->desc);

  update_state (client, MSM_CLIENT_STATE_INTERACTION_DONE);

  reset_timeout (client);
}

static void
internal_save (MsmClient *client,
               int        save_style,
               gboolean   allow_interaction,
               gboolean   shut_down)
{
  if (client->state != MSM_CLIENT_STATE_IDLE)
    {
      msm_warning (_("Tried to save client '%s' but it was not in the idle state\n"),
                   client->desc);

      return;
    }

  update_state (client, MSM_CLIENT_STATE_SAVING);
  /* clients can only leave the shutdown state by getting a ShutdownCancelled -
   * see SMlib source code.
   */
  if (shut_down && !client->in_shutdown)
    {
      msm_verbose ("Client '%s' entering shutdown state\n",
                   client->desc);
      client->in_shutdown = TRUE;
    }
  
  msm_verbose ("Sending SaveYourself to '%s'\n", client->desc);
  
  SmsSaveYourself (client->cnxn,
                   save_style,
                   shut_down,
                   allow_interaction ? SmInteractStyleAny : SmInteractStyleNone,
                   FALSE /* not "fast" */);

  reset_timeout (client);
}

void
msm_client_save (MsmClient *client,
                 gboolean   allow_interaction,
                 gboolean   shut_down)
{  
  internal_save (client, SmSaveBoth, /* ? don't know what to do here */
                 allow_interaction, shut_down);
}

void
msm_client_initial_save (MsmClient  *client)
{
  /* This is the save on client registration in the spec under
   * RegisterClientReply
   */
  msm_verbose ("Client '%s' needs initial SaveYourself\n",
               client->desc);
  internal_save (client, SmSaveLocal, FALSE, FALSE);
}

void
msm_client_shutdown_cancelled (MsmClient *client)
{
  switch (client->state)
    {
    case MSM_CLIENT_STATE_SAVING:
    case MSM_CLIENT_STATE_INTERACTION_REQUESTED:
    case MSM_CLIENT_STATE_INTERACT:
    case MSM_CLIENT_STATE_INTERACTION_DONE:
    case MSM_CLIENT_STATE_PHASE2_REQUESTED:
    case MSM_CLIENT_STATE_SAVING_PHASE2:
    case MSM_CLIENT_STATE_SAVE_DONE:
    case MSM_CLIENT_STATE_SAVE_FAILED:
      break;
    default:
      msm_warning (_("Tried to send cancel shutdown to client '%s' which is in state %s\n"),
                   client->desc,
                   msm_client_state_to_string (client->state));
      return;
    }

  if (!client->in_shutdown)
    {
      msm_warning (_("Tried to send cancel shutdown to client '%s' which is not in shutdown state, state %s"),
                   client->desc,
                   msm_client_state_to_string (client->state));
      return;
    }
  
  msm_verbose ("Sending ShutdownCancelled to '%s' which is in state %s\n",
               client->desc, msm_client_state_to_string (client->state));
  
  update_state (client, MSM_CLIENT_STATE_IDLE);
  client->in_shutdown = FALSE;
  SmsShutdownCancelled (client->cnxn);

  reset_timeout (client);
}

void
msm_client_phase2_request (MsmClient *client)
{
  if (client->state != MSM_CLIENT_STATE_SAVING)
    {
      msm_warning (_("Client '%s' requested phase 2 save but was not in a phase 1 save\n"),
                   client->desc);
      return;
    }
  
  update_state (client, MSM_CLIENT_STATE_PHASE2_REQUESTED);

  reset_timeout (client);
}

void
msm_client_save_phase2 (MsmClient *client)
{
  if (client->state != MSM_CLIENT_STATE_PHASE2_REQUESTED)
    {
      msm_warning (_("We tried to save client '%s' in phase 2, but it hadn't requested it.\n"), client->desc);
      return;
    }

  msm_verbose ("Sending SaveYourselfPhase2 to client '%s'\n",
               client->desc);

  update_state (client, MSM_CLIENT_STATE_SAVING_PHASE2);
  
  SmsSaveYourselfPhase2 (client->cnxn);

  reset_timeout (client);
}

void
msm_client_die (MsmClient  *client)
{
  msm_verbose ("Sending Die to client '%s'\n",
               client->desc);
  
  update_state (client, MSM_CLIENT_STATE_DEAD);
  SmsDie (client->cnxn);

  reset_timeout (client);
}

void
msm_client_save_complete (MsmClient *client)
{
  msm_verbose ("Sending SaveComplete to client '%s'\n",
               client->desc);
  
  update_state (client, MSM_CLIENT_STATE_IDLE);
  SmsSaveComplete (client->cnxn);

  reset_timeout (client);
}

void
msm_client_save_confirmed (MsmClient  *client,
                           gboolean    successful)
{
  if (client->state != MSM_CLIENT_STATE_SAVING &&
      client->state != MSM_CLIENT_STATE_SAVING_PHASE2 &&
      client->state != MSM_CLIENT_STATE_INTERACTION_DONE)
    {
      msm_warning (_("Client '%s' said it was done saving, but it hadn't been told to save\n"),
                   client->desc);
      return;
    }

  msm_verbose ("Client '%s' done saving, successful = %d\n",
               client->desc, successful);
  
  if (successful)
    update_state (client, MSM_CLIENT_STATE_SAVE_DONE);
  else
    update_state (client, MSM_CLIENT_STATE_SAVE_FAILED);

  reset_timeout (client);
}

void
msm_client_set_property_taking_ownership (MsmClient *client,
                                          SmProp    *prop)
{
  /* we own prop which should be freed with SmFreeProperty() */

  /* pass our ownership into the proplist */
  client->properties = proplist_replace (client->properties, prop);

  /* update pieces of the client struct */
  if (strcmp (prop->name, SmRestartStyleHint) == 0)
    {
      int hint;
      if (smprop_get_card8 (prop, &hint))
        client->restart_style = hint;
      else
        client->restart_style = DEFAULT_RESTART_STYLE;
    }
  else if (strcmp (prop->name, SmProgram) == 0)
    {
      char *name;

      if (smprop_get_string (prop, &name))
        {
          char *new_desc;
          
          new_desc = g_strdup_printf ("%d (%s)", client->number,
                                      name);

          msm_verbose ("Client '%s' has program name '%s'\n",
                       client->desc, name);
          
          g_free (name);
          g_free (client->desc);
          client->desc = new_desc;
        }
    }
}

void
msm_client_unset_property (MsmClient *client,
                           const char *name)
{
  client->properties = proplist_delete (client->properties, name);

  /* Return to default values */
  if (strcmp (name, SmRestartStyleHint) == 0)
    {
      client->restart_style = DEFAULT_RESTART_STYLE;
    }
}

void
msm_client_send_properties (MsmClient *client)
{
  int n_props;
  SmProp **props;

  proplist_as_array (client->properties, &props, &n_props);

  msm_verbose ("Returning all properties to '%s'\n", client->desc);
  
  SmsReturnProperties (client->cnxn, n_props, props);

  g_free (props);
}

static void
reset_timeout_but_not_keep_waitings (MsmClient *client)
{
  /* These are the states where we aren't waiting for the client
   * to do anything, or (in the case of STATE_INTERACT) we are
   * willing to wait indefinitely for the client.
   */
  if (client->state == MSM_CLIENT_STATE_IDLE ||
      client->state == MSM_CLIENT_STATE_INTERACTION_REQUESTED ||
      client->state == MSM_CLIENT_STATE_INTERACT ||
      client->state == MSM_CLIENT_STATE_PHASE2_REQUESTED ||
      client->state == MSM_CLIENT_STATE_SAVE_DONE ||
      client->state == MSM_CLIENT_STATE_SAVE_FAILED ||
      client->disabled)
    client->timeout_time = 0;
  else
    {
      int delay;

      /* lengthen delay each time the user "keeps waiting" */
      delay = CLIENT_TIMEOUT_DELAY +
        MIN (CLIENT_TIMEOUT_DELAY * client->n_keep_waitings,
             CLIENT_TIMEOUT_DELAY * 2);
      
      client->timeout_time = time (NULL) + delay;
    }

  msm_verbose ("Reset timeout for client '%s'\n", client->desc);
}

static void
reset_timeout (MsmClient *client)
{
  client->n_keep_waitings = 0;
  reset_timeout_but_not_keep_waitings (client);
}

int
msm_client_timeout_time (MsmClient *client,
                         GTime      t)
{ 
  if (client->timeout_time == 0)
    return -1;
  else
    {
      int secs;     

      secs = client->timeout_time - t;
      if (secs < 0)
        secs = 0;
      
      return secs * 1000;
    }
}

gboolean
msm_client_timed_out (MsmClient *client,
                      GTime      t)
{
  return msm_client_timeout_time (client, t) == 0;
}

void
msm_client_keep_waiting (MsmClient  *client)
{
  client->n_keep_waitings += 1;
  reset_timeout_but_not_keep_waitings (client);
  msm_verbose ("Client '%s' kept waiting %d times\n",
               client->desc, client->n_keep_waitings);
}

void
msm_client_disable (MsmClient  *client)
{
  /* FIXME */
  msm_verbose ("Disabling client '%s'\n", client->desc);

  if (client->disabled)
    msm_warning ("Client '%s' already disabled\n", client->desc);

  client->disabled = TRUE;
}

char*
msm_client_get_timeout_details (MsmClient *client)
{
  char *details;

  details = NULL;

  switch (client->state)
    {
    case MSM_CLIENT_STATE_NEW:
      details = g_strdup (_("Application has connected to the session manager, but hasn't registered successfully."));
      break;

    case MSM_CLIENT_STATE_IDLE:
      details = g_strdup (_("Internal error in the session manager: we aren't actually waiting on this application for anything!"));
      break;
   
    case MSM_CLIENT_STATE_SAVING:
      details = g_strdup (_("Application is in phase 1 of the session save."));
      break;

    case MSM_CLIENT_STATE_INTERACTION_REQUESTED:
      details = g_strdup (_("Application requested interaction but was never granted it."));
      break;

    case MSM_CLIENT_STATE_INTERACT:
      details = g_strdup (_("Internal error in the session manager: application is interacting with user, but we are still timed it out."));
      break;

    case MSM_CLIENT_STATE_INTERACTION_DONE:
      details = g_strdup (_("Application recently finished interacting with user."));
      break;

    case MSM_CLIENT_STATE_PHASE2_REQUESTED:
      details = g_strdup (_("Internal error in the session manager: application has requested a phase 2 save, but we aren't waiting for the application to do anything."));
      break;

    case MSM_CLIENT_STATE_SAVING_PHASE2:
      details = g_strdup (_("Application is in phase 2 of the session save."));
      break;
   
    case MSM_CLIENT_STATE_SAVE_DONE:
      details = g_strdup (_("Internal error in the session manager: application has successfully finished saving, we aren't waiting for it."));
      break;

    case MSM_CLIENT_STATE_SAVE_FAILED:
      details = g_strdup (_("Internal error in the session manager: application failed to successfully save, but we aren't waiting for it."));
      break;

    case MSM_CLIENT_STATE_DEAD:
      details = g_strdup (_("Application was asked to exit prior to logout, but it's still running."));
      break;
    }

  return details;
}

GList*
msm_client_copy_proplist (MsmClient *client)
{
  return proplist_copy (client->properties);
}

GList*
msm_client_get_proplist (MsmClient *client)
{
  return client->properties;
}

const char*
msm_client_state_to_string (MsmClientState state)
{
  switch (state)
    {
    case MSM_CLIENT_STATE_NEW:
      return "NEW";
    case MSM_CLIENT_STATE_IDLE:
      return "IDLE";
    case MSM_CLIENT_STATE_SAVING:
      return "SAVING";
    case MSM_CLIENT_STATE_PHASE2_REQUESTED:
      return "PHASE2_REQUESTED";
    case MSM_CLIENT_STATE_SAVING_PHASE2:
      return "SAVING_PHASE2";
    case MSM_CLIENT_STATE_SAVE_DONE:
      return "SAVE_DONE";
    case MSM_CLIENT_STATE_SAVE_FAILED:
      return "SAVE_FAILED";
    case MSM_CLIENT_STATE_DEAD:
      return "DEAD";
    case MSM_CLIENT_STATE_INTERACTION_REQUESTED:
      return "INTERACTION_REQUESTED";
    case MSM_CLIENT_STATE_INTERACT:
      return "INTERACT";
    case MSM_CLIENT_STATE_INTERACTION_DONE:
      return "INTERACTION_DONE";
    default:
      return "UNKNOWN";
    }
}
