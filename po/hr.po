# Translation of msm to Croatiann
# Copyright (C) Croatiann team
# Translators: Automatski Prijevod <>,Danijel Studen <dstuden@vuka.hr>,Nikola Planinac <>,
msgid ""
msgstr ""
"Project-Id-Version: msm 0\n"
"POT-Creation-Date: 2004-03-20 15:00+0100\n"
"PO-Revision-Date: 2004-03-20 15:00+CET\n"
"Last-Translator: auto\n"
"Language-Team: Croatian <lokalizacija@linux.hr>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: TransDict server\n"

#: gsmclient/gsmclient-test.c:18
msgid "Quitting...\n"
msgstr ""

#: gsmclient/gsmclient-test.c:27
msgid "Saving state...\n"
msgstr ""

#: gsmclient/gsmclient-test.c:28
msgid "Count level is '%d'\n"
msgstr ""

#: gsmclient/gsmclient-test.c:95
msgid "Unknown restart style '%s'.Using default restart style (if-running).\n"
msgstr ""

#: gsmclient/gsmclient-test.c:123
msgid "Unknown client role '%s'.Using default role (normal).\n"
msgstr ""

#: gsmclient/gsmclient-test.c:142
msgid "Specify session management ID"
msgstr "Odredte ID upravljanja sesijama"

#: gsmclient/gsmclient-test.c:142
msgid "ID"
msgstr "Identifikacija"

#: gsmclient/gsmclient-test.c:145
msgid ""
"Specify client role.  Can be one of: normal, window-manager, setup, desktop-"
"component"
msgstr ""

#: gsmclient/gsmclient-test.c:146
msgid "ROLE"
msgstr "ULOGA"

#: gsmclient/gsmclient-test.c:149
msgid "Specify restart style.  Can be one of: if-running, anyway, immediately, never"
msgstr ""

#: gsmclient/gsmclient-test.c:150
msgid "STYLE"
msgstr "STIL"

#: gsmclient/gsmclient-test.c:153
msgid "Specify where to count from"
msgstr ""

#: gsmclient/gsmclient-test.c:153
msgid "NUMBER"
msgstr "NUMBER"

#: gsmclient/gsmclient.c:361
msgid "%s failed to connect to the session manager: %s\n"
msgstr "%s nije uspio se spojiti na sesiju rukovoditelja: %s\n"

#: gsmclient/xsmp-control.c:59
msgid "Can't specify both --logout and --save\n"
msgstr ""

#: gsmclient/xsmp-control.c:64
msgid "Must specify either --logout or --save\n"
msgstr ""

#: gsmclient/xsmp-control.c:78
msgid "Failed to connect to session manager\n"
msgstr ""

#: src/client.c:215
msgid "Client '%s' attempted to register when it was already registered\n"
msgstr ""

#: src/client.c:243
msgid "Client '%s' requested interaction when it was not being saved\n"
msgstr ""

#: src/client.c:290
msgid "Tried to save client '%s' but it was not in the idle state\n"
msgstr ""

#: src/client.c:353
msgid "Tried to send cancel shutdown to client '%s' which is in state %s\n"
msgstr ""

#: src/client.c:361
msgid ""
"Tried to send cancel shutdown to client '%s' which is not in shutdown state, "
"state %s"
msgstr ""

#: src/client.c:382
msgid "Client '%s' requested phase 2 save but was not in a phase 1 save\n"
msgstr ""

#: src/client.c:397
msgid "We tried to save client '%s' in phase 2, but it hadn't requested it.\n"
msgstr ""

#: src/client.c:443
msgid "Client '%s' said it was done saving, but it hadn't been told to save\n"
msgstr ""

#: src/client.c:619
msgid ""
"Application has connected to the session manager, but hasn't registered "
"successfully."
msgstr ""

#: src/client.c:623
msgid ""
"Internal error in the session manager: we aren't actually waiting on this "
"application for anything!"
msgstr ""

#: src/client.c:627
msgid "Application is in phase 1 of the session save."
msgstr ""

#: src/client.c:631
msgid "Application requested interaction but was never granted it."
msgstr ""

#: src/client.c:635
msgid ""
"Internal error in the session manager: application is interacting with user, "
"but we are still timed it out."
msgstr ""

#: src/client.c:639
msgid "Application recently finished interacting with user."
msgstr ""

#: src/client.c:643
msgid ""
"Internal error in the session manager: application has requested a phase 2 "
"save, but we aren't waiting for the application to do anything."
msgstr ""

#: src/client.c:647
msgid "Application is in phase 2 of the session save."
msgstr ""

#: src/client.c:651
msgid ""
"Internal error in the session manager: application has successfully finished "
"saving, we aren't waiting for it."
msgstr ""

#: src/client.c:655
msgid ""
"Internal error in the session manager: application failed to successfully "
"save, but we aren't waiting for it."
msgstr ""

#: src/client.c:659
msgid "Application was asked to exit prior to logout, but it's still running."
msgstr ""

#: src/dialogs.c:36
msgid ""
"Your currently open applications have been recorded. These same applications "
"will be opened next time you log in."
msgstr ""

#: src/dialogs.c:63
msgid "_Details"
msgstr "_Detalji"

#: src/dialogs.c:108
msgid ""
"Your current applications were not successfully recorded, and may not be "
"restored next time you log in."
msgstr ""

#: src/gdmremote.c:47
msgid "Failed to stat socket '%s': %s\n"
msgstr ""

#: src/gdmremote.c:55
msgid "Failed to verify access permissions of socket '%s': %s\n"
msgstr ""

#: src/gdmremote.c:183
msgid "Failed to retrieve socket descriptor: %s\n"
msgstr ""

#: src/gdmremote.c:193
msgid "Failed to connect to socket '%s': %s\n"
msgstr ""

#: src/gdmremote.c:256
msgid "Failed to open X authorization file '%s': %s\n"
msgstr ""

#: src/gdmremote.c:323
#, c-format
msgid "All authentication cookies failed to authenticate with GDM socket '%s'"
msgstr ""

#: src/gdmremote.c:352
msgid "Failed to send command '%s' to socket: %s\n"
msgstr ""

#: src/gdmremote.c:370
msgid "Failed to receive answer from socket: %s\n"
msgstr ""

#: src/gdmremote.c:395
msgid "Command '%s' failed: %s\n"
msgstr ""

#: src/gdmremote.c:397
msgid "unknown error"
msgstr "nepoznata greška"

#: src/logout.c:183
msgid "Really log out?"
msgstr ""

#: src/logout.c:204 src/shutdowndialog.c:195
msgid "Save current setup"
msgstr ""

#: src/logout.c:224
msgid "Action"
msgstr "Akcija"

#: src/logout.c:232
msgid "Logout"
msgstr "Odjava"

#: src/logout.c:235
msgid "Shut Down"
msgstr ""

#: src/logout.c:238
msgid "Reboot"
msgstr "Ponovno pokretanje sustava"

#: src/logout.c:333 src/shutdowndialog.c:272
msgid "_Log out"
msgstr "_Odjavi se"

#: src/logout.c:336
msgid "_Shut down"
msgstr "_Gašenje sustava"

#: src/logout.c:339 src/shutdowndialog.c:278
msgid "_Reboot"
msgstr "_Ponovo pokretanje"

#: src/logout.c:342
msgid "_Cancel"
msgstr "_Odustani"

#: src/logout.c:510 src/server.c:409
msgid "Log out"
msgstr "Odjava"

#: src/logout.c:549
msgid "Sa_ve open applications to restore next time"
msgstr ""

#: src/main.c:68
msgid "Run in failsafe mode."
msgstr ""

#: src/main.c:71
msgid "Display detailed information about application progress."
msgstr ""

#: src/main.c:74
msgid "Load specified session."
msgstr ""

#: src/main.c:74
msgid "session"
msgstr "misija"

#: src/server.c:215
msgid "Could not initialize SMS: %s\n"
msgstr ""

#: src/server.c:289
msgid "session management error on client '%s' in state %s:\n"
msgstr ""

#: src/server.c:290
msgid "(unknown)"
msgstr "(nepoznato)"

#: src/server.c:291
msgid "n/a"
msgstr "nedostupno"

#: src/server.c:406
msgid "Reboot or shutdown was unsuccessful."
msgstr ""

#: src/server.c:596
msgid ""
"Client '%s' requested interaction, but interaction is not allowed right "
"now.\n"
msgstr ""

#: src/server.c:634
msgid ""
"Received InteractDone from client '%s' which should not be interacting right "
"now\n"
msgstr ""

#: src/server.c:727
msgid "Client '%s' requested save, but is not currently in the idle state\n"
msgstr ""

#: src/server.c:917
msgid ""
"Refusing new client connection because the session is currently being shut "
"down\n"
msgstr ""

#: src/server.c:1512
msgid ""
"Application '%s' is not responding. Keep waiting for it to respond, or "
"ignore it for now?"
msgstr ""

#: src/server.c:1520
msgid ""
"An application is not responding. Keep waiting for it to respond, or ignore "
"it for now?"
msgstr ""

#: src/server.c:1528
msgid "_Keep waiting"
msgstr ""

#: src/server.c:1530
msgid "_Ignore it"
msgstr ""

#: src/server.c:1823
msgid "Failed to accept new ICE connection\n"
msgstr ""

#: src/server.c:1871
msgid ""
"IO error trying to accept new connection (client may have crashed trying to "
"connect to the session manager, or client may be broken, or someone yanked "
"the ethernet cable)\n"
msgstr ""

#: src/server.c:1873
msgid ""
"Rejecting new connection (some client was not allowed to connect to the "
"session manager)\n"
msgstr ""

#. The default error handler would give a more informative message
#.  * than we are, but it would also exit most of the time, which
#.  * is not appropriate here.
#.  *
#.  * gnome-session closes the connection in here, but I'm not sure that
#.  * counts as a good idea. the default handler doesn't do that.
#: src/server.c:1912
msgid "ICE error received, opcode: %d sequence: %lu class: %d severity: %d\n"
msgstr ""

#: src/server.c:1954
msgid "Could not initialize ICE: %s\n"
msgstr ""

#: src/server.c:1988
msgid "Could not set up authentication\n"
msgstr ""

#: src/server.c:2040
msgid "Failed to run 'iceauth': %s\n"
msgstr ""

#: src/server.c:2055
msgid "Failed to write iceauth script: %s\n"
msgstr ""

#: src/server.c:2075
msgid "Unexpected error in waitpid() executing iceauth (%s)\n"
msgstr ""

#: src/server.c:2090
msgid "iceauth exited with failure status %d\n"
msgstr ""

#: src/server.c:2097
msgid "iceauth exited abnormally with signal %d\n"
msgstr ""

#. should not happen
#: src/server.c:2104
msgid "iceauth exited abnormally, unknown error\n"
msgstr ""

#: src/session.c:317
msgid "Sawfish Window Manager"
msgstr "Urednik prozora Sawfish"

#: src/session.c:320
msgid "Metacity Window Manager"
msgstr "Metacity upravitelj prozora"

#: src/session.c:323
msgid "Window Manager"
msgstr "Upravitelj prozora"

#: src/session.c:326
msgid "The Panel"
msgstr "Ploča"

#: src/session.c:329
msgid "Session Manager Proxy"
msgstr "Proxy upravitelja sesije"

#: src/session.c:332
msgid "Nautilus"
msgstr "Nautilus"

#: src/session.c:335
msgid "Desktop Settings"
msgstr "Postavke okružja"

#: src/session.c:503
msgid ""
"The application '%s' has been restarted several times in the last few "
"seconds, but crashed or exited each time. What do you want to do with this "
"application?"
msgstr ""

#: src/session.c:507
msgid "_Keep restarting"
msgstr ""

#: src/session.c:509
msgid "_Never restart again"
msgstr ""

#: src/session.c:511
msgid "_Stop restarting for now"
msgstr ""

#: src/session.c:544
msgid "The application '%s' could not be launched."
msgstr ""

#: src/session.c:630
msgid "Application"
msgstr "Aplikacija"

#: src/session.c:640
msgid "Error details"
msgstr "Prikaži &detalje"

#: src/session.c:724
msgid "There can be only one window manager loaded at once"
msgstr ""

#: src/session.c:747
msgid ""
"Internal session manager error, did not record an error for failed client "
"launch"
msgstr ""

#: src/session.c:1126
msgid "Some applications were not successfullly started."
msgstr ""

#: src/session.c:1349
msgid "Session file '%s' is read-only.\n"
msgstr ""

#: src/session.c:1360
msgid "Failed to open the session file '%s': %s (%s)"
msgstr ""

#: src/session.c:1370
msgid "Failed to open the session file '%s': %s"
msgstr ""

#: src/session.c:1396
msgid "Failed to lock the session file '%s': %s"
msgstr ""

#: src/session.c:1417
msgid "Failed to parse the session file '%s': %s\n"
msgstr ""

#: src/session.c:1450 src/session.c:2276
msgid "Default"
msgstr "Uobičajeno"

#: src/session.c:1490
msgid "Failsafe"
msgstr ""

#: src/session.c:1614
msgid "Not saving unknown property type '%s'\n"
msgstr ""

#: src/session.c:1647
msgid "Session '%s' is read-only\n"
msgstr ""

#: src/session.c:1657
msgid "Failed to open '%s': %s\n"
msgstr ""

#: src/session.c:1664
msgid "Failed to lock file '%s': %s"
msgstr ""

#: src/session.c:1723
msgid "Error writing new session file '%s': %s"
msgstr ""

#: src/session.c:1735 src/session.c:1750
msgid ""
"Failed to replace the old session file '%s' with the new session contents in "
"the temporary file '%s': %s"
msgstr ""

#: src/session.c:1744
#, c-format
msgid "Could not remove temporary file '%s': %s\n"
msgstr ""

#: src/session.c:1920
msgid "Select a session"
msgstr ""

#: src/session.c:1925
msgid "Load Session"
msgstr ""

#: src/session.c:1936
msgid ""
"<span weight=\"bold\" size=\"larger\">Select a session</span>\n"
"\n"
"Please select a session to load from the list below."
msgstr ""

#: src/session.c:1993
msgid "(read-only)"
msgstr "(samo-za-čitanje)"

#: src/session.c:2018
msgid "Session"
msgstr "Sesija"

#: src/session.c:2024
msgid "Status"
msgstr "Status"

#: src/session.c:2135
msgid "Could not open the session \"%s.\""
msgstr ""

#: src/session.c:2139
msgid "Abort login"
msgstr "Otkaži prijavu"

#: src/session.c:2141 src/session.c:2164 src/session.c:2183 src/session.c:2201
#: src/session.c:2219
msgid "Log into other session"
msgstr ""

#: src/session.c:2143 src/session.c:2185 src/session.c:2203 src/session.c:2221
msgid "Log into default session"
msgstr ""

#: src/session.c:2145 src/session.c:2166
msgid "Log into failsafe session"
msgstr ""

#: src/session.c:2155
msgid ""
"You are already logged in elsewhere, using the session \"%s.\" You can only "
"use a session from one location at a time."
msgstr ""

#: src/session.c:2160
msgid "Log in anyway"
msgstr "Ipak se prijavi"

#: src/session.c:2162
msgid "Try again"
msgstr "Pokušajte ponovno"

#: src/session.c:2176
msgid "The session file for session \"%s\" appears to be invalid or corrupted."
msgstr ""

#: src/session.c:2181 src/session.c:2199 src/session.c:2217
msgid "Revert session to defaults"
msgstr ""

#: src/session.c:2195
msgid "The session \"%s\" contains no applications."
msgstr ""

#: src/session.c:2213
msgid "The session \"%s\" has encountered an unrecoverable error."
msgstr ""

#: src/session.c:2257
msgid "User selected session could not be loaded."
msgstr ""

#: src/session.c:2339
msgid "line %d character %d: %s\n"
msgstr ""

#: src/session.c:2355
msgid "<client> element must have an 'id' attribute giving the session ID"
msgstr ""

#: src/session.c:2361 src/session.c:2368
msgid "<client> element does not allow the attribute '%s', only an 'id' attribute"
msgstr ""

#: src/session.c:2375
msgid "'id' attribute of <client> element has empty value"
msgstr "'id' atribut od <client> elementa ima praznu vrijednost"

#: src/session.c:2386
msgid ""
"'id' attribute of <client> element duplicates another <client> element's id "
"(duplicate id is '%s')"
msgstr ""
"'id' atribut od <client> elementa duplicira drugi <klijent> id elementa"
"(duplicirani id je '%s')"

#: src/session.c:2425
msgid "'name' attribute specified twice on <prop> element"
msgstr "'ime' atribut dvaput je naveden na <prop> elementu"

#: src/session.c:2436
msgid "'type' attribute specified twice on <prop> element"
msgstr "'tip' atribut dvaput je naveden na <prop> elementu"

#: src/session.c:2445
msgid "Attribute '%s' not allowed on <prop> element"
msgstr ""

#: src/session.c:2456
msgid "<prop> element must have a 'type' attribute"
msgstr ""

#: src/session.c:2462
msgid "<prop> element must have a 'name' attribute"
msgstr ""

#: src/session.c:2490
msgid "<prop> type '%s' is unknown, should be %s, %s, or %s"
msgstr ""

#: src/session.c:2520
msgid ""
"File does not appear to be a session file; outermost element is <%s> rather "
"than <msm_session>"
msgstr ""

#: src/session.c:2533
msgid "Element <%s> may not appear inside the <msm_session> element"
msgstr ""

#: src/session.c:2553
msgid "Element <%s> may not appear inside a <client> element"
msgstr ""

#: src/session.c:2572
msgid "Element <%s> may not appear inside a <prop> element"
msgstr ""

#: src/session.c:2578
msgid "Attributes are not allowed on the <value> element"
msgstr ""

#: src/session.c:2588
msgid "No elements allowed inside <value> element (<%s> found)"
msgstr ""

#: src/session.c:2594 src/session.c:2760
msgid "Element <%s> appears after the <msm_session> element"
msgstr ""

#: src/session.c:2613 src/session.c:2621 src/session.c:2634 src/session.c:2653
#: src/session.c:2675
msgid "Unexpected close element tag </%s>"
msgstr ""

#: src/session.c:2688
msgid "<value> element has no contents"
msgstr ""

#: src/session.c:2694
msgid ""
"Exactly one <value> element required below <prop> elements other than type "
"LISTofARRAY8 - no more and no less"
msgstr ""

#: src/session.c:2710
msgid "Failed to parse '%s' (given as value of a CARD8 property) as an integer"
msgstr ""

#: src/session.c:2716
msgid "Only values between 0 and 255 are allowed in CARD8 properties (%ld too large)"
msgstr ""

#: src/session.c:2796
msgid "Unexpected text outside of <value> element"
msgstr ""

#. Can't imagine this actually happening
#: src/session.c:2875
msgid "Failed to stat new session file descriptor (%s)\n"
msgstr ""

#: src/session.c:2892
msgid "Session is empty and no global session available.\n"
msgstr ""

#: src/session.c:3040
msgid "No '%s' command provided by the application"
msgstr ""

#: src/session.c:3120
msgid "Failed to clean up saved session data for '%s': %s"
msgstr ""

#: src/shutdowndialog.c:133
msgid "Shutdown Types"
msgstr ""

#: src/shutdowndialog.c:134
msgid "Available shutdown types to offer to user"
msgstr ""

#: src/shutdowndialog.c:143
msgid "Save Session"
msgstr "Snimi Sesiju"

#: src/shutdowndialog.c:144
msgid "Whether or not to save session"
msgstr ""

#: src/shutdowndialog.c:151
msgid "Selected Shutdown Type"
msgstr ""

#: src/shutdowndialog.c:152
msgid "Default shutdown type"
msgstr ""

#: src/shutdowndialog.c:184
msgid ""
"<span weight=\"bold\" size=\"larger\">End the session</span>\n"
"\n"
"Would you like to end the session?  "
msgstr ""

#: src/shutdowndialog.c:275
msgid "_Power off"
msgstr ""

#: src/shutdowndialog.c:411
msgid "End Session"
msgstr ""

#: src/splashwindow.c:183
msgid "Icon Spacing"
msgstr "Razmak ikona"

#: src/splashwindow.c:185
msgid "Space between splash screen icons."
msgstr ""

#: src/splashwindow.c:192
msgid "Icon Border Width"
msgstr ""

#: src/splashwindow.c:194
msgid "Space around splash screen icons."
msgstr ""

#: src/splashwindow.c:201
msgid "Icon Size"
msgstr "Veličina sličice"

#: src/splashwindow.c:203
msgid "Size of splash screen icons."
msgstr ""

#: src/splashwindow.c:210
msgid "Status Font Size"
msgstr ""

#: src/splashwindow.c:212
msgid "Size of splash screen status text."
msgstr ""

#: src/splashwindow.c:218
msgid "Background"
msgstr "Pozadina"

#: src/splashwindow.c:220
msgid "Splash screen background image."
msgstr ""

#: src/startup-default.c:142 src/startup.c:339
msgid "Starting session \"%s,\" please wait"
msgstr ""

#: src/startup-default.c:162 src/startup.c:350
msgid "Finished starting applications"
msgstr ""

#: src/startup-default.c:180 src/startup.c:369
msgid "Starting \"%s\""
msgstr ""

#: src/startup-default.c:197 src/startup.c:409
msgid "Successfully started \"%s\""
msgstr ""

#: src/startup.c:87 src/startup.c:95
msgid "Failed to open startup progress indicator module '%s': %s\n"
msgstr ""

#: src/startup.c:108
msgid "No function 'msm_startup_module_get_vtable' in module %s"
msgstr ""

#: src/startup.c:124
msgid "Module '%s' is for interface version %d but msm was compiled for %d"
msgstr ""

#: src/startup.c:134
msgid "Failed to init module '%s'"
msgstr ""

#: src/startup.c:411
msgid "Unsuccessfully started \"%s\""
msgstr ""

#: src/util.c:157
msgid "Failed to create directory '%s': %s\n"
msgstr ""

#: src/util.c:206
msgid "Failed to get writable status of file '%s': %s\n"
msgstr ""

#: src/util.c:231
msgid "Failed to get readable status of file '%s': %s\n"
msgstr ""

#: src/util.c:265
msgid "Failed to overwrite file '%s': %s\n"
msgstr ""

