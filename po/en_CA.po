# English/Canada translation of msm.
# Copyright (C) 2004 Adam Weinberger and the GNOME Foundation
# This file is distributed under the same license as the msm package.
# Adam Weinberger <adamw@gnome.org>, 2004.
# 
# 
msgid ""
msgstr ""
"Project-Id-Version: msm\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2004-05-01 02:30-0400\n"
"PO-Revision-Date: 2004-05-01 02:31-0400\n"
"Last-Translator: Adam Weinberger <adamw@gnome.org>\n"
"Language-Team: Canadian English <adamw@gnome.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit"

#: gsmclient/gsmclient-test.c:18
msgid "Quitting...\n"
msgstr "Quitting...\n"

#: gsmclient/gsmclient-test.c:27
msgid "Saving state...\n"
msgstr "Saving state...\n"

#: gsmclient/gsmclient-test.c:28
#, c-format
msgid "Count level is '%d'\n"
msgstr "Count level is '%d'\n"

#: gsmclient/gsmclient-test.c:95
#, c-format
msgid "Unknown restart style '%s'.Using default restart style (if-running).\n"
msgstr "Unknown restart style '%s'.Using default restart style (if-running).\n"

#: gsmclient/gsmclient-test.c:123
#, c-format
msgid "Unknown client role '%s'.Using default role (normal).\n"
msgstr "Unknown client role '%s'.Using default role (normal).\n"

#: gsmclient/gsmclient-test.c:142
msgid "Specify session management ID"
msgstr "Specify session management ID"

#: gsmclient/gsmclient-test.c:142
msgid "ID"
msgstr "ID"

#: gsmclient/gsmclient-test.c:145
msgid ""
"Specify client role.  Can be one of: normal, window-manager, setup, desktop-"
"component"
msgstr ""
"Specify client role.  Can be one of: normal, window-manager, setup, desktop-"
"component"

#: gsmclient/gsmclient-test.c:146
msgid "ROLE"
msgstr "ROLE"

#: gsmclient/gsmclient-test.c:149
msgid ""
"Specify restart style.  Can be one of: if-running, anyway, immediately, never"
msgstr ""
"Specify restart style.  Can be one of: if-running, anyway, immediately, never"

#: gsmclient/gsmclient-test.c:150
msgid "STYLE"
msgstr "STYLE"

#: gsmclient/gsmclient-test.c:153
msgid "Specify where to count from"
msgstr "Specify where to count from"

#: gsmclient/gsmclient-test.c:153
msgid "NUMBER"
msgstr "NUMBER"

#: gsmclient/gsmclient.c:361
#, c-format
msgid "%s failed to connect to the session manager: %s\n"
msgstr "%s failed to connect to the session manager: %s\n"

#: gsmclient/xsmp-control.c:59
#, c-format
msgid "Can't specify both --logout and --save\n"
msgstr "Can't specify both --logout and --save\n"

#: gsmclient/xsmp-control.c:64
#, c-format
msgid "Must specify either --logout or --save\n"
msgstr "Must specify either --logout or --save\n"

#: gsmclient/xsmp-control.c:78
#, c-format
msgid "Failed to connect to session manager\n"
msgstr "Failed to connect to session manager\n"

#: src/client.c:215
#, c-format
msgid "Client '%s' attempted to register when it was already registered\n"
msgstr "Client '%s' attempted to register when it was already registered\n"

#: src/client.c:243
#, c-format
msgid "Client '%s' requested interaction when it was not being saved\n"
msgstr "Client '%s' requested interaction when it was not being saved\n"

#: src/client.c:290
#, c-format
msgid "Tried to save client '%s' but it was not in the idle state\n"
msgstr "Tried to save client '%s' but it was not in the idle state\n"

#: src/client.c:353
#, c-format
msgid "Tried to send cancel shutdown to client '%s' which is in state %s\n"
msgstr "Tried to send cancel shutdown to client '%s' which is in state %s\n"

#: src/client.c:361
#, c-format
msgid ""
"Tried to send cancel shutdown to client '%s' which is not in shutdown state, "
"state %s"
msgstr ""
"Tried to send cancel shutdown to client '%s' which is not in shutdown state, "
"state %s"

#: src/client.c:382
#, c-format
msgid "Client '%s' requested phase 2 save but was not in a phase 1 save\n"
msgstr "Client '%s' requested phase 2 save but was not in a phase 1 save\n"

#: src/client.c:397
#, c-format
msgid "We tried to save client '%s' in phase 2, but it hadn't requested it.\n"
msgstr "We tried to save client '%s' in phase 2, but it hadn't requested it.\n"

#: src/client.c:443
#, c-format
msgid "Client '%s' said it was done saving, but it hadn't been told to save\n"
msgstr "Client '%s' said it was done saving, but it hadn't been told to save\n"

#: src/client.c:619
msgid ""
"Application has connected to the session manager, but hasn't registered "
"successfully."
msgstr ""
"Application has connected to the session manager, but hasn't registered "
"successfully."

#: src/client.c:623
msgid ""
"Internal error in the session manager: we aren't actually waiting on this "
"application for anything!"
msgstr ""
"Internal error in the session manager: we aren't actually waiting on this "
"application for anything!"

#: src/client.c:627
msgid "Application is in phase 1 of the session save."
msgstr "Application is in phase 1 of the session save."

#: src/client.c:631
msgid "Application requested interaction but was never granted it."
msgstr "Application requested interaction but was never granted it."

#: src/client.c:635
msgid ""
"Internal error in the session manager: application is interacting with user, "
"but we are still timed it out."
msgstr ""
"Internal error in the session manager: application is interacting with user, "
"but we are still timed it out."

#: src/client.c:639
msgid "Application recently finished interacting with user."
msgstr "Application recently finished interacting with user."

#: src/client.c:643
msgid ""
"Internal error in the session manager: application has requested a phase 2 "
"save, but we aren't waiting for the application to do anything."
msgstr ""
"Internal error in the session manager: application has requested a phase 2 "
"save, but we aren't waiting for the application to do anything."

#: src/client.c:647
msgid "Application is in phase 2 of the session save."
msgstr "Application is in phase 2 of the session save."

#: src/client.c:651
msgid ""
"Internal error in the session manager: application has successfully finished "
"saving, we aren't waiting for it."
msgstr ""
"Internal error in the session manager: application has successfully finished "
"saving, we aren't waiting for it."

#: src/client.c:655
msgid ""
"Internal error in the session manager: application failed to successfully "
"save, but we aren't waiting for it."
msgstr ""
"Internal error in the session manager: application failed to successfully "
"save, but we aren't waiting for it."

#: src/client.c:659
msgid "Application was asked to exit prior to logout, but it's still running."
msgstr "Application was asked to exit prior to logout, but it's still running."

#: src/dialogs.c:36
msgid ""
"Your currently open applications have been recorded. These same applications "
"will be opened next time you log in."
msgstr ""
"Your currently open applications have been recorded. These same applications "
"will be opened next time you log in."

#: src/dialogs.c:63
msgid "_Details"
msgstr "_Details"

#: src/dialogs.c:108
msgid ""
"Your current applications were not successfully recorded, and may not be "
"restored next time you log in."
msgstr ""
"Your current applications were not successfully recorded, and may not be "
"restored next time you log in."

#: src/gdmremote.c:47
#, c-format
msgid "Failed to stat socket '%s': %s\n"
msgstr "Failed to stat socket '%s': %s\n"

#: src/gdmremote.c:55
#, c-format
msgid "Failed to verify access permissions of socket '%s': %s\n"
msgstr "Failed to verify access permissions of socket '%s': %s\n"

#: src/gdmremote.c:183
#, c-format
msgid "Failed to retrieve socket descriptor: %s\n"
msgstr "Failed to retrieve socket descriptor: %s\n"

#: src/gdmremote.c:193
#, c-format
msgid "Failed to connect to socket '%s': %s\n"
msgstr "Failed to connect to socket '%s': %s\n"

#: src/gdmremote.c:256
#, c-format
msgid "Failed to open X authorization file '%s': %s\n"
msgstr "Failed to open X authorization file '%s': %s\n"

#: src/gdmremote.c:323
#, c-format
msgid "All authentication cookies failed to authenticate with GDM socket '%s'"
msgstr "All authentication cookies failed to authenticate with GDM socket '%s'"

#: src/gdmremote.c:352
#, c-format
msgid "Failed to send command '%s' to socket: %s\n"
msgstr "Failed to send command '%s' to socket: %s\n"

#: src/gdmremote.c:370
#, c-format
msgid "Failed to receive answer from socket: %s\n"
msgstr "Failed to receive answer from socket: %s\n"

#: src/gdmremote.c:395
#, c-format
msgid "Command '%s' failed: %s\n"
msgstr "Command '%s' failed: %s\n"

#: src/gdmremote.c:397
msgid "unknown error"
msgstr "unknown error"

#: src/logout.c:183
msgid "Really log out?"
msgstr "Really log out?"

#: src/logout.c:204 src/shutdowndialog.c:195
msgid "Save current setup"
msgstr "Save current setup"

#: src/logout.c:224
msgid "Action"
msgstr "Action"

#: src/logout.c:232
msgid "Logout"
msgstr "Logout"

#: src/logout.c:235
msgid "Shut Down"
msgstr "Shut Down"

#: src/logout.c:238
msgid "Reboot"
msgstr "Reboot"

#: src/logout.c:333 src/shutdowndialog.c:272
msgid "_Log out"
msgstr "_Log out"

#: src/logout.c:336
msgid "_Shut down"
msgstr "_Shut down"

#: src/logout.c:339 src/shutdowndialog.c:278
msgid "_Reboot"
msgstr "_Reboot"

#: src/logout.c:342
msgid "_Cancel"
msgstr "_Cancel"

#: src/logout.c:510 src/server.c:409
msgid "Log out"
msgstr "Log out"

#: src/logout.c:549
msgid "Sa_ve open applications to restore next time"
msgstr "Sa_ve open applications to restore next time"

#: src/main.c:68
msgid "Run in failsafe mode."
msgstr "Run in failsafe mode."

#: src/main.c:71
msgid "Display detailed information about application progress."
msgstr "Display detailed information about application progress."

#: src/main.c:74
msgid "Load specified session."
msgstr "Load specified session."

#: src/main.c:74
msgid "session"
msgstr "session"

#: src/server.c:215
#, c-format
msgid "Could not initialize SMS: %s\n"
msgstr "Could not initialize SMS: %s\n"

#: src/server.c:289
#, c-format
msgid "session management error on client '%s' in state %s:\n"
msgstr "session management error on client '%s' in state %s:\n"

#: src/server.c:290
msgid "(unknown)"
msgstr "(unknown)"

#: src/server.c:291
msgid "n/a"
msgstr "n/a"

#: src/server.c:406
msgid "Reboot or shutdown was unsuccessful."
msgstr "Reboot or shutdown was unsuccessful."

#: src/server.c:596
#, c-format
msgid ""
"Client '%s' requested interaction, but interaction is not allowed right "
"now.\n"
msgstr ""
"Client '%s' requested interaction, but interaction is not allowed right "
"now.\n"

#: src/server.c:634
#, c-format
msgid ""
"Received InteractDone from client '%s' which should not be interacting right "
"now\n"
msgstr ""
"Received InteractDone from client '%s' which should not be interacting right "
"now\n"

#: src/server.c:727
#, c-format
msgid "Client '%s' requested save, but is not currently in the idle state\n"
msgstr "Client '%s' requested save, but is not currently in the idle state\n"

#: src/server.c:917
msgid ""
"Refusing new client connection because the session is currently being shut "
"down\n"
msgstr ""
"Refusing new client connection because the session is currently being shut "
"down\n"

#: src/server.c:1512
#, c-format
msgid ""
"Application '%s' is not responding. Keep waiting for it to respond, or "
"ignore it for now?"
msgstr ""
"Application '%s' is not responding. Keep waiting for it to respond, or "
"ignore it for now?"

#: src/server.c:1520
msgid ""
"An application is not responding. Keep waiting for it to respond, or ignore "
"it for now?"
msgstr ""
"An application is not responding. Keep waiting for it to respond, or ignore "
"it for now?"

#: src/server.c:1528
msgid "_Keep waiting"
msgstr "_Keep waiting"

#: src/server.c:1530
msgid "_Ignore it"
msgstr "_Ignore it"

#: src/server.c:1823
msgid "Failed to accept new ICE connection\n"
msgstr "Failed to accept new ICE connection\n"

#: src/server.c:1871
msgid ""
"IO error trying to accept new connection (client may have crashed trying to "
"connect to the session manager, or client may be broken, or someone yanked "
"the ethernet cable)\n"
msgstr ""
"IO error trying to accept new connection (client may have crashed trying to "
"connect to the session manager, or client may be broken, or someone yanked "
"the ethernet cable)\n"

#: src/server.c:1873
msgid ""
"Rejecting new connection (some client was not allowed to connect to the "
"session manager)\n"
msgstr ""
"Rejecting new connection (some client was not allowed to connect to the "
"session manager)\n"

#. The default error handler would give a more informative message
#. * than we are, but it would also exit most of the time, which
#. * is not appropriate here.
#. *
#. * gnome-session closes the connection in here, but I'm not sure that
#. * counts as a good idea. the default handler doesn't do that.
#.
#: src/server.c:1912
#, c-format
msgid "ICE error received, opcode: %d sequence: %lu class: %d severity: %d\n"
msgstr "ICE error received, opcode: %d sequence: %lu class: %d severity: %d\n"

#: src/server.c:1954
#, c-format
msgid "Could not initialize ICE: %s\n"
msgstr "Could not initialize ICE: %s\n"

#: src/server.c:1988
msgid "Could not set up authentication\n"
msgstr "Could not set up authentication\n"

#: src/server.c:2040
#, c-format
msgid "Failed to run 'iceauth': %s\n"
msgstr "Failed to run 'iceauth': %s\n"

#: src/server.c:2055
#, c-format
msgid "Failed to write iceauth script: %s\n"
msgstr "Failed to write iceauth script: %s\n"

#: src/server.c:2075
#, c-format
msgid "Unexpected error in waitpid() executing iceauth (%s)\n"
msgstr "Unexpected error in waitpid() executing iceauth (%s)\n"

#: src/server.c:2090
#, c-format
msgid "iceauth exited with failure status %d\n"
msgstr "iceauth exited with failure status %d\n"

#: src/server.c:2097
#, c-format
msgid "iceauth exited abnormally with signal %d\n"
msgstr "iceauth exited abnormally with signal %d\n"

#. should not happen
#: src/server.c:2104
msgid "iceauth exited abnormally, unknown error\n"
msgstr "iceauth exited abnormally, unknown error\n"

#: src/session.c:317
msgid "Sawfish Window Manager"
msgstr "Sawfish Window Manager"

#: src/session.c:320
msgid "Metacity Window Manager"
msgstr "Metacity Window Manager"

#: src/session.c:323
msgid "Window Manager"
msgstr "Window Manager"

#: src/session.c:326
msgid "The Panel"
msgstr "The Panel"

#: src/session.c:329
msgid "Session Manager Proxy"
msgstr "Session Manager Proxy"

#: src/session.c:332
msgid "Nautilus"
msgstr "Nautilus"

#: src/session.c:335
msgid "Desktop Settings"
msgstr "Desktop Settings"

#: src/session.c:503
#, c-format
msgid ""
"The application '%s' has been restarted several times in the last few "
"seconds, but crashed or exited each time. What do you want to do with this "
"application?"
msgstr ""
"The application '%s' has been restarted several times in the last few "
"seconds, but crashed or exited each time. What do you want to do with this "
"application?"

#: src/session.c:507
msgid "_Keep restarting"
msgstr "_Keep restarting"

#: src/session.c:509
msgid "_Never restart again"
msgstr "_Never restart again"

#: src/session.c:511
msgid "_Stop restarting for now"
msgstr "_Stop restarting for now"

#: src/session.c:544
#, c-format
msgid "The application '%s' could not be launched."
msgstr "The application '%s' could not be launched."

#: src/session.c:630
msgid "Application"
msgstr "Application"

#: src/session.c:640
msgid "Error details"
msgstr "Error details"

#: src/session.c:724
msgid "There can be only one window manager loaded at once"
msgstr "There can be only one window manager loaded at once"

#: src/session.c:747
msgid ""
"Internal session manager error, did not record an error for failed client "
"launch"
msgstr ""
"Internal session manager error, did not record an error for failed client "
"launch"

#: src/session.c:1126
msgid "Some applications were not successfullly started."
msgstr "Some applications were not successfullly started."

#: src/session.c:1349
#, c-format
msgid "Session file '%s' is read-only.\n"
msgstr "Session file '%s' is read-only.\n"

#: src/session.c:1360
#, c-format
msgid "Failed to open the session file '%s': %s (%s)"
msgstr "Failed to open the session file '%s': %s (%s)"

#: src/session.c:1370
#, c-format
msgid "Failed to open the session file '%s': %s"
msgstr "Failed to open the session file '%s': %s"

#: src/session.c:1396
#, c-format
msgid "Failed to lock the session file '%s': %s"
msgstr "Failed to lock the session file '%s': %s"

#: src/session.c:1417
#, c-format
msgid "Failed to parse the session file '%s': %s\n"
msgstr "Failed to parse the session file '%s': %s\n"

#: src/session.c:1450 src/session.c:2276
msgid "Default"
msgstr "Default"

#: src/session.c:1490
msgid "Failsafe"
msgstr "Failsafe"

#: src/session.c:1614
#, c-format
msgid "Not saving unknown property type '%s'\n"
msgstr "Not saving unknown property type '%s'\n"

#: src/session.c:1647
#, c-format
msgid "Session '%s' is read-only\n"
msgstr "Session '%s' is read-only\n"

#: src/session.c:1657
#, c-format
msgid "Failed to open '%s': %s\n"
msgstr "Failed to open '%s': %s\n"

#: src/session.c:1664
#, c-format
msgid "Failed to lock file '%s': %s"
msgstr "Failed to lock file '%s': %s"

#: src/session.c:1723
#, c-format
msgid "Error writing new session file '%s': %s"
msgstr "Error writing new session file '%s': %s"

#: src/session.c:1735 src/session.c:1750
#, c-format
msgid ""
"Failed to replace the old session file '%s' with the new session contents in "
"the temporary file '%s': %s"
msgstr ""
"Failed to replace the old session file '%s' with the new session contents in "
"the temporary file '%s': %s"

#: src/session.c:1744
#, c-format
msgid "Could not remove temporary file '%s': %s\n"
msgstr "Could not remove temporary file '%s': %s\n"

#: src/session.c:1920
msgid "Select a session"
msgstr "Select a session"

#: src/session.c:1925
msgid "Load Session"
msgstr "Load Session"

#: src/session.c:1936
msgid ""
"<span weight=\"bold\" size=\"larger\">Select a session</span>\n"
"\n"
"Please select a session to load from the list below."
msgstr ""
"<span weight=\"bold\" size=\"larger\">Select a session</span>\n"
"\n"
"Please select a session to load from the list below."

#: src/session.c:1993
msgid "(read-only)"
msgstr "(read-only)"

#: src/session.c:2018
msgid "Session"
msgstr "Session"

#: src/session.c:2024
msgid "Status"
msgstr "Status"

#: src/session.c:2135
#, c-format
msgid "Could not open the session \"%s.\""
msgstr "Could not open the session \"%s.\""

#: src/session.c:2139
msgid "Abort login"
msgstr "Abort login"

#: src/session.c:2141 src/session.c:2164 src/session.c:2183 src/session.c:2201
#: src/session.c:2219
msgid "Log into other session"
msgstr "Log into other session"

#: src/session.c:2143 src/session.c:2185 src/session.c:2203 src/session.c:2221
msgid "Log into default session"
msgstr "Log into default session"

#: src/session.c:2145 src/session.c:2166
msgid "Log into failsafe session"
msgstr "Log into failsafe session"

#: src/session.c:2155
#, c-format
msgid ""
"You are already logged in elsewhere, using the session \"%s.\" You can only "
"use a session from one location at a time."
msgstr ""
"You are already logged in elsewhere, using the session \"%s.\" You can only "
"use a session from one location at a time."

#: src/session.c:2160
msgid "Log in anyway"
msgstr "Log in anyway"

#: src/session.c:2162
msgid "Try again"
msgstr "Try again"

#: src/session.c:2176
#, c-format
msgid "The session file for session \"%s\" appears to be invalid or corrupted."
msgstr ""
"The session file for session \"%s\" appears to be invalid or corrupted."

#: src/session.c:2181 src/session.c:2199 src/session.c:2217
msgid "Revert session to defaults"
msgstr "Revert session to defaults"

#: src/session.c:2195
#, c-format
msgid "The session \"%s\" contains no applications."
msgstr "The session \"%s\" contains no applications."

#: src/session.c:2213
#, c-format
msgid "The session \"%s\" has encountered an unrecoverable error."
msgstr "The session \"%s\" has encountered an unrecoverable error."

#: src/session.c:2257
msgid "User selected session could not be loaded."
msgstr "User selected session could not be loaded."

#: src/session.c:2339
#, c-format
msgid "line %d character %d: %s\n"
msgstr "line %d character %d: %s\n"

#: src/session.c:2355
msgid "<client> element must have an 'id' attribute giving the session ID"
msgstr "<client> element must have an 'id' attribute giving the session ID"

#: src/session.c:2361 src/session.c:2368
#, c-format
msgid ""
"<client> element does not allow the attribute '%s', only an 'id' attribute"
msgstr ""
"<client> element does not allow the attribute '%s', only an 'id' attribute"

#: src/session.c:2375
msgid "'id' attribute of <client> element has empty value"
msgstr "'id' attribute of <client> element has empty value"

#: src/session.c:2386
#, c-format
msgid ""
"'id' attribute of <client> element duplicates another <client> element's id "
"(duplicate id is '%s')"
msgstr ""
"'id' attribute of <client> element duplicates another <client> element's id "
"(duplicate id is '%s')"

#: src/session.c:2425
msgid "'name' attribute specified twice on <prop> element"
msgstr "'name' attribute specified twice on <prop> element"

#: src/session.c:2436
msgid "'type' attribute specified twice on <prop> element"
msgstr "'type' attribute specified twice on <prop> element"

#: src/session.c:2445
#, c-format
msgid "Attribute '%s' not allowed on <prop> element"
msgstr "Attribute '%s' not allowed on <prop> element"

#: src/session.c:2456
msgid "<prop> element must have a 'type' attribute"
msgstr "<prop> element must have a 'type' attribute"

#: src/session.c:2462
msgid "<prop> element must have a 'name' attribute"
msgstr "<prop> element must have a 'name' attribute"

#: src/session.c:2490
#, c-format
msgid "<prop> type '%s' is unknown, should be %s, %s, or %s"
msgstr "<prop> type '%s' is unknown, should be %s, %s, or %s"

#: src/session.c:2520
#, c-format
msgid ""
"File does not appear to be a session file; outermost element is <%s> rather "
"than <msm_session>"
msgstr ""
"File does not appear to be a session file; outermost element is <%s> rather "
"than <msm_session>"

#: src/session.c:2533
#, c-format
msgid "Element <%s> may not appear inside the <msm_session> element"
msgstr "Element <%s> may not appear inside the <msm_session> element"

#: src/session.c:2553
#, c-format
msgid "Element <%s> may not appear inside a <client> element"
msgstr "Element <%s> may not appear inside a <client> element"

#: src/session.c:2572
#, c-format
msgid "Element <%s> may not appear inside a <prop> element"
msgstr "Element <%s> may not appear inside a <prop> element"

#: src/session.c:2578
msgid "Attributes are not allowed on the <value> element"
msgstr "Attributes are not allowed on the <value> element"

#: src/session.c:2588
#, c-format
msgid "No elements allowed inside <value> element (<%s> found)"
msgstr "No elements allowed inside <value> element (<%s> found)"

#: src/session.c:2594 src/session.c:2760
#, c-format
msgid "Element <%s> appears after the <msm_session> element"
msgstr "Element <%s> appears after the <msm_session> element"

#: src/session.c:2613 src/session.c:2621 src/session.c:2634 src/session.c:2653
#: src/session.c:2675
#, c-format
msgid "Unexpected close element tag </%s>"
msgstr "Unexpected close element tag </%s>"

#: src/session.c:2688
msgid "<value> element has no contents"
msgstr "<value> element has no contents"

#: src/session.c:2694
msgid ""
"Exactly one <value> element required below <prop> elements other than type "
"LISTofARRAY8 - no more and no less"
msgstr ""
"Exactly one <value> element required below <prop> elements other than type "
"LISTofARRAY8 - no more and no less"

#: src/session.c:2710
#, c-format
msgid "Failed to parse '%s' (given as value of a CARD8 property) as an integer"
msgstr ""
"Failed to parse '%s' (given as value of a CARD8 property) as an integer"

#: src/session.c:2716
#, c-format
msgid ""
"Only values between 0 and 255 are allowed in CARD8 properties (%ld too large)"
msgstr ""
"Only values between 0 and 255 are allowed in CARD8 properties (%ld too large)"

#: src/session.c:2796
msgid "Unexpected text outside of <value> element"
msgstr "Unexpected text outside of <value> element"

#. Can't imagine this actually happening
#: src/session.c:2875
#, c-format
msgid "Failed to stat new session file descriptor (%s)\n"
msgstr "Failed to stat new session file descriptor (%s)\n"

#: src/session.c:2892
msgid "Session is empty and no global session available.\n"
msgstr "Session is empty and no global session available.\n"

#: src/session.c:3040
#, c-format
msgid "No '%s' command provided by the application"
msgstr "No '%s' command provided by the application"

#: src/session.c:3120
#, c-format
msgid "Failed to clean up saved session data for '%s': %s"
msgstr "Failed to clean up saved session data for '%s': %s"

#: src/shutdowndialog.c:133
msgid "Shutdown Types"
msgstr "Shutdown Types"

#: src/shutdowndialog.c:134
msgid "Available shutdown types to offer to user"
msgstr "Available shutdown types to offer to user"

#: src/shutdowndialog.c:143
msgid "Save Session"
msgstr "Save Session"

#: src/shutdowndialog.c:144
msgid "Whether or not to save session"
msgstr "Whether or not to save session"

#: src/shutdowndialog.c:151
msgid "Selected Shutdown Type"
msgstr "Selected Shutdown Type"

#: src/shutdowndialog.c:152
msgid "Default shutdown type"
msgstr "Default shutdown type"

#: src/shutdowndialog.c:184
msgid ""
"<span weight=\"bold\" size=\"larger\">End the session</span>\n"
"\n"
"Would you like to end the session?  "
msgstr ""
"<span weight=\"bold\" size=\"larger\">End the session</span>\n"
"\n"
"Would you like to end the session?  "

#: src/shutdowndialog.c:275
msgid "_Power off"
msgstr "_Power off"

#: src/shutdowndialog.c:411
msgid "End Session"
msgstr "End Session"

#: src/splashwindow.c:183
msgid "Icon Spacing"
msgstr "Icon Spacing"

#: src/splashwindow.c:185
msgid "Space between splash screen icons."
msgstr "Space between splash screen icons."

#: src/splashwindow.c:192
msgid "Icon Border Width"
msgstr "Icon Border Width"

#: src/splashwindow.c:194
msgid "Space around splash screen icons."
msgstr "Space around splash screen icons."

#: src/splashwindow.c:201
msgid "Icon Size"
msgstr "Icon Size"

#: src/splashwindow.c:203
msgid "Size of splash screen icons."
msgstr "Size of splash screen icons."

#: src/splashwindow.c:210
msgid "Status Font Size"
msgstr "Status Font Size"

#: src/splashwindow.c:212
msgid "Size of splash screen status text."
msgstr "Size of splash screen status text."

#: src/splashwindow.c:218
msgid "Background"
msgstr "Background"

#: src/splashwindow.c:220
msgid "Splash screen background image."
msgstr "Splash screen background image."

#: src/startup-default.c:142 src/startup.c:339
#, c-format
msgid "Starting session \"%s,\" please wait"
msgstr "Starting session \"%s,\" please wait"

#: src/startup-default.c:162 src/startup.c:350
msgid "Finished starting applications"
msgstr "Finished starting applications"

#: src/startup-default.c:180 src/startup.c:369
#, c-format
msgid "Starting \"%s\""
msgstr "Starting \"%s\""

#: src/startup-default.c:197 src/startup.c:409
#, c-format
msgid "Successfully started \"%s\""
msgstr "Successfully started \"%s\""

#: src/startup.c:87 src/startup.c:95
#, c-format
msgid "Failed to open startup progress indicator module '%s': %s\n"
msgstr "Failed to open startup progress indicator module '%s': %s\n"

#: src/startup.c:108
#, c-format
msgid "No function 'msm_startup_module_get_vtable' in module %s"
msgstr "No function 'msm_startup_module_get_vtable' in module %s"

#: src/startup.c:124
#, c-format
msgid "Module '%s' is for interface version %d but msm was compiled for %d"
msgstr "Module '%s' is for interface version %d but msm was compiled for %d"

#: src/startup.c:134
#, c-format
msgid "Failed to init module '%s'"
msgstr "Failed to init module '%s'"

#: src/startup.c:411
#, c-format
msgid "Unsuccessfully started \"%s\""
msgstr "Unsuccessfully started \"%s\""

#: src/util.c:157
#, c-format
msgid "Failed to create directory '%s': %s\n"
msgstr "Failed to create directory '%s': %s\n"

#: src/util.c:206
#, c-format
msgid "Failed to get writable status of file '%s': %s\n"
msgstr "Failed to get writable status of file '%s': %s\n"

#: src/util.c:231
#, c-format
msgid "Failed to get readable status of file '%s': %s\n"
msgstr "Failed to get readable status of file '%s': %s\n"

#: src/util.c:265
#, c-format
msgid "Failed to overwrite file '%s': %s\n"
msgstr "Failed to overwrite file '%s': %s\n"

